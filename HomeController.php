<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	protected $questions;
	protected $answer;
	protected $comments;

	 public function __construct(){
	 	//Declaring objects of Question and Answer Model
	 	$this->questions=new Question;
		$this->answer=new Answer;
		$this->comments=new Comments;
	 }

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function index(){ 
            
		//Taking parameter to sort accordingly
		$params = App::make('router')->getCurrentRoute()->getParameters();
                $catid = 'all';
                $tagid = 'all';
                $keywords = '';                
                $user_login = '';
                $user_key = '';
                
                if(Request::segment(2)){
                    if(Request::segment(1) == 'category'){
                        $cat_url_key = Request::segment(2);
                        $cat_info = DB::table('categories')->select(array('categoryid'))->where('url_key', $cat_url_key)->first();
                        $catid = 1;
                        if(count($cat_info) > 0){
                            $catid = $cat_info->categoryid;
                        }
                    }
                    if(Request::segment(1) == 'tag'){
                        $tag_url_key = Request::segment(2);
                        $tag_info = DB::table('tags')->select(array('id'))->where('url_key', $tag_url_key)->first();
                        $tagid = 1;
                        if(count($tag_info) > 0){
                            $tagid = $tag_info->id;
                        }
                    }
                    
                    if(Request::segment(2) == 'search'){
                        $request_uri = $_SERVER['REQUEST_URI'];
                        $request_uri = explode('?keyword=', $request_uri);  
                        if(count($request_uri) > 1 and $request_uri[1]!=""){
                            $keywords = $request_uri[1];
                        }
                    }
                    
                    if(Request::segment(2) == 'reset'){
                        $user_information = DB::table('users')->select(array('email'))->where('activation_token', $params['token'])->first();
                        if(count($user_information) > 0){                            
                            $user_login = $user_information->email;
                            $user_key = $params['token'];
                        }
                    }
                    
                    
                }
                
                $limit = Input::get('limit', 10);
                
                
		//If there is anything for sorting
		if(isset($params['sort'])){
			$data['questions'] = $this->questions->getSortedQuestions($params['sort'], $limit, $catid, $tagid);
                        $data['basic']['sort_type'] = $params['sort'];
                        $data['basic']['head'] = "Frågor & Svar";
                        if($params['sort']=="hot"){
                            $data['basic']['head'] = "Gillade frågor";
                        }
                        if($params['sort']=="votes"){
                            $data['basic']['head'] = "Frågor med flest röster";
                        }
                        if($params['sort']=="answers"){
                            $data['basic']['head'] = "Frågor med flest svar";
                        }
                        if($params['sort']=="flagged"){
                            $data['basic']['head'] = "Flaggade frågor";
                        }
		}
		else{ 
			$data['questions'] = $this->questions->getQuestions($limit, $catid, $tagid, $keywords);
                        $data['basic']['head'] = "Frågor & Svar";
                        $data['basic']['sort_type'] = "recent";
		}
                foreach($data['questions'] as $record){                   
                    if($record->tag_ids!=""){
                        $tag_ids = explode(',',$record->tag_ids);
                        $tag_names = DB::table('tags')->select(array('id', 'name', 'url_key'))->whereIn('id', $tag_ids)->get();
                        $record->tags = $tag_names;
                    }else{
                        $record->tags = array();
                    }    
                }
                $data['basic']['page_limit'] = $limit;
                $data['basic']['catid'] = $catid;
                if($catid == 'all' and $tagid == 'all'){
                   $data['basic']['base_url'] = '/questions'; 
                }else if($catid != 'all'){
                    $data['basic']['base_url'] = '/category/'.$cat_url_key;
                }else{
                    $data['basic']['base_url'] = '/tag/'.$tag_url_key;
                } 
                
                 $data['user_information_reset']['user_login'] = $user_login;
                 $data['user_information_reset']['user_key'] = $user_key;
                  
                
                if(BrowserDetect::isMobile() == TRUE){
                    return View::make('mobile.index')->with('data', $data);
                }else{
                    return View::make('front.index')->with('data', $data);
                }
                
		
	}

	public function showUnanswerQuestion(){
		//$questions=new Question;
		$data=$this->questions->getUnanswered();
		$data[0]['head']="Recent questions without answers";
		return View::make('front.index')->with('data', $data);
	}

    public function showTags(){
        
        $data['keyword'] = '';
        $request_uri = $_SERVER['REQUEST_URI'];
        $request_uri = explode('?ckey=', $request_uri);
        if (count($request_uri) > 1 and $request_uri[1] != "") {
            $keyword = $request_uri[1];
            $first_cher = substr($keyword, 0,1);
            $alphas = array($first_cher);
            $data['alphas'] = $alphas;
            $data['keyword'] = $keyword;
            $data['tags'][$first_cher] = DB::table('tags')
                            ->where('name', 'LIKE', "%$keyword%")
                            ->orWhere('url_key', 'LIKE', "%$keyword%")
                            ->orderBy('usage', 'DESC')->paginate(50);
             
        } else {
            $alphas = range('a', 'z');
            $data['alphas'] = $alphas;
            foreach ($alphas as $alpha) {
                $data['tags'][$alpha] = $this->questions->getTags($alpha, 4);
            }            
        }
        
        if (BrowserDetect::isMobile() == TRUE) {
            return View::make('mobile.tags')->with('data', $data);
        } else {
            return View::make('front.tags')->with('data', $data);
        }
    }
        
        public function tags_popular(){
            $data['keyword'] = '';
            $data['tags'] = $this->questions->getTagsPopular();
            if(BrowserDetect::isMobile() == TRUE){
                return View::make('mobile.tags_popular')->with('data', $data);
            }else{
                return View::make('front.tags_popular')->with('data', $data);
            }
            
	}
        
        public function tagsByAlphabatic(){
            $data['keyword'] = '';
           if(Request::segment(2)){ 
                $alphas = array(Request::segment(2));
                $data['alphas'] = $alphas;
                foreach($alphas as $alpha){    
                    $data['tags'][$alpha] = $this->questions->getTags($alpha, 'all');
                }
                if(BrowserDetect::isMobile() == TRUE){
                    return View::make('mobile.tags')->with('data', $data);
                }else{
                    return View::make('front.tags')->with('data', $data);
                }
                
           }  
	}

	public function showSingleQuestion($qa){
                $post_info = DB::table('posts')->select(array('postid'))->where('type', 'Q')->where('url_key', $qa)->first();
                $qa = $post_info->postid;
                
                $data['question_detail_page'] = TRUE;
		$data['answers']=$this->answer->getAnswer($qa);
		$data['question']=$this->questions->getSingleQuestion($qa);
		DB::table('posts')->where('postid', $qa)->increment('views');
                DB::table('posts')->where('postid', $qa)->increment('hotness');
                
		//Getting comments for each section
                 
                $comments=$this->comments->getCommentsByParentId($data['question']['postid']);
                $data['comments'][$data['question']['postid']]=$comments;
		foreach ($data['answers'] as $record) {
			$comments=$this->comments->getCommentsByParentId($record['postid']);
			$data['comments'][$record['postid']]=$comments;
		}
                $tag_ids = explode(',',$data['question']['tag_ids']);
                $tag_names = DB::table('tags')->select(array('id', 'name', 'url_key'))->whereIn('id', $tag_ids)->get();
                $data['question']['tags'] = $tag_names;
                
//                echo '<pre>'; print_r($data);die;
		//$data['comments']=;
                if(BrowserDetect::isMobile() == TRUE){
                    $view=View::make('mobile.single_question')->with('data', $data);
                }else{
                    $view=View::make('front.single_question')->with('data', $data);
                }
		
		return $view;
	}

	function askQuestion() {
		//Taking parameter if there is asking related question
		$params = App::make('router')->getCurrentRoute()->getParameters();
		$data = [];
		if(isset($params['follow'])){
			$data['follow']=$params['follow'];//parent post id
			$post=$this->questions->getPostById($data['follow']);
			$data['content']=$post['content'];
			$data['postid']=$post['postid'];
		}
		return View::make('user.ask')->with('data', $data);
	}

	function doAskQuestion(){
		$rules = array(
			'title'    => 'required|min:12', // make sure its required and min length of 12
		);
		
		// Run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('/ask')
				->withErrors($validator); // send back all errors to the login form
		}
		else{
			try{
				if (Auth::check())
				{
				   $userid = Auth::user()->userid;
				}
				else 
					$userid=NULL;
				
				if(Input::get('notify'))
					$email=Input::get('email');
				else
					$email=NULL;

				if(Input::get('parentid'))
					$parentid=Input::get('parentid');
				else
					$parentid=NULL;

				//$request = Request::instance();
				//$request->setTrustedProxies(array('127.0.0.1')); // only trust proxy headers coming from the IP addresses on the array (change this to suit your needs)
				//$ip = $request->getClientIp();
				//echo $ip;die;
				
				$postid = DB::table('posts')->insertGetId(
				    array('type' => 'Q',
				    	 'createip' => $_SERVER['REMOTE_ADDR'],
				    	 'lastviewip' =>$_SERVER['REMOTE_ADDR'],
		    	 		 'created' =>date('Y-m-d H:i:s'),
		    	 		 'title' => Input::get('title'),		    	 		
		    	 		 'content' => Input::get('content'),
		    	 		 'name' => Input::get('name'),
		    	 		 'tags' => Input::get('tags'),
		    	 		 'userid' => $userid,
		    	 		 'parentid' => $parentid,	    	 		
			    	 	 'notify' => $email)
				);

				DB::table('posts')
            		->where('postid', Input::get('qa'))
            		->increment('acount');
						
                return Redirect::to('/question/'.$postid);
			}
			catch(ParseException $error){

			}
		}
	}

	public function doSearch(){
		$data=$this->questions->getSearchQuestions(Input::get('q'));
		if(empty($data))
			$data[0]['head']="No results found for ".Input::get('q')."";
		else
			$data[0]['head']="Search results for ".Input::get('q')."";
		//echo "<pre>";print_r($data);die;
		return View::make('front.index')->with('data', $data);
	}

	public function doTag(){
		//Taking parameter if there is asking related question
		$params = App::make('router')->getCurrentRoute()->getParameters();
		$data = [];
		//echo $params['tag'];die;
		$data=$this->questions->SearchQuestionsByTag($params['tag']);
		if(empty($data))
			$data[0]['head']="No results found for tag ".$params['tag']."";
		else
			$data[0]['head']="Recent questions tagged tag ".$params['tag']."";
		//echo "<pre>";print_r($data);die;
		return View::make('front.index')->with('data', $data);
	}
        
        public function edit(){
            //Taking parameter i.e postid
            $params = App::make('router')->getCurrentRoute()->getParameters();
            $post=new Question;
            $data=$post->getPostById($params['postid']);
            return View::make('user.edit')->with('data', $data);
        }
        
        public function doEdit(){
            $rules = array(
			'title'    => 'required|min:12', // make sure its required and min length of 12
		);
		
		// Run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('/edit/'.Input::get('postid'))
				->withErrors($validator); // send back all errors to the login form
		}
		else{
			try{
				if (Auth::check())
				{
				   $userid = Auth::user()->userid;
				}
				else 
					$userid=NULL;
				
				if(Input::get('notify'))
					$email=Input::get('email');
				else
					$email=NULL;

				if(Input::get('parentid'))
					$parentid=Input::get('parentid');
				else
					$parentid=NULL;
                       
                                DB::table('posts')
                                    ->where('postid', Input::get('postid'))
                                    ->update(array(
		    	 		 'title' => Input::get('title'),		    	 		
		    	 		 'content' => Input::get('content'),
		    	 		 'name' => Input::get('name'),
                                         'notify' => Input::get('notify'),
		    	 		 'tags' => Input::get('tags')
                                    ));
						
                return Redirect::to('/question/'.Input::get('postid'));
			}
			catch(ParseException $error){

			}
		}
        }
        
        public function categories(){
            
            $data['keyword'] = '';
            $request_uri = $_SERVER['REQUEST_URI'];
            $request_uri = explode('?ckey=', $request_uri);
            if (count($request_uri) > 1 and $request_uri[1] != "") {
                $keyword = $request_uri[1];
                $data['keyword'] = $keyword;
                $first_cher = substr($keyword, 0,1);
                $alphas = array($first_cher);
                $data['alphas'] = $alphas;
                $data['categories'][$first_cher] = $this->questions->getCategories($first_cher, 'all', $keyword);
            }else{
                $alphas = range('a', 'z');
                $data['alphas'] = $alphas;
                foreach($alphas as $alpha){    
                 $data['categories'][$alpha] = $this->questions->getCategories($alpha, 4);
                }
            }
            
            if(BrowserDetect::isMobile() == TRUE){
                return View::make('mobile.categories')->with('data', $data);
            }else{
                return View::make('front.categories')->with('data', $data);
            }
            
	}
        
        public function categories_popular(){
            
            $data['categories'] = $this->questions->getCategoriesPopular();
            if(BrowserDetect::isMobile() == TRUE){
                return View::make('mobile.categories_popular')->with('data', $data);
            }else{
                return View::make('front.categories_popular')->with('data', $data);
            }
            
	}
        
        public function categoriesByAlphabatic(){
           if(Request::segment(2)){ 
                $alphas = array(Request::segment(2));
                $data['alphas'] = $alphas;
                foreach($alphas as $alpha){    
                    $data['categories'][$alpha] = $this->questions->getCategories($alpha, 'all');
                }
                if(BrowserDetect::isMobile() == TRUE){
                    return View::make('mobile.categories_alphabatic')->with('data', $data);
                }else{
                    return View::make('front.categories_alphabatic')->with('data', $data);
                }
                
           }  
	}
        
        public function confirm_user(){
            if(Auth::check() and Request::segment(3)!=""){
                $user = DB::table('users')->where('userid', Auth::user()->userid)->where('activation_token', Request::segment(3))->first();
                if(count($user) > 0){
                    DB::table('users')->where('userid', Auth::user()->userid)->update(array('verified_account' => 1, 'activation_token' => ''));
                }
            }
            return Redirect::to('/');
        }

        public function listAllUsers(){
            
            $data['type'] = '';
            $data['keyword'] = '';
            if(Request::segment(2)){
                $data['type'] = 'points';
                $data['users'] = DB::table('users')->orderBy('points', 'desc')->paginate(50);
            }else{
               $request_uri = $_SERVER['REQUEST_URI'];
               $request_uri = explode('?keyword=', $request_uri);              
               if(count($request_uri) > 1 and $request_uri[1]!=""){
                   $keyword = $request_uri[1];
                   $data['keyword'] = $keyword;
                   $data['users'] = DB::table('users')
                                    ->where('email', 'LIKE', "%$keyword%")
                                    ->orWhere('handle', 'LIKE', "%$keyword%")
                                    ->orWhere('username', 'LIKE', "%$keyword%")
                                    ->orderBy('username', 'asc')->paginate(50);
               }else{
                    $data['users'] = DB::table('users')->orderBy('username', 'asc')->paginate(50);
               }
            }
            
            if(BrowserDetect::isMobile() == TRUE){
                return View::make('mobile.listAllUsers')->with('data', $data);
            }else{
                return View::make('front.listAllUsers')->with('data', $data);
            }
            
        }
        
        public function user_detail($url_key){
            if(isset($url_key) and $url_key!=""){
                $data['user'] = DB::table('users')->where('url_key', $url_key)->get();
                if(count($data['user']) > 0){ 
                    $id = $data['user'][0]->userid;
                    $type = 'question';
                    $params = App::make('router')->getCurrentRoute()->getParameters();
                    if(isset($params['sort'])){
			$type = $params['sort'];
		    }
                    $data['type'] = $type;
                    $data['total_questions'] = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                        'categories.backpath as categorybackpath',
                                        'userpoints.points',
                                        'users.flags',
                                        'users.level',
                                        'users.email',
                                        'users.handle',
                                        'users.user_picture',
                                        'users.usertype',
                                        'users.url_key as user_url_key',
                                        'users.avatarblobid',
                                        'users.avatarwidth',
                                        'users.avatarheight'
                                        ))
                        ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                        ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                        ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                        ->where('type', '=', 'Q')
                        ->where('posts.userid', '=', $id)            
                        ->orderBy('postid', 'desc')
                        ->count();                    
                    
                    $questions = array();
                    if($type == 'question'){
                        $questions = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                        'categories.backpath as categorybackpath',
                                        'userpoints.points',
                                        'users.flags',
                                        'users.level',
                                        'users.email',
                                        'users.handle',
                                        'users.user_picture',
                                        'users.usertype',
                                        'users.url_key as user_url_key',
                                        'users.avatarblobid',
                                        'users.avatarwidth',
                                        'users.avatarheight'
                                        ))
                        ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                        ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                        ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                        ->where('type', '=', 'Q')
                        ->where('posts.userid', '=', $id)            
                        ->orderBy('postid', 'desc')
                        ->paginate(10);
                    }
                    
                    $data['total_answers'] = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                                    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                                    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                                    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                                    ->where('type', '=', 'A')
                                    ->where('posts.userid', '=', $id)            
                                    ->orderBy('postid', 'desc')
                                    ->count();
                    
                $answers = array();    
                if($type == 'answer'){
                    $answers = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'A')
                ->where('posts.userid', '=', $id)            
                ->orderBy('postid', 'desc')
                ->paginate(10); 
                    
                }    
                $comments = array(); 
                
                if($type == 'comment'){
                $comments = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'C')
                ->where('posts.userid', '=', $id)            
                ->orderBy('postid', 'desc')
                ->paginate(10); 
                
                }   
                
                
                foreach($questions as $record){
                    if($record->tag_ids!=""){
                        $tag_ids = explode(',',$record->tag_ids);
                        $tag_names = DB::table('tags')->select(array('id', 'name', 'url_key'))->whereIn('id', $tag_ids)->get();
                        $record->tags = $tag_names;
                    }else{
                        $record->tags = array();
                    }    
                }
                
                $answer_questions_ids = array();
                foreach($answers as $record){
                    $answer_questions_ids[] = $record->parentid;
                    if($record->tag_ids!=""){
                        $tag_ids = explode(',',$record->tag_ids);
                        $tag_names = DB::table('tags')->select(array('id', 'name', 'url_key'))->whereIn('id', $tag_ids)->get();
                        $record->tags = $tag_names;
                    }else{
                        $record->tags = array();
                    }    
                }
                
                $answer_questions = array();
                
                if(count($answer_questions_ids) > 0){
                    $answer_questions_ids = array_unique($answer_questions_ids);
                    $answer_questions = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'Q')
                ->whereIn('posts.postid', $answer_questions_ids)            
                ->orderBy('postid', 'desc')
                ->get();
                    
                  foreach($answer_questions as $record){
                    if($record->tag_ids!=""){
                        $tag_ids = explode(',',$record->tag_ids);
                        $tag_names = DB::table('tags')->select(array('id', 'name', 'url_key'))->whereIn('id', $tag_ids)->get();
                        $record->tags = $tag_names;
                    }else{
                        $record->tags = array();
                    }    
                   }  
                }
                
                
                
                
                
                
                $answer_comments_ids = array();
                $answer_comments = array();
                foreach($comments as $record){
                    $answer_comments_ids[] = $record->parentid;
                }
                
                if(count($answer_comments_ids) > 0){
                    $answer_comments_ids = array_unique($answer_comments_ids);
                    $answer_comments = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->whereIn('posts.postid', $answer_comments_ids)            
                ->get();
                }
                
                
                //echo '<pre>'; print_r($answer_comments); echo '</pre>'; die;   
                $data['questions'] = $questions;
                $data['answers'] = $answers;
                $data['comments'] = $comments;
                $data['answer_questions'] = $answer_questions;
                $data['answer_comments'] = $answer_comments;
                
                if(BrowserDetect::isMobile() == TRUE){
                    return View::make('mobile.user_detail')->with('data', $data);
                }else{
                    return View::make('front.user_detail')->with('data', $data);
                }
                
                }else {
                   return Redirect::to('/users');
                }
            }else{
                return Redirect::to('/users');
            }
        }
        
        public function about_us(){
            $page_data['page_name'] = 'About Us';
            $page_data['page_content'] = 'About Us';
            return View::make('front.cms_page')->with('page_data', $page_data);
        }
        
        public function term_and_condition(){
            $page_data['page_name'] = 'Term and Condition';
            $page_data['page_content'] = 'Term and Condition';
            return View::make('front.cms_page')->with('page_data', $page_data);
        }
        
        public function contact(){
            $page_data['page_name'] = 'Contact';
            $page_data['page_content'] = '';
            return View::make('front.cms_page')->with('page_data', $page_data);
        }
        
        public function badges(){
            if(BrowserDetect::isMobile() == TRUE){
                return View::make('mobile.badges');
            }else{
                return View::make('front.badges');
            }
        }
        
        public function update_hotness(){
           $all_questions = DB::table('posts')->select(array('postid','views','netvotes'))->where('type', 'Q')->get(); 
           foreach ($all_questions as $question){
               $hotness = $question->views + $question->netvotes;
               $total_comment = DB::table('posts')->where('parentid' , $question->postid)->where('type', 'C')->count();
               $hotness += $total_comment;
               $total_answers = DB::table('posts')->where('parentid' , $question->postid)->where('type', 'A')->count();
               $hotness += $total_answers;
               
               $answers = DB::table('posts')->select(array('postid','netvotes'))->where('parentid' , $question->postid)->where('type', 'A')->get();
               if(count($answers) > 0){
                   foreach ($answers as $ans){
                       $hotness += $ans->netvotes;
                       $total_comment = DB::table('posts')->where('parentid' , $ans->postid)->where('type', 'C')->count();
                       $hotness += $total_comment;
                   } 
               }
               
               DB::table('posts')->where('postid', $question->postid)->update(array( 'hotness' => $hotness ));
               
           }
        }
        
        public function update_question_title(){
            $all_questions = DB::table('posts')->select(array('postid','url_key'))->where('type', 'Q')->get(); 
            foreach($all_questions as $question){
                $title = str_replace('-', ' ', $question->url_key);
                DB::table('posts')->where('postid', $question->postid)->update(array( 'title' => ucfirst($title) ));
            }
        }
        
        public function sphinx_indexer(){
            
	      $output = exec("/usr/local/bin/indexer --config /usr/local/etc/sphinx.conf --all --rotate", $o);
	      echo '<pre>'; print_r($o); echo '</pre>';	die;		
//            $output = shell_exec('ps aux | grep searchd') or die("bash didn't work = ps aux | grep searchd");
//            $output = explode(' ', $output);
//            if(count($output) > 0){
//                shell_exec('Kill '.$output[4]) or die("bash didn't work = Kill ".$output[4]);
//                shell_exec('Kill '.$output[39]) or die("bash didn't work");
//            }
//            $output = shell_exec('ps aux | grep searchd');
//            echo '<pre>';
//                print_r($output);
//            echo '</pre>'; 
//            die;
            //$command = 'sh /home/nginx/domains/svarpaallt.website/public/www/sphinx_indexer.sh';
           // $output = shell_exec($command);
           // echo '<pre>';
             //   print_r($output);
           // echo '</pre>';    
        }
        
}
