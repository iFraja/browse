@extends('layouts.default')
@section('content')
<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>Användare</span>
        </div>
        <div class="col-md-6 col-xs-6 select-categories input-find-tags">
            <form class="form-input-search" autocomplete="off" method="GET" action="/users">
                <input autocomplete="off" type="text" name="keyword" id="keyword" value="{{$data['keyword']}}" class="search-users" placeholder="Hitta användare" />
                <i class="fa fa-chevron-circle-right"></i>
            </form>
        </div>                       
    </div><!-- END SELECT-CATEGORY -->
   
    <div class="row question-filter">
        <div class="col-md-6 col-xs-6 sort-questions">
            <ul>
                <li>
                    <a href="/users" @if($data['type'] == '') class="active" @endif >Namn</a>
                </li>
                <li>
                    <a href="/users/points" @if($data['type'] == 'points') class="active" @endif >Poäng</a>
                </li>                    
            </ul>
        </div>
    </div>
   
    @if($data['users'])
    <div class="main-user-list">
        <ul class="row" id="main_users_list">
            @foreach($data['users'] as $user)
            <li class="user-item col-md-4 col-xs-6">
                <span class="user-avatar">
                    <img alt="" class="avatar" src="{{$user->user_picture}}">                    </span>
                <div class="left-info">
                    <a href="/user/{{$user->url_key}}"><span class="display_name">{{$user->username}}</span></a><br>
                    <span class="location">
                        <i class="fa fa-map-marker"></i> {{$user->location}}                        </span>
                    <div class="question-cat">
                        <span style="background-color:{{ Config::get('badges.'.$user->usertype.'.color') }};" class="user-badge" title="{{ Config::get('badges.'.$user->usertype.'.type') }}">{{ Config::get('badges.'.$user->usertype.'.type') }}</span>                            <span class="points">{{$user->points}} Poäng </span>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
   @endif
    <div class="clearfix grey-line"></div>
    <div class="row paginations home">
        <div class="col-md-12">
            {{ $data['users']->links() }}
        </div>
    </div>
</div>

@stop
