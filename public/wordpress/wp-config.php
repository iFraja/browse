<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'r0OtP@sz_orD');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^>uy2)q0HwWZ&FworN{9$5]p-%|^Zm6_XzsB~|{NN/xKJX)0Y|5ht|@S%v|Aba^F');
define('SECURE_AUTH_KEY',  '@q1)^)@,P+j:j;c05L^TEr,*W*7?}R;<w-,fFIsbp.D1eV8|~|VyO?p>nbGU*G!5');
define('LOGGED_IN_KEY',    'I>WP7]$!>L?o5.:b+2Yh/U7s6 1uMcus,2@VxE~QOjl^A(-Gnco?)JnBDAAEDDrc');
define('NONCE_KEY',        'ha>s;g%ISY5:yb#[t9arj1i gT@R+0V<RHU[~R@CW]-KYH.)H|i*G{Qgd<]zH-;d');
define('AUTH_SALT',        'z%0k0tZ]a-[BPNVmBiX(nX417Q?ZJmCGbEH&w-}_~yCWO.@Kwl)X~4vjm2^:]FOs');
define('SECURE_AUTH_SALT', 'D4vrZD/`^UGk(_FA:K-=;Q>l&}o$D)6;99-qO%G-VV)qrzr|P{|F[li|%3cXI-p`');
define('LOGGED_IN_SALT',   'Oz9yM]k.x)#|:R+jT$a~~.cA}n&nOx6v#x%YJ+0c@{P[Z/Uw/*T#LtkgoECxUK-$');
define('NONCE_SALT',       'B.!O1]~]2PSFBbL>n~8%f_|1@Sw(>iRaQ&%;^lj/M]l*+|O#Nj{$A*XfIx8|{)s-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
