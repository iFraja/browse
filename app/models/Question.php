<?php 
/*
	|--------------------------------------------------------------------------
	| Model for Questions
	|--------------------------------------------------------------------------
	|
	| All Questions related Queries are to be there in this model. 
*/
use Carbon\Carbon;
class Question extends Eloquent{
	
	public function getQuestions($limit = 10, $catid = 'all', $tagid = 'all', $keywords=NULL){
		
               $data = array(); 
               if($catid == 'all' and $tagid == 'all'){
                   if($keywords!=""){
                       $total_rec = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                                    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                                    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                                    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                                    ->where('type', '=', 'Q')
                                    ->where('posts.title', 'LIKE', "%$keywords%")
                                    ->orWhere('posts.content', 'LIKE', "%$keywords%")           
                                    ->orderBy('postid', 'desc')
                                    ->count();
                   }else{
                       $total_rec = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                                    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                                    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                                    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                                    ->where('type', '=', 'Q')
                                    ->orderBy('postid', 'desc')
                                    ->count();
               }
                   
                   if($total_rec > 0){
                      
                       if($keywords!=""){
                           $data = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                                    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                                    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                                    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                                    ->where('type', '=', 'Q')
                                    ->where('posts.title', 'LIKE', "%$keywords%")
                                    ->orWhere('posts.content', 'LIKE', "%$keywords%")              
                                    ->orderBy('postid', 'desc')
                                    ->paginate($limit);
                       }else{
                           $data = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                                    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                                    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                                    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                                    ->where('type', '=', 'Q')
                                    ->orderBy('postid', 'desc')
                                    ->paginate($limit);
                       }
                    
                   }
               }else if($catid != 'all'){
                   $total_rec = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'Q')
                ->where('posts.categoryid', '=', $catid)           
                ->orderBy('postid', 'desc')
                ->count();
                   if($total_rec > 0){
                   $data = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'Q')
                ->where('posts.categoryid', '=', $catid)           
                ->orderBy('postid', 'desc')
                ->paginate($limit);
                   }
               }else{
                   $total_rec = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'Q')
                ->whereRaw('FIND_IN_SET('.intval($tagid).',tag_ids)')           
                ->orderBy('postid', 'desc')
                ->count();
                   if($total_rec > 0){
                   $data = DB::table('posts')->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->where('type', '=', 'Q')
                ->whereRaw('FIND_IN_SET('.intval($tagid).',tag_ids)')           
                ->orderBy('postid', 'desc')
                ->paginate($limit);
                   }
               }
               
	    return $data;
	}

	public function getSortedQuestions($sort, $limit = 10, $catid = 'all', $tagid = 'all'){
               
                $w_field = 'postid';
                $w_value = '0';
                $data = array();
		if($sort=="hot"){
			//$sorting="hotness";
                        $sorting="views";
			$order="desc";
			$head="Hot questions";
		}
		elseif($sort=="votes"){
			$sorting="netvotes";
			$order="desc";
			$head="Highest voted questions";
		}
		elseif($sort=="answers"){
			$sorting="acount";
			$order="desc";
			$head=" Most answered questions";
		}
                elseif($sort=="flagged"){
			$sorting="flagged";
			$order="desc";
			$head=" Flagged questions";
                        $w_field = 'flagged';
		}
		else{
			$sorting="postid";
			$order="desc";	
		}
		
		if($catid == 'all' and $tagid == 'all'){
                    $total_rec = DB::table('posts')
                ->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))            
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->orderBy($sorting, $order)
                ->where($w_field, '>', $w_value)
                ->where('type', '=', 'Q')           
                ->count();
                    if($total_rec > 0){
                    $data = DB::table('posts')
                ->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))            
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->orderBy($sorting, $order)
                ->where($w_field, '>', $w_value)            
                ->where('type', '=', 'Q')
                ->paginate($limit);
                    }
               }else if($catid != 'all'){
                   $total_rec = DB::table('posts')
                ->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))            
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->orderBy($sorting, $order)
                ->where($w_field, '>', $w_value)           
                ->where('type', '=', 'Q')
                ->where('posts.categoryid', '=', $catid)             
                ->count();
                   if($total_rec > 0){
                   $data = DB::table('posts')
                ->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))            
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->orderBy($sorting, $order)
                ->where($w_field, '>', $w_value)           
                ->where('type', '=', 'Q')
                ->where('posts.categoryid', '=', $catid)             
                ->paginate($limit);
                   }
               }else{
                   $total_rec = DB::table('posts')
                ->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))            
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->orderBy($sorting, $order)
                ->where($w_field, '>', $w_value)           
                ->where('type', '=', 'Q')
                ->whereRaw('FIND_IN_SET('.intval($tagid).',tag_ids)')            
                ->count();
                   if($total_rec > 0){
                   $data = DB::table('posts')
                ->select(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
                                    'categories.backpath as categorybackpath',
                                    'userpoints.points',
                                    'users.flags',
                                    'users.level',
                                    'users.email',
                                    'users.handle',
                                    'users.user_picture',
                                    'users.usertype',
                                    'users.url_key as user_url_key',
                                    'users.avatarblobid',
                                    'users.avatarwidth',
                                    'users.avatarheight'
                                    ))            
                ->leftjoin('users', 'users.userid', '=', 'posts.userid')
                ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
                ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
                ->orderBy($sorting, $order)
                ->where($w_field, '>', $w_value)           
                ->where('type', '=', 'Q')
                ->whereRaw('FIND_IN_SET('.intval($tagid).',tag_ids)')            
                ->paginate($limit);
                   }
               } 
	    
	    
	    return $data;
	}

	public function getUnanswered(){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$data = DB::table('posts')
	    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
	    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
	    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
	    ->orderBy('postid', 'desc')
	    ->where('type', '=', 'Q')
	    ->where('acount', '=' , 0)
	    ->get(array('posts.*', 'categories.title as categoryname', 
	    			'categories.backpath as categorybackpath',
	    			'userpoints.points',
	    			'users.flags',
	    			'users.level',
	    			'users.email',
	    			'users.handle',
	    			'users.avatarblobid',
	    			'users.avatarwidth',
	    			'users.avatarheight'
	    			));
	    DB::setFetchMode(PDO::FETCH_CLASS);
	    return $data;
	}

	public function getTags($alpha, $limit = 'all'){
            
            if($limit == 'all'){
                $data = DB::table('tags')->where('name', 'LIKE', $alpha.'%')->orderBy('name', 'asc')->paginate(50);
            }else {
                $data = DB::table('tags')->where('name', 'LIKE', $alpha.'%')->orderBy('name', 'asc')->take($limit)->get();
            }
            return $data;
	}
        
        
        
        function getTagsPopular(){
            $data = DB::table('tags')->where('usage', '>=', 5)->orderBy('usage', 'desc')->take(200)->get();
            return $data;
        } 

	//Function to calculate time since question created
	public static function formattedCreatedDate($creat) {
		$created = new Carbon($creat);
		//$now = Carbon::now();
		$daysSinceEpoch = Carbon::create(2014, 12, 04, 15, 20, 31)->diffForHumans();
		//echo $daysSinceEpoch;die;
                $returndate = $created->diffForHumans();
                $returndate = str_replace('ago', 'sedan', $returndate);
                $returndate = str_replace('days', 'dagar', $returndate);
                $returndate = str_replace('day', 'dag', $returndate);
                $returndate = str_replace('weeks', 'veckor', $returndate);
                $returndate = str_replace('week', 'vecka', $returndate);
                $returndate = str_replace('months', 'månader', $returndate);
                $returndate = str_replace('month', 'månad', $returndate);
                $returndate = str_replace('years', 'år', $returndate);
                $returndate = str_replace('year', 'år', $returndate);
                $returndate = str_replace('hours', 'timmar', $returndate);
                $returndate = str_replace('hour', 'timme', $returndate);
                $returndate = str_replace('minutes', 'minuteer', $returndate);
                $returndate = str_replace('minute', 'minut', $returndate);
                $returndate = str_replace('seconds', 'sekunder', $returndate);
                $returndate = str_replace('second', 'sekund', $returndate);
		return $returndate;
	}

	public function getSingleQuestion($postid){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$data = DB::table('posts')
	    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
	    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
	    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
	    ->where('postid', '=' , $postid)
	    ->first(array('posts.*', 'categories.title as categoryname', 'categories.url_key as cat_url_key',
	    			'categories.backpath as categorybackpath',
	    			'userpoints.points',
	    			'users.flags',
	    			'users.level',
	    			'users.email',
	    			'users.handle',
                                'users.user_picture',
                                'users.usertype',
                                'users.url_key as user_url_key',
	    			'users.avatarblobid',
	    			'users.avatarwidth',
	    			'users.avatarheight'
	    			));
	    DB::setFetchMode(PDO::FETCH_CLASS);
	    return $data;
	}

	public static function qa_qs_sub_navigation($sort, $categoryslugs) {
		$request='questions';
		/*if (isset($categoryslugs)){
			foreach ($categoryslugs as $slug) {
				$request.='/'.$slug;
			}
		}*/

		$navigation=array(
			'recent' => array(
				'label' => "Recent",
				'url' => URL::to("/question"),
			),
			
			'hot' => array(
				'label' => "Hot!",
				'url' => URL::to("/question/sort/hot"),
			),
			
			'votes' => array(
				'label' => "Most votes",
				'url' => URL::to("/question/sort/votes"),
			),

			'answers' => array(
				'label' => "Most answers",
				'url' => URL::to("/question/sort/answers"),
			),

			'views' => array(
				'label' => "Most views",
				'url' => "#",
			),
		);
		
		
		return $navigation;
	}

	public static function qa_unanswered_sub_navigation($by, $categoryslugs){
		$request='unanswered';

	/*	if (isset($categoryslugs)){
			foreach ($categoryslugs as $slug){
				$request.='/'.$slug;
			}
		}*/
		
		$navigation=array(
			'by-answers' => array(
				'label' => "No answer",
				'url' => "#",
			),
			
			'by-selected' => array(
				'label' => "No selected answer",
				'url' => "#",
			),
			
			'by-upvotes' => array(
				'label' => "No upvoted answer",
				'url' => "#",
			),
		);
		
		
		return $navigation;
	}

	//Getting post by its id
	public function getPostById($id){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$record=DB::table('posts')
            ->where('postid', $id)
            ->get();
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $record[0];
	}

	public function getSearchQuestions($qa){//echo $qa;die;
		//$categoryids="CONCAT_WS(',', ^posts.catidpath1, ^posts.catidpath2, ^posts.catidpath3, ^posts.categoryid)";
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$data = DB::table('posts')
	    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
	    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
	    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
	    ->orderBy('postid', 'desc')
	    ->where('type', '=', 'Q')
	    ->where('posts.content', 'LIKE', '%' . $qa . '%')
	    ->orWhere('posts.title', 'LIKE', '%' . $qa . '%')
	    ->orWhere('posts.tags', 'LIKE','%'. $qa . '%')
	    ->get(array('posts.*', 'categories.title as categoryname', 
	    			'categories.backpath as categorybackpath',
	    			'userpoints.points',
	    			'users.flags',
	    			'users.level',
	    			'users.email',
	    			'users.handle',
	    			'users.avatarblobid',
	    			'users.avatarwidth',
	    			'users.avatarheight'
	    			));
	    DB::setFetchMode(PDO::FETCH_CLASS);
	    return $data;
	}

	public function SearchQuestionsByTag($tag){
		//$categoryids="CONCAT_WS(',', ^posts.catidpath1, ^posts.catidpath2, ^posts.catidpath3, ^posts.categoryid)";
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$data = DB::table('posts')
	    ->leftjoin('users', 'users.userid', '=', 'posts.userid')
	    ->leftjoin('userpoints', 'userpoints.userid', '=', 'posts.userid')
	    ->leftjoin('categories', 'categories.categoryid', '=', 'posts.categoryid')
	    ->orderBy('postid', 'desc')
	    ->where('type', '=', 'Q')
	    ->where('posts.tags', '=', $tag)
	    ->get(array('posts.*', 'categories.title as categoryname', 
	    			'categories.backpath as categorybackpath',
	    			'userpoints.points',
	    			'users.flags',
	    			'users.level',
	    			'users.email',
	    			'users.handle',
	    			'users.avatarblobid',
	    			'users.avatarwidth',
	    			'users.avatarheight'
	    			));
	    DB::setFetchMode(PDO::FETCH_CLASS);
	    return $data;
	}
        
        function getCategories($alpha, $limit, $keyword= NULL){
            if($limit == 'all'){ 
                if($keyword){
                    $alpha = $keyword;
                }
                $data = DB::table('categories')->where('title', 'LIKE', $alpha.'%')->orderBy('title', 'asc')->paginate(50);
            }else {
                $data = DB::table('categories')->where('title', 'LIKE', $alpha.'%')->orderBy('title', 'asc')->take($limit)->get();
            }
            return $data;
        } 
        
        function getCategoriesPopular(){
            $data = DB::table('categories')->where('qcount', '>=', 100)->orderBy('qcount', 'desc')->take(50)->get();
            return $data;
        } 
        
        

}