<?php

class SphinxController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Sphinx Controller
	|--------------------------------------------------------------------------
	|
	*/
    public function index(){
        
        $page = 1;
        $perPage = 10;
        $data['questions'] = array();
        $data['basic']['sort_type'] = '';
        $data['basic']['head'] = '';
        $data['basic']['page_limit'] = $perPage;
        $data['basic']['catid'] = '';
        $data['basic']['base_url'] = '';
        $data['basic']['page'] = $page;
        $data['total_question'] = 0;
        $data['keywords'] = '';
        
        $params = App::make('router')->getCurrentRoute()->getParameters();
        
        if(Request::segment(2) == 'search' and count($params) > 0){
            $keywords = $params['any'];
            $data['keywords'] = $keywords;
            $data['basic']['head'] = $keywords;
            $data['basic']['base_url'] = '/questions/search/'.$keywords;
            if(isset($params['page'])){
                $data['basic']['page'] = $params['page'];
                $page = $params['page'];
            }
            if(isset($params['number'])){
                $data['basic']['page_limit'] = $params['number'];
            }
            
            $results = SphinxSearch::search($keywords, 'questions')
                       ->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "@weight DESC") 
                       ->limit($data['basic']['page_limit'], ($page-1)* $data['basic']['page_limit'])
                       ->get();
            //echo '<pre>'; print_r($results); echo '</pre>'; die;
            if(!empty($results['total'])) {
                $data['total_question'] = $results['total'];
               $data['questions'] = $results['matches'];
            }
        }
        
        if(BrowserDetect::isMobile() == TRUE){
            return View::make('mobile.sphinx')->with('data', $data);
        }else{
            return View::make('front.sphinx')->with('data', $data);
        }
        
        
    }
    
    function doSearch(){
        if(Request::segment(2) == 'search'){
            $request_uri = $_SERVER['REQUEST_URI'];
            $request_uri = explode('?keyword=', $request_uri); 
            if (count($request_uri) > 1 and $request_uri[1] != "") {
                $keywords = $request_uri[1];
                return Redirect::to('questions/search/'.$keywords);
            }
        }
    }
}