<?php

class AjaxController extends BaseController {

    public function __construct() {
        
    }

    public function index() {
        $method = Input::get('method', 'upload_images');
        $content = Input::get('content', array());
        if ($method != "") {
            $this->$method($content);
        }
    }

    private function read($post) {

        $return = array('success' => false, 'code' => '401', 'msg' => 'Dina inloggningsuppgifter var felaktiga. Vänligen försök igen.');
        
        if (isset($post['action']) && $post['action'] == 'reset') {
            if(isset($post['user_key']) and $post['user_key']!="" and isset($post['user_login']) and $post['user_login']!="" and isset($post['new_pass']) and $post['new_pass']!=""){
                $user_information = DB::table('users')->select(array('email'))->where('activation_token', $post['user_key'])->where('email', $post['user_login'])->first();
                if(count($user_information) > 0){
                    $new_pass = Hash::make($post['new_pass']);
                    User::where('email', '=', $post['user_login'])->update(array('activation_token' => '', 'token_expiry' => '', 'password' => $new_pass));
                    $return['success'] = true;
                    $return['msg'] = 'Password has been reset successfully please login now.';
                    $return['redirect'] = '/';
                }else{
                    $return['msg'] = 'Sorry you are not allowed to perform this action.';
                }
            }
            
            
        }else if (isset($post['action']) && $post['action'] == 'forgot') {
            
            $rules = array(
                'user_login' => 'required|email'
            );
            $validator = Validator::make($post, $rules);
            if ($validator->fails()) {
                $return['msg'] = 'Please enter valid email address.';
            } else {                
                $activation_token = md5(time());
                $token_expiry = date('Y-m-d');
                $user = DB::table('users')->where('email', '=', $post['user_login'])->get();
                if (empty($user)) {
                    $return['msg'] = 'Your provided email address does not found, please try again with correct email address.';
                } else {                    
                    $_POST['user_login'] = $post['user_login'];
                    User::where('email', '=', $post['user_login'])->update(array('activation_token' => $activation_token, 'token_expiry' => $token_expiry));                    
                    
                    $data['user'] = array('token' => $activation_token, 'full_name' => $user[0]->username, 'email' => $user[0]->email);
                    Mail::send('includes.forgot_password', $data, function($message) use ($data) {
                        $message->to($data['user']['email'], $data['user']['full_name'])->subject('Forgot password Request from SvarpÃ¥allt Q&A!');
                    });
                    
                    $return['success'] = true;
                    $return['msg'] = 'Please check your email and follow the instructions.';
                }
            }
        } else {
            $rules = array(
                'username' => 'required', // make sure its required
                'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make($post, $rules);
            if ($validator->fails()) {
                $return['msg'] = 'Email Address/Password can not be blank! Please try again.';
            } else {
                try {
                    $user = array(
                        'email' => $post['username'],
                        'password' => $post['password'],
                        'verified_account' => 1
                    );
                    if (Auth::attempt($user)) {
                        //Session::put('user', $user);
                        Auth::login(Auth::user(), true);
                        $return['redirect'] = '/';
                        $return['success'] = true;
                        $return['msg'] = 'Please wait, we are redirecting...';
                    }
                } catch (ParseException $error) {
                    
                }
            }
        }

        echo json_encode($return);
    }

    private function create($post) {
        $action = Input::get('action');
        if(isset($action) && $action == 'et_post_sync'){
            $method = Input::get('do_action', 'postAnswer');
        }else{
            $method = Input::get('do_action', 'registerUser');
        }
        $this->$method($post);
    }
    
    private function update($post) {
        $do_action = Input::get('do_action', 'changePassword');
        $do_action = str_replace('-', '_', $do_action);
        if ($do_action != '') {
            if($do_action == 'saveQuestion'){
                $this->$do_action($post, 'U');
            }else{
                $this->$do_action($post);
            }
            
        }
    }
    
    private function remove($post) {
        $do_action = Input::get('do_action', 'savePost');
        if ($do_action != '') {
            if($do_action == 'savePost'){
                $this->$do_action($post, 'D');
            }else {
                $this->$do_action($post);
            }            
        }
    }
    
    private function inbox($post){
        $return = array('success' => false, 'code' => '401', 'msg' => 'Please enter message field.');
        if(isset($post['user_id']) && $post['user_id']!="" && isset($post['message']) && $post['message']!=""){
            if (Auth::check()) {
                $user_info = DB::table('users')->where('userid', $post['user_id'])->first();
                
                $data['user'] = array('full_name' => $user_info->handle, 'message' => $post['message'], 'email' => $user_info->email);
                Mail::send('includes.contact_email', $data, function($message) use ($data) {
                    
                    $message->to($data['user']['email'], 'Dear User')->subject('Contact request from SvarpÃ¥allt Q&A!');
                });
                
                $return['msg'] = 'Ditt meddelande har skickats.';
                $return['success'] = true;
            }else{
                $return['msg'] = 'You are not allowed to perform this action.';
            }
        }
        echo json_encode($return);
        
    }

    private function saveProfile($post){
        
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        $rules = array(
            'full_name' => 'required',
            'location' => 'required'
        );
        // run the validation rules on the inputs from the form

        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
            $return['msg'] = 'Full Name / Location can not be left blank.';
        } else {
            try {
                $email_me = 'n';
                if(isset($post['email_me']) and $post['email_me'] == 'y'){
                    $email_me = 'y';
                }
                $post_me = 'n';
                if(isset($post['post_me']) and $post['post_me'] == 'y'){
                    $post_me = 'y';
                }
                $user = Auth::user();
                if ($user) {
                    $user->handle = $post['full_name'];
                    $user->username = $post['full_name'];
                    $user->location = $post['location'];
                    $user->website = $post['website'];
                    $user->description = $post['description'];
                    $user->email_me = $email_me;
                    $user->post_me = $post_me;
                    $user->save();
                    $return['success'] = true;
                    $return['redirect'] = '/';
                    $return['msg'] = 'Your Profile has been updated.';
                }
            } catch (ParseException $error) {
                $return['msg'] = 'Ogiltig användarinformation!';
            }
        }
        echo json_encode($return);
    }
    
    private function change_logo($post){
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        if (Auth::check()) {             
            if (Input::hasFile('user_avatar')){
                $ImageType = $_FILES['user_avatar']['type'];
                switch(strtolower($ImageType))
                {
                    case 'image/png':
                            //Create a new image from file 
                            $CreatedImage =  imagecreatefrompng($_FILES['user_avatar']['tmp_name']);
                            break;
                    case 'image/gif':
                            $CreatedImage =  imagecreatefromgif($_FILES['user_avatar']['tmp_name']);
                            break;			
                    case 'image/jpeg':
                    case 'image/pjpeg':
                            $CreatedImage = imagecreatefromjpeg($_FILES['user_avatar']['tmp_name']);
                            break;
                    default:
                    die('Unsupported File!'); //output error and exit
                }                
                $extension = Input::file('user_avatar')->getClientOriginalExtension();                 
		$fileName = md5(time()).'.'.$extension;
                $destinationPath = public_path().'/qaengine/img/';
                $uploaded_image = Input::file('user_avatar')->move($destinationPath, $fileName);                
                list($CurWidth,$CurHeight)=getimagesize($uploaded_image);
                
                $ThumbSquareSize = 80;
		$thumb_DestRandImageName = $destinationPath.'/thumb_'.$fileName;
		$Quality = 90;
		$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
                
                $ThumbSquareSize = 65;
                $thumb_DestRandImageName = $destinationPath.'/avatar_'.$fileName;
                $Quality = 90;
                $this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
                
                $old_picture = Auth::user()->user_picture;			
                if($old_picture!="/qaengine/img/default_picture.png"){
                    $destinationPath = str_replace('/qaengine/img/', '', $destinationPath);
                    File::delete($destinationPath.$old_picture);
                    File::delete($destinationPath.'avatar_'.$old_picture);
                    File::delete($destinationPath.'thumb_'.$old_picture);
                }
                $user = Auth::user();
                $user->user_picture = '/qaengine/img/avatar_'.$fileName;
                $user->user_picture_main = '/qaengine/img/'.$fileName;
                $user->user_picture_profile = '/qaengine/img/thumb_'.$fileName;
                $user->save();
                $return['success'] = true;
            }else{
                $return['msg'] = 'Please upload profile picture.';
            }
        }else{
            $return['msg'] = 'You are not allowed to perform this action, please login/sign up.';
        }
        echo json_encode($return);
    }

    private function vote_up($post){
        $return = array('success' => false, 'code' => '401', 'msg' => 'Please try again.');
        $vote_id = Input::get('ID');
        if($vote_id){
            if(Auth::check()){
                $userid = Auth::user()->userid;
                $is_voted = DB::table('uservotes')->where('postid', $vote_id)->where('userid', $userid)->count();
                if($is_voted == 0){
                    DB::table('posts')->where('postid', $vote_id)->increment('upvotes');
                    DB::table('posts')->where('postid', $vote_id)->increment('netvotes');
                    $votes = DB::table('posts')->where('postid', $vote_id)->first();
                    if($votes){
                        if($votes->parentid == NULL or $votes->parentid == '' or $votes->parentid == '0'){
                            DB::table('posts')->where('postid', $vote_id)->increment('hotness');
                        }else{
                            DB::table('posts')->where('postid', $votes->parentid)->increment('hotness');
                        }
                    }
                    
                    $return['et_vote_count'] = $votes->netvotes;
                    $return['success'] = true;
                
                    DB::table('uservotes')->insert(
                        array('postid' => $vote_id, 'userid' => $userid, 'vote' => 1, 'flag' => 1, 'created' => date('Y-m-d H:i:s'))
                    );
                    
                    if($userid!=""){
                        $user_info = DB::table('users')->select(array('points'))->where('userid', $userid)->first();
                        $updated_points = $user_info->points + 1;
                        $usertype = 0;
                        if($updated_points >= 50 and $updated_points <= 249){
                            $usertype = 1;
                        }
                        if($updated_points >= 250 and $updated_points <= 499){
                            $usertype = 2;
                        }
                        if($updated_points >= 500 and $updated_points <= 999){
                            $usertype = 3;
                        }
                        if($updated_points >= 1000 and $updated_points <= 2999){
                            $usertype = 4;
                        }
                        if($updated_points >= 3000){
                            $usertype = 5;
                        }

                        DB::table('users')->where('userid', $userid)->update(array('points' => $updated_points, 'usertype' => $usertype));
                    }
                    
                }else{
                   $return['msg'] = 'You have been voted already.';
                }
            }else{
                $return['msg'] = 'Du måste vara inloggad för att utföra denna åtgärd.';
            }
        }
        echo json_encode($return);
    }

    private function vote_down($post){
        $return = array('success' => false, 'code' => '401', 'msg' => 'Please try again.');
        $vote_id = Input::get('ID');
        if($vote_id){
            if(Auth::check()){
                $userid = Auth::user()->userid;
                $is_voted = DB::table('uservotes')->where('postid', $vote_id)->where('userid', $userid)->count();
                if($is_voted == 0){
                    DB::table('posts')->where('postid', $vote_id)->increment('downvotes');
                    DB::table('posts')->where('postid', $vote_id)->decrement('netvotes');
                    $votes = DB::table('posts')->where('postid', $vote_id)->first();
                    $return['et_vote_count'] = $votes->netvotes;
                    $return['success'] = true;
                    DB::table('uservotes')->insert(
                        array('postid' => $vote_id, 'userid' => $userid, 'vote' => 1, 'flag' => 0, 'created' => date('Y-m-d H:i:s'))
                    );
                    
                    if($userid!=""){
                        $user_info = DB::table('users')->select(array('points'))->where('userid', $userid)->first();
                        $updated_points = $user_info->points + 1;
                        $usertype = 0;
                        if($updated_points >= 50 and $updated_points <= 249){
                            $usertype = 1;
                        }
                        if($updated_points >= 250 and $updated_points <= 499){
                            $usertype = 2;
                        }
                        if($updated_points >= 500 and $updated_points <= 999){
                            $usertype = 3;
                        }
                        if($updated_points >= 1000 and $updated_points <= 2999){
                            $usertype = 4;
                        }
                        if($updated_points >= 3000){
                            $usertype = 5;
                        }

                        DB::table('users')->where('userid', $userid)->update(array('points' => $updated_points, 'usertype' => $usertype));
                    }
                    
                }else{
                    $return['msg'] = 'You have been voted already.';
                }    
            }else{
                $return['msg'] = 'Du måste vara inloggad för att utföra denna åtgärd';
            }
        }
        echo json_encode($return);
    }
    
    private function insert_comment($post){
        $return = array('success' => false, 'code' => '401', 'msg' => 'Please enter comments.');
        if($post['comment_post_ID']!="" and $post['user_id']!="" and $post['post_content']!=""){
            $comment_date = date('Y-m-d H:i:s');
            $data['type'] = 'C';
            $data['parentid'] = $post['comment_post_ID'];
            $data['userid'] = $post['user_id'];
            $data['content'] = $post['post_content'];
            $data['created'] = $comment_date;
            //DB::table('posts')->insert($data);
            $commentId = DB::table('posts')->insertGetId($data);
            
            $parent_post_info = DB::table('posts')->where('postid', $post['comment_post_ID'])->first();
            if($parent_post_info){
                if($parent_post_info->parentid == NULL or $parent_post_info->parentid == '' or $parent_post_info->parentid == '0'){
                    DB::table('posts')->where('postid', $post['comment_post_ID'])->increment('hotness');
                }else{
                    DB::table('posts')->where('postid', $parent_post_info->parentid)->increment('hotness');
                }
            }
            
            $comment_author = '';
            $comment_author_email = '';
            $comment_author_url = '';
            $comment_user_id = NULL;
            $comment_author_url = '';
            $avatar = '';
            if(Auth::check()){
                $comment_user_id = Auth::user()->userid;
                $comment_author = Auth::user()->username;
                $comment_author_email = Auth::user()->email;
                $comment_author_url = '/user/'.Auth::user()->url_key;
                $avatar = Auth::user()->user_picture;
            }
            $human_date = Question::formattedCreatedDate($comment_date);
            $return_data = array("comment_ID" => $commentId,
                                "comment_post_ID" => $post['comment_post_ID'],
                                "comment_author" => $comment_author,
                                "comment_author_email" => $comment_author_email,
                                "comment_author_url" => $comment_author_url,
                                "comment_author_IP" => $_SERVER['REMOTE_ADDR'],
                                "comment_date" => $comment_date,
                                "comment_date_gmt" => $comment_date,
                                "comment_content" => $post['post_content'],
                                "comment_karma" => "0",
                                "comment_approved" => "1",
                                "comment_agent" => "",
                                "comment_type" => "answer",
                                "comment_parent" => "0",
                                "user_id" => $comment_user_id,
                                "id" => $commentId,
                                "et_votes" => array(),
                                "et_votes_count" => 1,
                                "content_filter" => $post['post_content'],
                                "content_edit" => $post['post_content'],
                                "avatar" => '<img src="'.$avatar.'" class="avatar" alt="" />',
                                "human_date" => $human_date,
                                "total_childs" => "Comment(0) ",
                                "new_nonce" => "21f792ae9f",
                                "author" => $comment_author,
                                "author_url" => $comment_author_url
                            );
            unset($return['code']);
            $return['success'] = true;            
            $return['msg'] = 'Comment has been created successfully.';
            $return['data'] = $return_data;
        }   
        echo json_encode($return);
    }
    
    private function saveComment(){
        $return = array('success' => false, 'code' => '401', 'msg' => 'Please enter comments.');
        $post = Input::get();
        
        if($post['comment_content']!="" and $post['commentID']!=""){
            $commentId = str_replace('edit_comment_', '', $post['commentID']);
            $comment_data = DB::table('posts')->where('postid', $commentId)->first();
            if(count($comment_data) > 0){
                $comment_date = date('Y-m-d H:i:s');
                DB::table('posts')->where('postid', $commentId)->update(array('created' => $comment_date,'content' => $post['comment_content']));
                $human_date = Question::formattedCreatedDate($comment_date);
                $return_data = array("comment_ID" => $commentId,
                                "comment_post_ID" => $comment_data->parentid,
                                "comment_author" => '',
                                "comment_author_email" => '',
                                "comment_author_url" => '',
                                "comment_author_IP" => $_SERVER['REMOTE_ADDR'],
                                "comment_date" => $comment_date,
                                "comment_date_gmt" => $comment_date,
                                "comment_content" => $post['comment_content'],
                                "comment_karma" => "0",
                                "comment_approved" => "1",
                                "comment_agent" => "",
                                "comment_type" => "answer",
                                "comment_parent" => "0",
                                "user_id" => $comment_data->userid,
                                "id" => $commentId,
                                "et_votes" => array(),
                                "et_votes_count" => 1,
                                "content_filter" => $post['comment_content'],
                                "content_edit" => $post['comment_content'],
                                "avatar" => '',
                                "human_date" => $human_date,
                                "total_childs" => "Comment(0) ",
                                "new_nonce" => "21f792ae9f",
                                "author" => '',
                                "author_url" => ''
                            );
                unset($return['code']);
                $return['success'] = true;            
                $return['msg'] = 'Comment has been updated successfully.';
                $return['data'] = $return_data;
            }else{
                $return['msg'] = 'Sorry, You are not allowed to perform this action.';
            }
            
        }   
        echo json_encode($return);
    }
    
    private function deleteComment(){
        $return = array('success' => false, 'code' => '401', 'msg' => 'Sorry, You are not allowed to perform this action.');
        $comment_ID = Input::get('comment_ID');
        if($comment_ID!=""){
           $comment_data = DB::table('posts')->where('postid', $comment_ID)->first(); 
           if(count($comment_data) > 0){
               DB::table('posts')->where('postid', $comment_ID)->delete(); 
               unset($return['code']);
               $return['success'] = true;            
               $return['msg'] = 'Comment has been deleted successfully.';
           }
        }
        echo json_encode($return);
    }
    
    private function changePassword($post){
        
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        $rules = array(
            'current_password' => 'required',
            'password' => 'required|Confirmed',
            'password_confirmation' => 'required'
        );
        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
            $return['msg'] = 'Please enter all fields.';
        } else {
            $current_password = $post['current_password'];
            $password = $post['password'];
            $user = Auth::user();
            if (strlen($current_password) > 0 && !Hash::check($current_password, $user->password)) {
                $return['msg'] = 'Ditt lösenord matchar inte, vänligen försök på nytt.';
            } else {
                $user->password = Hash::make($password);
                $user->save();
                $return['msg'] = 'Your password has been changed.';
                $return['success'] = true;
            }
        }
        
        echo json_encode($return);
    }

    private function registerUser($post) {
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        $rules = array(
            'email' => 'required|unique:users',
            'password' => 'required',
            'username' => 'required'
        );
        // run the validation rules on the inputs from the form

        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
            $return['msg'] = 'Email Address is already register with us, Please try another email address.';
        } else {
            try {

                $activation_token = md5(time());
                $token_expiry = date('Y-m-d');
                
                $userId = DB::table('users')->insertGetId(
                        array('email' => $post['email'],
                            'passcheck' => Hash::make($post['password']),
                            'username' => $post['username'],
                            'password' => Hash::make($post['password']),
                            'handle' => $post['username'],
                            'created' => date('Y-m-d H:i:s'),
                            'level' => 0,
                            'verified_account' => 0,
                            'user_picture' => '/qaengine/img/default_picture.png',
                            'points' => 0,
                            'usertype' => 0,
                            'activation_token' => $activation_token,
                            'token_expiry' => $token_expiry,
                            'loggedin' => date('Y-m-d H:i:s'),
                            'loginip' => $_SERVER['REMOTE_ADDR'],
                            'createip' => $_SERVER['REMOTE_ADDR'])
                );
                DB::table('userpoints')->insertGetId(
                        array('userid' => $userId,
                            'points' => 100,
                        )
                );
                
                $url_key = Str::slug($post['username']).'-'.$userId;
                DB::table('users')->where('userid', $userId)->update(array('url_key' => $url_key));
                
                
                $data['user'] = array('token' => $activation_token, 'full_name' => $post['username'], 'email_address' => $post['email']);
                Mail::send('includes.confirmation_email', $data, function($message) use ($data) {
                    $message->to($data['user']['email_address'], $data['user']['full_name'])->subject('Welcome to SvarpÃ¥allt Q&A!');
                });                
                

                $user = array(
                    'email' => $post['email'],
                    'password' => $post['password']
                );
                if (Auth::attempt($user)) {
                    //Session::put('user', $user);
                    Auth::login(Auth::user(), true);
                    $return['success'] = true;
                    $return['msg'] = 'Please wait, we are redirecting...';
                    $return['redirect'] = '/user/'.$url_key;
                }
            } catch (ParseException $error) {
                $return['msg'] = 'Ogiltig användarinformation!';
            }
        }
        echo json_encode($return);
    }
    
    private function saveQuestion($post, $type = 'N') {
        //echo '<pre>';  print_r($post); echo '</pre>'; die;
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        $rules = array(
            'post_title' => 'required',
            'question_category' => 'required',
            'post_content' => 'required',
        );
        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
            $return['msg'] = 'Please enter require fields.';
        } else {
            try {
                if($type == 'U'){
                    
                    $postid = Input::get('ID');
                    $q_data = DB::table('posts')->where('postid', $postid)->first();
                    if(count($q_data) > 0){
                        $url_key = $q_data->url_key;
                        
                        $tag_ids = '';
                        if (isset($post['tags']) and count($post['tags']) > 0) {
                            foreach ($post['tags'] as $tag) {
                                $count = Tag::where('name', '=', $tag)->count();
                                if ($count > 0) {
                                    $tagdb = Tag::where('name', '=', $tag)->get();
                                    $tag_ids .= $tagdb[0]->id . ',';
                                    DB::table('tags')->where('id', $tagdb[0]->id)->increment('usage');
                                } else {
                                    $tag_url_key = Str::slug($tag);
                                    $tagdb = new Tag;
                                    $tagdb->name = $tag;
                                    $tagdb->url_key = $tag_url_key;
                                    $tagdb->usage = 1;
                                    $tagdb->save();
                                    $tag_ids .= $tagdb->id . ',';
                                }
                            }
                            $tag_ids = rtrim($tag_ids, ',');
                        }
                        $qemail_me = Input::get('qemail_me', 'n');
                        $update_post['title'] = $post['post_title'];
                        $update_post['content'] = '';
                        $update_post['categoryid'] = $post['question_category'];
                        $update_post['tag_ids'] = $tag_ids;
                        $update_post['qemail_me'] = $qemail_me;
                        DB::table('posts')->where('postid', $postid)->update($update_post);
                        $return['success'] = true;
                        $return['msg'] = 'Please wait, we are redirecting...';
                        $return['redirect'] =  '/question/'.$url_key;
                        
                    }else{
                        $return['msg'] = 'Sorry, you are not allowed to perform this action.';
                    }
                }else{
                
                $userid = NULL;
                $email = '';
                $categoryid = NULL;
                $name = '';
                $qa = '';
                if (Auth::check()) {
                    $userid = Auth::user()->userid;
                }
                if (isset($post['email']) && $post['email'] != "") {
                    $email = $post['email'];
                }

                $post_title = $post['post_title'];
                $categoryid = $post['question_category'];
                //$post_content = $post['post_content'];
                $post_content = '';

                if (isset($post['name']) && $post['name'] != "") {
                    $name = $post['name'];
                }

                if (isset($post['qa']) && $post['qa'] != "") {
                    $qa = $post['qa'];
                }
              
               
               $tag_ids = '';
                if(isset($post['tags']) and count($post['tags']) > 0){
                    foreach($post['tags'] as $tag){
                        $count = Tag::where('name', '=', $tag)->count(); 
                        if($count > 0){
                           $tagdb = Tag::where('name', '=', $tag)->get(); 
                           $tag_ids .= $tagdb[0]->id.',';
                           DB::table('tags')->where('id', $tagdb[0]->id)->increment('usage');
                        }else {
                            $tag_url_key = Str::slug($tag);    
                            $tagdb = new Tag;
                            $tagdb->name = $tag;
                            $tagdb->url_key = $tag_url_key;
                            $tagdb->usage = 1;
                            $tagdb->save();
                            $tag_ids .= $tagdb->id.',';
                        }
                    }
                    $tag_ids = rtrim($tag_ids, ',');
                }
                
                $qemail_me = Input::get('qemail_me', 'n');
                $url_key = Str::slug($post_title);
                $postid = DB::table('posts')->insertGetId(
                        array('type' => 'Q',
                            'createip' => $_SERVER['REMOTE_ADDR'],
                            'lastviewip' => $_SERVER['REMOTE_ADDR'],
                            'created' => date('Y-m-d H:i:s'),
                            'title' => $post_title,
                            'content' => $post_content,
                            'url_key' => $url_key,
                            'name' => $name,
                            'tags' => '',
                            'userid' => $userid,
                            'categoryid' => $categoryid,
                            'tag_ids' => $tag_ids,
                            'qemail_me' => $qemail_me,
                            'notify' => $email)
                );
                
                if($categoryid!=""){
                    DB::table('categories')->where('categoryid', $categoryid)->increment('qcount');
                }
                
                if($userid!=""){
                    $user_info = DB::table('users')->select(array('points'))->where('userid', $userid)->first();
                    $updated_points = $user_info->points + 2;
                    $usertype = 0;
                    if($updated_points >= 50 and $updated_points <= 249){
                        $usertype = 1;
                    }
                    if($updated_points >= 250 and $updated_points <= 499){
                        $usertype = 2;
                    }
                    if($updated_points >= 500 and $updated_points <= 999){
                        $usertype = 3;
                    }
                    if($updated_points >= 1000 and $updated_points <= 2999){
                        $usertype = 4;
                    }
                    if($updated_points >= 3000){
                        $usertype = 5;
                    }
                    
                    DB::table('users')->where('userid', $userid)->update(array('points' => $updated_points, 'usertype' => $usertype));
                }
                
                
                $return['success'] = true;
                $return['msg'] = 'Please wait, we are redirecting...';
                $return['redirect'] =  '/question/'.$url_key;
                } 
            } catch (ParseException $error) {
                
            }
        }
        echo json_encode($return);
    }
    
    private function postAnswer($post){
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        $rules = array(
            'post_parent' => 'required',
            'post_content' => 'required'
        );
        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
            $return['msg'] = 'Please enter answer field.';
        } else {
            try {
                $userid = NULL;
                $email = '';
                $categoryid = NULL;
                $name = '';
                $qa = '';
                $post_title = '';
                $tag_ids = '';
                if (Auth::check()) {
                    $userid = Auth::user()->userid;
                    $name = Auth::user()->username;
                    $email = Auth::user()->email;
                }
                $post_content = $post['post_content'];
                $post_parent = $post['post_parent'];
                
                
                
                $post_content = str_replace("width: 70%;", "", $post_content);
                
                
                $answerdate = date('Y-m-d H:i:s');
                $postid = DB::table('posts')->insertGetId(
                        array('type' => 'A',
                            'createip' => $_SERVER['REMOTE_ADDR'],
                            'lastviewip' => $_SERVER['REMOTE_ADDR'],
                            'created' => $answerdate,
                            'title' => $post_title,
                            'content' => $post_content,
                            'name' => $name,
                            'tags' => '',
                            'parentid' => $post_parent,
                            'userid' => $userid,
                            'categoryid' => $categoryid,
                            'tag_ids' => $tag_ids,
                            'notify' => $email)
                );
                DB::table('posts')->where('postid', $post_parent)->increment('acount');
                DB::table('posts')->where('postid', $post_parent)->increment('hotness');
                
                $main_post_data = DB::table('posts')->select(array('title', 'userid', 'url_key'))->where('postid', $post_parent)->first();
                
                if(count($main_post_data) > 0){
                    $main_user_data = DB::table('users')->select(array('username', 'email'))->where('userid', $main_post_data->userid)->first();
                    $email_data['user'] = array('full_name' => $main_user_data->username, 'email' => $main_user_data->email, 'question_title' => $main_post_data->title, 'question_url' => $main_post_data->url_key);
                    Mail::send('includes.answer_email', $email_data, function($message) use ($email_data) {
                        $message->to($email_data['user']['email'], $email_data['user']['full_name'])->subject('Ett svar på din fråga har registrerats på Svar på Allt!');
                    });
                }
                
                
                if($userid!=""){
                    $user_info = DB::table('users')->select(array('points'))->where('userid', $userid)->first();
                    $updated_points = $user_info->points + 2;
                    $usertype = 0;
                    if($updated_points >= 50 and $updated_points <= 249){
                        $usertype = 1;
                    }
                    if($updated_points >= 250 and $updated_points <= 499){
                        $usertype = 2;
                    }
                    if($updated_points >= 500 and $updated_points <= 999){
                        $usertype = 3;
                    }
                    if($updated_points >= 1000 and $updated_points <= 2999){
                        $usertype = 4;
                    }
                    if($updated_points >= 3000){
                        $usertype = 5;
                    }

                    DB::table('users')->where('userid', $userid)->update(array('points' => $updated_points, 'usertype' => $usertype));
                }
                
                $comment_author = '';
                $comment_author_email = '';
                $comment_author_url = '';
                $comment_user_id = NULL;
                $comment_author_url = '';
                $avatar = '';
                if(Auth::check()){
                    $comment_user_id = Auth::user()->userid;
                    $comment_author = Auth::user()->username;
                    $comment_author_email = Auth::user()->email;
                    $comment_author_url = '/user/'.Auth::user()->url_key;
                    $avatar = Auth::user()->user_picture;
                }
                $human_date = Question::formattedCreatedDate($answerdate);
                $return_data = array(
                    "ID" => $postid,
                    "post_author" => $comment_user_id,
                    "post_date" => $answerdate,
                    "post_date_gmt" => $answerdate,
                    "post_content" => $post_content,
                    "post_title" => "RE: ".$main_post_data->title,
                    "post_excerpt" => "",
                    "post_status" => "publish",
                    "comment_status" => "open",
                    "ping_status" => "open",
                    "post_password" => "",
                    "post_name" => "/question/re-".$main_post_data->url_key,
                    "to_ping" => "",
                    "pinged" => "",
                    "post_modified" => $answerdate,
                    "post_modified_gmt" => $answerdate,
                    "post_content_filtered" => "",
                    "post_parent" => $post_parent,
                    "guid" => "/question/".$main_post_data->url_key,
                    "menu_order" => 0,
                    "post_type" => "answer",
                    "post_mime_type" => "",
                    "comment_count" => "0",
                    "filter" => "raw",
                    "et_vote_count" => 0,
                    "et_answer_authors" => array(),
                    "et_answers_count" => 0,
                    "et_vote_up_authors" => array(),
                    "et_vote_down_authors" => array(),
                    "et_is_best_answer" => "",
                    "id" => $postid,
                    "voted_down" => false,
                    "voted_up" => false,
                    "user_badge" => "",
                    "avatar" => '<img src="'.$avatar.'" class="avatar" alt="" />',
                    "new_nonce" => "21f792ae9f",
                    "human_date" => $human_date,
                    "content_filter" => $post_content,
                    "content_edit" => $post_content,
                    "parent_author" => $main_post_data->userid,
                    "comments" => "Comment(0) ",
                    "author_name" => $comment_author,
                    "author_url" => $comment_author_url,
                    "reported" => false
                );
                unset($return['code']);
                $return['success'] = true;
                $return['data'] = $return_data;
                $return['msg'] = 'Please wait, we are redirecting...';
                $q_data = DB::table('posts')->select(array('url_key'))->where('postid',$post_parent)->first();
                $return['redirect'] =  '/question/'.$q_data->url_key;
                
            } catch (ParseException $error) {
                
            }
        }
        echo json_encode($return);
    
    }
    
    private function savePost($post, $type = 'U'){
        $post = Input::get();
        $return = array('success' => false, 'code' => '401', 'msg' => '', 'redirect' => '');
        if(isset($post['ID']) and $post['ID']!=""){ 
            if($type == 'U'){ 
                if(isset($post['post_content']) and $post['post_content']!=""){
                    $answer_data = DB::table('posts')->where('postid', $post['ID'])->first();
                    if (count($answer_data) > 0) {
                        $answerdate = date('Y-m-d H:i:s');
                        DB::table('posts')->where('postid', $post['ID'])->update(array('created' => $answerdate, 'content' => $post['post_content']));
                        $human_date = Question::formattedCreatedDate($answerdate);
                        $return_data = array(
                            "ID" => $post['ID'],
                            "post_author" => $answer_data->userid,
                            "post_date" => $answerdate,
                            "post_date_gmt" => $answerdate,
                            "post_content" => $post['post_content'],
                            "post_title" => "",
                            "post_excerpt" => "",
                            "post_status" => "publish",
                            "comment_status" => "open",
                            "ping_status" => "open",
                            "post_password" => "",
                            "post_name" => "",
                            "to_ping" => "",
                            "pinged" => "",
                            "post_modified" => $answerdate,
                            "post_modified_gmt" => $answerdate,
                            "post_content_filtered" => "",
                            "post_parent" => $answer_data->parentid,
                            "guid" => "",
                            "menu_order" => 0,
                            "post_type" => "answer",
                            "post_mime_type" => "",
                            "comment_count" => "0",
                            "filter" => "raw",
                            "et_vote_count" => 0,
                            "et_answer_authors" => array(),
                            "et_answers_count" => 0,
                            "et_vote_up_authors" => array(),
                            "et_vote_down_authors" => array(),
                            "et_is_best_answer" => "",
                            "id" => $post['ID'],
                            "voted_down" => false,
                            "voted_up" => false,
                            "user_badge" => "",
                            "avatar" => '',
                            "new_nonce" => "21f792ae9f",
                            "human_date" => $human_date,
                            "content_filter" => $post['post_content'],
                            "content_edit" => $post['post_content'],
                            "parent_author" => $answer_data->parentid,
                            "comments" => "Comment(0) ",
                            "author_name" => '',
                            "author_url" => '',
                            "reported" => false
                        );
                        unset($return['code']);
                        unset($return['redirect']);
                        $return['success'] = true;
                        $return['data'] = $return_data;
                        $return['msg'] = 'Answer has been updated.';
                    }
                }
            }else{
                $post_data = DB::table('posts')->select(array('type'))->where('postid', $post['ID'])->first();
                $post_type = 'answer';
                unset($return['code']);
                unset($return['redirect']);
                $msg = 'Answer has been deleted successfully.';
                if($post_data->type == 'Q'){
                    $post_type = 'question';
                    $return['redirect'] = '/';
                    $msg = 'Question has been deleted successfully.';
                    $dep_posts_data = DB::table('posts')->select(array('postid'))->where('parentid', $post['ID'])->get();
                    if(count($dep_posts_data) > 0){
                        foreach($dep_posts_data as $p_data){
                            DB::table('posts')->where('parentid', $p_data->postid)->delete();
                        }
                    }else{
                        DB::table('posts')->where('parentid', $post['ID'])->delete();
                    }
                    
                }else {
                    DB::table('posts')->where('parentid', $post['ID'])->delete();
                }                
                DB::table('posts')->where('postid', $post['ID'])->delete();                
                
                $return['post_type'] = $post_type;            
                $return['success'] = true;            
                $return['msg'] = $msg;
            }
            
            
        }
        echo json_encode($return);
    }
    
    private function accept_answer($post){
        $return = array('success' => false, 'code' => '401', 'msg' => 'You are not allowed to perform this action.');
        $answer_id = Input::get('ID');
        if($answer_id){
            if(Auth::check()){
                $userid = Auth::user()->userid;
                $answer_info = DB::table('posts')->select(array('parentid', 'userid'))->where('postid', $answer_id)->first();
                if($answer_info->parentid!=NULL and $answer_info->parentid!=''){
                    
                    DB::table('posts')->where('postid', $answer_info->parentid)->where('userid', $userid)->update(array('best_answer_id' => $answer_id));
                    $userid = $answer_info->userid;
                    if($userid!=""){
                        $user_info = DB::table('users')->select(array('points'))->where('userid', $userid)->first();
                        $updated_points = $user_info->points + 3;
                        $usertype = 0;
                        if($updated_points >= 50 and $updated_points <= 249){
                            $usertype = 1;
                        }
                        if($updated_points >= 250 and $updated_points <= 499){
                            $usertype = 2;
                        }
                        if($updated_points >= 500 and $updated_points <= 999){
                            $usertype = 3;
                        }
                        if($updated_points >= 1000 and $updated_points <= 2999){
                            $usertype = 4;
                        }
                        if($updated_points >= 3000){
                            $usertype = 5;
                        }

                        DB::table('users')->where('userid', $userid)->update(array('points' => $updated_points, 'usertype' => $usertype));
                    }
                    $return['success'] = true;
                    $return['msg'] = 'Answer has been marked as best.';
                }
            }else{
                $return['msg'] = 'Du måste vara inloggad för att utföra denna åtgärd';
            }
        }
        echo json_encode($return);
    }
    
    private function cropImage($CurWidth, $CurHeight, $iSize, $DestFolder, $SrcImage, $Quality, $ImageType) {
        //Check Image size is not 0
        if ($CurWidth <= 0 || $CurHeight <= 0) {
            return false;
        }

        //abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
        if ($CurWidth > $CurHeight) {
            $y_offset = 0;
            $x_offset = ($CurWidth - $CurHeight) / 2;
            $square_size = $CurWidth - ($x_offset * 2);
        } else {
            $x_offset = 0;
            $y_offset = ($CurHeight - $CurWidth) / 2;
            $square_size = $CurHeight - ($y_offset * 2);
        }

        $NewCanves = imagecreatetruecolor($iSize, $iSize);
        if (imagecopyresampled($NewCanves, $SrcImage, 0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size)) {
            switch (strtolower($ImageType)) {
                case 'image/png':
                    imagepng($NewCanves, $DestFolder);
                    break;
                case 'image/gif':
                    imagegif($NewCanves, $DestFolder);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    imagejpeg($NewCanves, $DestFolder, $Quality);
                    break;
                default:
                    return false;
            }
            //Destroy image, frees memory	
            if (is_resource($NewCanves)) {
                imagedestroy($NewCanves);
            }
            return true;
        }
    }
    
    private function report($post){
        
        $return = array('success' => false, 'code' => '401', 'msg' => 'Sorry, You are not allowed to perform this action.');
        if(Auth::check()){
            $question_id = Input::get('ID');
            if(isset($question_id) and $question_id!=""){
                $question_data = DB::table('posts')->where('postid', $question_id)->first();
                if(count($question_data) > 0){
                    $data = Input::get('data');
                    if(is_array($data) and count($data) > 0){
                        if($data['message']!=""){
                            DB::table('posts')->where('postid', $question_id)->increment('flagged');
                            $question_title = Input::get('post_title');
                            $question_url = Input::get('guid');
                            $user_info = DB::table('users')->where('userid', $question_data->userid)->first();
                            $email_data['user'] = array('full_name' => $user_info->handle, 'message' => $data['message'], 'email' => $user_info->email, 'question_title' => $question_title, 'question_url' => $question_url);
                            
                            /*Mail::send('includes.report_email', $email_data, function($message) use ($email_data) {
                                $message->to($email_data['user']['email'], $email_data['user']['full_name'])->subject('Report request from SvarpÃ¥allt Q&A!');
                            });
                            
                            Mail::send('includes.report_email', $email_data, function($message) use ($email_data) {
                                $message->to('eirikslettengen@gmail.com', 'SvarpÃ¥allt Administrator')->subject('Report request from SvarpÃ¥allt Q&A!');
                            });*/
                            
                            DB::table('flagged')->insert(
                                array('question_id' => $question_id, 'user_id' => Auth::user()->userid, 'flagged_reasons' => $data['message'], 'added_date' => date('Y-m-d H:i:s'))
                            );
                
                            $return['msg'] = 'Ditt meddelande har skickats.';
                            $return['success'] = true;
                            
                        }
                    }
                }
            }
        }
        
        echo json_encode($return);
    }
    
    private function upload_images($post){
        //echo '<pre>'; print_r($_POST); echo '</pre>'; die;
        $return = array('success' => false, 'code' => '401', 'msg' => '');
        if (Auth::check()) {             
            if (Input::hasFile('images_upload')){
                $extension = Input::file('images_upload')->getClientOriginalExtension();
		$fileName = md5(time()).'.'.$extension;
                $destinationPath = public_path().'/qaengine/img/';
                Input::file('images_upload')->move($destinationPath, $fileName);  
                $uploaded_image = url('/', $parameters = array(), $secure = null).'/qaengine/img/'.$fileName;
                $return['data'] = $uploaded_image;
                $return['success'] = true;
            }else{
                $return['msg'] = 'Please upload image.';
            }
        }else{
            $return['msg'] = 'You are not allowed to perform this action, please login/sign up.';
        }
        echo json_encode($return);
    }
    
    private function get_all_string_between($string, $start, $end)
    {
        $result = array();
        $string = " ".$string;
        $offset = 0;
        while(true)
        {
            $ini = strpos($string,$start,$offset);
            if ($ini == 0)
                break;
            $ini += strlen($start);
            $len = strpos($string,$end,$ini) - $ini;
            $result[] = substr($string,$ini,$len);
            $offset = $ini+$len;
        }
        return $result;
    }
    
    private function parse_content($content){
        $result = $this->get_all_string_between($content, '[img]', '[/img]');
        if(count($result) > 0){
            foreach($result as $res){
             $text_need_to_rep = "[img]".$res."[/img]";
             $rep_text = '<img src="'.$res.'" >';
             $content = str_replace($text_need_to_rep, $rep_text, $content);   
            }
        } 
        return $content;
    }

}
