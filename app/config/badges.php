<?php

$user_level[0]['type'] = 'Ny medlem';
$user_level[0]['color'] = '#04aad4';

$user_level[1]['type'] = 'Nybörjare';
$user_level[1]['color'] = '#a35429';

$user_level[2]['type'] = 'Amatör';
$user_level[2]['color'] = '#450ead';

$user_level[3]['type'] = 'Veteran';
$user_level[3]['color'] = '#138209';

$user_level[4]['type'] = 'Elit';
$user_level[4]['color'] = '#ffbf00';

$user_level[5]['type'] = 'Professor';
$user_level[5]['color'] = '#780978';

return $user_level;