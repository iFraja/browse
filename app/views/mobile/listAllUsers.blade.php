@extends('mobile.layouts.default')
@section('content')
<!-- CONTAINER -->

<!-- TAGS BAR -->
<section class="tag-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <h1 class="title-page">Användare</h1>
            </div>
            <div class="col-md-8 col-xs-8">
                <form class="find-tag-form" action="/users" method="GET">
                    <i class="fa fa-chevron-circle-right"></i>
                    <input type="text" name="keyword" id="keyword" value="{{$data['keyword']}}" placeholder="Find a user ">
                </form>
            </div>
        </div>
    </div>
</section>
<!-- TAGS BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li @if($data['type'] == '') class="active" @endif>
                         <a href="/users"  >Namn</a>
                    </li>
                    <li @if($data['type'] == 'points') class="active" @endif>
                         <a href="/users/points"  >Points</a>
                    </li> 
                </ul>
            </div>
        </div>
    </div> 
</section>
<!-- MIDDLE BAR / END -->

@if($data['users'])
<!-- LIST USER -->
<section id="user-list">
    @foreach($data['users'] as $user)
    <div class="profile-user-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-3 padding-right-0">
                    <a href="/user/{{$user->url_key}}" class="profile-avatar">
                        <img src="{{$user->user_picture}}" class="avatar" alt="" />                        </a>
                </div>
                <div class="col-md-9 col-xs-9">
                    <div class="profile-wrapper">
                        <span class="user-name-profile">{{$user->username}}</span>
                        @if($user->location!="")
                        <span class="address-profile"><i class="fa fa-map-marker"></i> {{$user->location}} </span>
                        @endif
                        <span class="email-profile"><i class="fa fa-envelope"></i> 
                            @if($user->post_me == 'y')
                            <a href="mailto:{{$user->email}}" >{{$user->email}}</a>    
                            @else
                            E-postadress är dold.
                            @endif
                        </span>
                        <div class="list-bag-profile-wrapper user-list">
                            <span style="background-color:{{ Config::get('badges.'.$user->usertype.'.color') }};" class="user-badge" title="{{ Config::get('badges.'.$user->usertype.'.type') }}">{{ Config::get('badges.'.$user->usertype.'.type') }}</span>
                            <span class="point-profile"><span>{{$user->points}}<i class="fa fa-star"></i></span> @if($user->points > 0) Points @else Point @endif</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endforeach
</section>
<!-- LIST USER / END -->
<section class="list-pagination-wrapper">
    {{ $data['users']->links() }}
</section>
<!-- PAGINATIONS QUESTION / END -->
@endif
<!-- CONTAINER / END -->
@stop