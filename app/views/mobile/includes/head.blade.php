<title>Frågor & Svar</title>
<link rel="shortcut icon" href=""/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href='//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
<script type="text/javascript">
	ae_globals = {"ajaxURL":"/admin-ajax","imgURL":"","posts_per_page":"12","homeURL":"/","user_confirm":0,"pending_questions":0,"pending_answers":0,"introURL":"/login","gplus_client_id":false,"plupload_config":{"max_file_size":"3mb","url":"/admin-ajax","flash_swf_url":"/qaengine\/qa-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"/qaengine\/qa-includes\/js\/plupload\/plupload.silverlight.xap"}}		
</script>
<meta name='robots' content='noindex,follow' />

<link rel='stylesheet' id='bootstrap-css'  href='/qaengine/css/libs/bootstrap.min.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='mobile-style-css'  href='/qaengine/mobile/css/main.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='/qaengine/css/libs/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/underscore.min.js?ver=1.6.0'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/backbone.min.js?ver=1.1.2'></script>
<script type='text/javascript' src='/qaengine/js/functions.js?ver=4.1'></script>
<script type='text/javascript'>
        /* <![CDATA[ */
        var qa_front = {"form_auth": {"error_msg": "Please fill out all fields required.", "error_user": "Please enter your user name.", "error_email": "Please enter a valid email address.", "error_username": "Please enter a valid username.", "error_repass": "Please enter the same password as above.", "error_url": "Please enter a valid URL.", "error_cb": "You must accept the term & privacy."}, "texts": {"require_login": "You must be logged in to perform this action.", "enought_points": "You don't have enought points to perform this action.", "create_topic": "Create Topic", "upload_images": "Ladda upp bild", "insert_codes": "Insert Code", "no_file_choose": "Ingen fil vald.", "require_tags": "Please insert at least one tag.", "add_comment": "Lägg till kommentar", "cancel": "Cancel", "sign_up": "Bli medlem", "sign_in": "Logga in", "accept_txt": "Accept", "best_ans_txt": "Best answer", "forgotpass": "Glömt lösenord?", "close_tab": "You have made some changes which you might want to save.", "confirm_account": "You must activate your account first to create questions \/ answers!.", "cancel_auth": "User cancelled login or did not fully authorize.", "banned_account": "You account has been banned, you can't make this action!"}};
        /* ]]> */
</script>
<script type='text/javascript' src='/qaengine/mobile/js/front.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/mobile/js/jquery.mouseweel.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/mobile/js/script-mobile.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/mobile/js/single-question.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/plupload/plupload.full.min.js?ver=2.1.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/qaengine/qa-includes/wlwmanifest.xml" /> 
