<!-- HEADDER -->
<header>
    <div class="wrapper-mobile">
        <div class="menu-btn">
            <i class="fa fa-bars"></i>
        </div>
        <div class="post-question-btn">
            <i class="fa fa-plus"></i>
        </div>
        <div class="logo">
            <a href="/">
                <img src="/qaengine/img/logo1.png" />
            </a>
        </div>
    </div>
</header>
<!-- HEADDER / END -->

<!-- POST QUESTION -->
<section class="post-question-form-wrapper">
    <form id="submit_question" action="">
        <input type="hidden" id="qa_nonce" name="qa_nonce" value="4c360ec74b">
        <input id="add_tag_text" type="hidden" value="You must have 1500 points to add tag. Current, you have to select existed tags." />
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-post">
                        <input type="text" name="post_title" id="question_title" placeholder="Din fråga">
                    </div>
                    <?php
                        $categories = DB::table('categories')->where('display_ask_question', 'y')->orderBy('title', 'ASC')->get();
                       // $categories = DB::table('categories')->where('parentid', NULL)->orWhere('parentid', 0)->take(50)->get();
                    ?>
                    <div class="form-post">
                        <div class="select-categories-wrapper">
                            <div class="select-categories">
                                <select class="select-grey-bg" id="" name="question_category">
                                    <option value="">Välj kategori</option>
                                    <?php foreach($categories as $cate){ ?>
                                         <option value="/category/<?php echo $cate->url_key; ?>" ><?php echo $cate->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-post" style="display: none">
                        <textarea name="post_content" id="" cols="30" rows="10" placeholder="Your Description">&nbsp;</textarea>
                    </div>
                    <div class="form-post">
                        <input  data-provide="typeahead" type="text" name="" id="question_tags" placeholder="Tagg (max 5 taggar)" />
                    </div>
                    <ul class="post-question-tags" id="tag_list"></ul>
                </div>
            </div>
        </div>
        <div class="group-btn-post">
            <div class="container">
                <div class="row">
                    <div class="col-xs-5"><span class="text">Ställ en fråga</span></div>
                    <div class="col-xs-7 text-right">
                        <button type="submit" class="submit-post-question">Submit</button>
                        <a href="javascript:void(0)" class="cancel-post-question">Avbryt</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<!-- POST QUESTION / END -->

<!-- MENU PUSH -->
<section class="menu-push">
    <div class="container">
        <div class="row">
            
            @if(Auth::check())
            <div class="col-md-3 col-xs-3">
                <div class="author-avatar">
                    <a href="/user/{{Auth::user()->url_key}}">
                        <img src="{{Auth::user()->user_picture}}" class="avatar" alt="" /> 
                    </a>
                </div>
            </div>
            <div class="col-md-9 col-xs-9">
                <div class="author-info">
                    <span class="author-name">{{Auth::user()->username}}</span>
                    <a href="/user/{{Auth::user()->url_key}}" class="setting-author"><i class="fa fa-cog"></i></a>
                </div>
            </div>
            <div class="clearfix"></div>
            @endif        
            <div class="line-menu-push first"></div>
            @if(Auth::check())
            <ul class="list-categories sign-out-link">
                <li>
                    <a href="/logout">Sign Out</a>
                </li>
            </ul>
            @else
            <ul class="list-categories sign-in-link">
                <li>
                    <a href="/login">Logga in</a>
                </li>
            </ul>
            @endif
            <div class="line-menu-push"></div>
            <ul class="list-categories list-menus">
                <li class="fa-question-circle menu-item menu-item-type-custom menu-item-object-custom"><a href="/questions"><i class="fa fa-question-circle"></i>Questions</a></li>
                <li class="fa-tags menu-item menu-item-type-post_type menu-item-object-page"><a href="/tags"><i class="fa fa-tags"></i>Tags</a></li>
                <li class="fa-tags menu-item menu-item-type-post_type menu-item-object-page"><a href="/badges"><i class="fa fa-trophy"></i>Titlar</a></li>
                <li class="fa-th-list menu-item menu-item-type-post_type menu-item-object-page"><a href="/categories"><i class="fa fa-th-list"></i>Kategorier</a></li>
                <li class="fa-users menu-item menu-item-type-post_type menu-item-object-page"><a href="/users"><i class="fa fa-users"></i>Användare</a></li>
                
                <li class="fa-th-list menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blog.merinfo.se/om-oss-merinfo-se" target="_blank"><i class="fa fa-th-list"></i>Om oss</a></li>
                <li class="fa-th-list menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blog.merinfo.se/kop-och-avtalsvillkor" target="_blank"><i class="fa fa-th-list"></i>Villkor</a></li>
                <li class="fa-th-list menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blog.merinfo.se/om-oss-merinfo-se" target="_blank"><i class="fa fa-th-list"></i>Kontakt</a></li>
            </ul>
            <div class="line-menu-push"></div>
            
        </div>
    </div>
</section>
<!-- MENU PUSH / END -->