@if(isset($data['question_detail_page']) and $data['question_detail_page'] === TRUE)
@else
<script type="text/javascript">
   currentQuestion = {}
</script>
@endif

<script type="text/template" id="tag_item">
    <input type="hidden" name="tags[]" value="{<?php echo '{= stripHTML(name) '; ?>}}" />
    {<?php echo '{= stripHTML(name) }}'; ?> <a href="javascript:void(0)" class="delete"><i class="fa fa-times"></i></a>
</script>

<script type="text/javascript">
    function stripHTML(html)
    {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText;
    }
</script>
<!--Customizer CSS-->
<style type="text/css"></style>
<!--/Customizer CSS-->
<script type='text/javascript' src='/qaengine/includes/aecore/assets/js/appengine.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/bootstrap.min.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/modernizr.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/jquery.simple-text-rotator.min.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/jquery.validate.min.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/core.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/widget.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/position.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/menu.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.2'></script>

<script type="text/javascript" id="frontend_scripts">
    (function ($) {
        $(document).ready(function () {
            if (typeof QAEngine.Views.MobileFront != 'undefined') {
                QAEngine.MobileApp = new QAEngine.Views.MobileFront();
            }
            if (typeof QAEngine.Views.MobileSingleQuestion != 'undefined') {
                QAEngine.MobileSingleQuestion = new QAEngine.Views.MobileSingleQuestion();
            }
        });
    })(jQuery);
</script>

<script type="text/javascript">
    _.templateSettings = {
        evaluate: /\<\#(.+?)\#\>/g,
        interpolate: /\{\{=(.+?)\}\}/g,
        escape: /\{\{-(.+?)\}\}/g
    };
</script>

<script type="text/javascript" id="frontend_scripts">
    (function ($) {
        $(document).ready(function () {

        });
    })(jQuery);
</script>

<script type="text/javascript" id="current_user">
@if(Auth::check())
    currentUser = {"ID":"{{Auth::user()->userid}}","user_login":"{{Auth::user()->username}}","user_nicename":"{{Auth::user()->username}}","user_email":"{{Auth::user()->email}}","user_url":"","user_registered":"{{Auth::user()->created}}","user_activation_key":"","user_status":"0","display_name":"{{Auth::user()->handle}}","et_avatar":"<img src=\"http:\/\/1.gravatar.com\/avatar\/b033185a7cebc82082f2cd177cc99c4f?s=64&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D64&amp;r=G\" class=\"avatar\" alt=\"tanveer\" \/>","qa_following_questions":"","user_facebook":"","user_twitter":"","user_gplus":"","user_location":"","register_status":"","key_confirm":"b033185a7cebc82082f2cd177cc99c4f","et_question_count":0,"et_answer_count":0,"description":"","qa_point":"1000","show_email":"","ban_expired":"January 1, 1970","is_ban":false,"id":"{{Auth::user()->userid}}","cap":{"vote_up":true,"add_comment":true,"vote_down":true,"create_tag":true,"edit_question":true,"edit_answer":true,"edit":true}}
@else
    currentUser = {"id":0,"ID":0}		
@endif
</script>