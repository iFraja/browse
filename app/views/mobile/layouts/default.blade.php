<!DOCTYPE html>
<html lang="en-US">
    <!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
    <!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->    
    <head>
        @include('mobile.includes.head')
    </head>
    <body class="home page page-template page-template-page-questions page-template-page-questions-php single-question">
        @include('mobile.includes.header')
        <!-- CONTAINER -->
        <div class="wrapper-mobile">
            @yield('content')
        </div>
        <!-- CONTAINER / END -->
        @include('mobile.includes.footer')
    </body>
</html>