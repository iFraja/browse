@extends('mobile.layouts.default')
@section('content')
<!-- TOP BAR -->
<section class="profile-user-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-3 padding-right-0">
                <a href="javascript:void(0);" class="profile-avatar">
                    <img src="{{$data['user'][0]->user_picture}}" class="avatar" alt="" />                    </a>
            </div>
            <div class="col-md-9 col-xs-9">
                <div class="profile-wrapper">
                    <span class="user-name-profile">{{$data['user'][0]->username}}</span>
                    <span class="address-profile">
                        <i class="fa fa-map-marker"></i>{{$data['user'][0]->location}}</span>
                    <span class="email-profile">
                        <i class="fa fa-envelope"></i>
                        @if($data['user'][0]->post_me == 'y')
                            <a href="mailto:{{$data['user'][0]->email}}" >{{$data['user'][0]->email}}</a>
                        @else
                             E-postadress är dold.
                        @endif
                    </span>
                    <div class="clearfix"></div>
                    <div class="description">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8 col-xs-8 padding-right-0">
                <div class="list-bag-profile-wrapper">
                    <span style="background-color:{{ Config::get('badges.'.$data['user'][0]->usertype.'.color') }};" class="user-badge" title="{{ Config::get('badges.'.$data['user'][0]->usertype.'.type') }}">{{ Config::get('badges.'.$data['user'][0]->usertype.'.type') }}</span>
                    <span class="point-profile">
                        <span>{{$data['user'][0]->points}}
                            <i class="fa fa-star"></i>
                        </span>@if($data['user'][0]->points > 0) points @else point @endif
                    </span>
                </div>
            </div>
            <div class="col-md-4 col-xs-4 padding-left-0">
                <div class="list-bag-profile-wrapper text-right">
                    <span class="question-profile">
                        {{$data['total_questions']}}<i class="fa fa-question-circle"></i>
                    </span>
                    <span class="answers-profile">
                        {{$data['total_answers']}}<i class="fa fa-comments"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- TOP BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li class="@if($data['type'] == 'question') active @endif" >
                        <a href="/user/{{$data['user'][0]->url_key}}">Questions</a>
                    </li>
                    <li class="@if($data['type'] == 'answer') active @endif" >
                        <a href="/user/{{$data['user'][0]->url_key}}/sort/answer">Answers</a>
                    </li>
                    <li class="@if($data['type'] == 'comment') active @endif" >
                        <a href="/user/{{$data['user'][0]->url_key}}/sort/comment">Comments</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="form-search-wrapper">
        <form id="form-search" class="collapse">
            <a href="javascript:void(0);" class="clear-text-search"><i class="fa fa-times-circle"></i></a>
            <a href="javascript:void(0);" class="close-form-search">Avbryt</a>
            <input type="text" name="" id="" placeholder="Enter keyword" class="form-input-search">
        </form>
    </div>
</section>
<!-- MIDDLE BAR / END -->

<!-- LIST QUESTION -->
<section class="list-question-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if($data['type'] == 'question')
                <ul class="list-question ">
                    @foreach($data['questions'] as $record)
                    <li class="post-{{{ $record->postid }}} question type-question status-publish hentry question-item" data-id="{{{ $record->postid }}}">
                        <div class="avatar-user">
                            <a href="/question/{{{ $record->url_key }}}">
                                <img src="{{$record->user_picture}}" class="avatar" alt="" />
                            </a>
                        </div> 
                        <div class="info-user" >
                            <span title="{{ Config::get('badges.'.$record->usertype.'.type') }}" class="user-badge">{{ Config::get('badges.'.$record->usertype.'.type') }}</span>
                            <ul class="info-review-question">
                                <li>{{$record->views}}<i class="fa fa-eye"></i></li>
                                <li>{{$record->acount}}<i class="fa fa-comments"></i></li>
                                <li>@if($record->netvotes > 0) {{$record->netvotes}} @else 0 @endif<i class="fa fa-chevron-circle-up"></i></li>
                            </ul>
                        </div>                        
                        <div class="content-question">
                            <h2 class="title-question">
                                <a href="/question/{{{ $record->url_key }}}">{{{ $record->title }}}</a>
                            </h2>
                            <div class="info-tag-time">
                                @if(!empty($record->tags) and count($record->tags) > 0)
                                <ul class="list-tag collapse">
                                    @foreach($record->tags as $tag)
                                    <li>
                                        <a href="/tag/{{ $tag->url_key }}">{{ $tag->name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                                <span class="time-categories">
                                    Asked by <a href="/user/{{$record->user_url_key}}">
                                        By 
                                    @if($record->handle ==NULL)
                                    me
                                    @else
                                    {{{ $record->handle }}}
                                    @endif
                                    </a> on {{ Question::formattedCreatedDate($record->created) }} i               <a href="/category/{{ $record->cat_url_key }}">{{ $record->categoryname }}</a>.
                                </span>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
               @elseif($data['type'] == 'answer')
               <ul class="list-question ">
                   @foreach($data['answer_questions'] as $record)
                   <li class="post-{{{ $record->postid }}} answer type-answer status-publish hentry question-item" data-id="{{{ $record->postid }}}">
                       <div class="avatar-user">
                           <a href="/user/{{{ $record->user_url_key }}}">
                               <img src="{{$record->user_picture}}" class="avatar" alt="" />        </a>
                       </div>
                       <div class="info-user" style="display: none">
                           <ul class="info-review-question">
                               <li>
                                   30<i class="fa fa-eye"></i>
                               </li>
                               <li class="active">
                                   3<i class="fa fa-check-circle-o"></i>
                               </li>
                               <li>
                                   1<i class="fa fa-chevron-circle-up"></i>
                               </li>
                           </ul>
                       </div>
                       <div class="content-question">
                           <h2 class="title-question">
                               <a href="/question/{{{ $record->url_key }}}">RE: {{{ $record->title }}}</a>
                           </h2>
                           <div class="info-tag-time">
                               @if(!empty($record->tags) and count($record->tags) > 0)
                               <ul class="list-tag">
                                   @foreach($record->tags as $tag)
                                        <li><a href="/tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                                    @endforeach
                               </ul>
                               @endif
                               <span class="time-categories">Asked {{ Question::formattedCreatedDate($record->created) }}.</span>
                           </div>
                           @foreach($data['answers'] as $record1)
                           @if($record1->parentid == $record->postid)
                           <div class="blockquote-wrapper">
                               <blockquote>
                                   <p>{{$record1->content}}</p>
                               </blockquote>
                           </div>
                           @endif
                        @endforeach 
                       </div>
                   </li>
                   @endforeach
               </ul>   
               @elseif($data['type'] == 'comment')
               <ul class="list-question ">
                   @foreach($data['answer_comments'] as $record)
                   <li class="post-{{{ $record->postid }}} answer type-answer status-publish hentry question-item" data-id="{{{ $record->postid }}}">
                       <div class="avatar-user">
                           <a href="/user/{{{ $record->user_url_key }}}">
                               <img src="{{$record->user_picture}}" class="avatar" alt="" />        </a>
                       </div>
                       <div class="info-user" style="display: none">
                           <ul class="info-review-question">
                               <li>
                                   30<i class="fa fa-eye"></i>
                               </li>
                               <li class="active">
                                   3<i class="fa fa-check-circle-o"></i>
                               </li>
                               <li>
                                   1<i class="fa fa-chevron-circle-up"></i>
                               </li>
                           </ul>
                       </div>
                       <div class="content-question">
                           <h2 class="title-question" style="font-size: 20px;">
                               @if($record->content)
                                {{{ strip_tags($record->content) }}} &#8230;
                                @else
                                &nbsp;
                            @endif
                           </h2>
                           <div class="info-tag-time">
                               @if(!empty($record->tags) and count($record->tags) > 0)
                               <ul class="list-tag">
                                   @foreach($record->tags as $tag)
                                        <li><a href="/tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                                    @endforeach
                               </ul>
                               @endif
                               <span class="time-categories">Asked {{ Question::formattedCreatedDate($record->created) }}.</span>
                           </div>
                           @foreach($data['comments'] as $record1)
                           @if($record1->parentid == $record->postid)
                           <div class="blockquote-wrapper">
                               <blockquote>
                                   <p>{{$record1->content}}</p>
                               </blockquote>
                           </div>
                           @endif
                        @endforeach 
                       </div>
                   </li>
                   @endforeach
               </ul>
               @endif
            </div>
        </div>
    </div>
</section>
<!-- LIST QUESTION / END -->
<section class="list-pagination-wrapper">
    <?php
    if ($data['type'] == 'comment') {
        echo $data['comments']->links();
    } else if ($data['type'] == 'answer') {
        echo $data['answers']->links();
    } else {
        echo $data['questions']->links();
    }
    ?>
</section>
<!-- PAGINATIONS QUESTION / END -->
@stop

