@extends('mobile.layouts.default')
@section('content')
<!-- TOP BAR -->
<section class="top-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <span class="top-bar-title">{{{$data['basic']['head']}}}</span>
            </div>
            <?php
                $categories = DB::table('categories')->where('display_ask_question', 'y')->remember(100)->get();
            ?>
            <div class="col-md-8 col-xs-8">
                <div class="select-categories-wrapper">
                    <div class="select-categories">
                        <select class="select-grey-bg" id="move_to_category">
                            <option value="">Select Categories</option>
                            <?php foreach($categories as $cate){ ?>
                                <option value="/category/<?php echo $cate->url_key; ?>" @if($data['basic']['catid'] == $cate->categoryid) selected="selected" @endif ><?php echo $cate->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <a href="javascript:void(0)" class="icon-search-top-bar"><i class="fa fa-search"></i></a>
            </div>
        </div>
    </div>
</section>
<!-- TOP BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li class="@if ($data['basic']['sort_type'] == 'recent') active @endif">
                        <a href="{{$data['basic']['base_url']}}">Senaste</a>
                    </li> 
                    <li class="@if ($data['basic']['sort_type'] == 'hot') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/hot">Gillat!</a>
                    </li>
                    <li class="@if ($data['basic']['sort_type'] == 'votes') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/votes">Flest röster</a>
                    </li>
                    <li class="@if ($data['basic']['sort_type'] == 'answers') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/answers">Flest svar</a>
                    </li>
                    <li style="display: none" class="@if ($data['basic']['sort_type'] == 'answers') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/answers">Flest svar</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php
    $keywords = '';
    if (Request::segment(2) and Request::segment(2) == 'search') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $request_uri = explode('?keyword=', $request_uri);
        if (count($request_uri) > 0 and $request_uri[1] != "") {
            $keywords = $request_uri[1];
        }
    }
    ?>
    <div class="form-search-wrapper" >
        <form id="form-search" method="GET" action="/questions/search">
            <a href="javascript:void(0)" class="clear-text-search"><i class="fa fa-times-circle"></i></a>
            <a href="javascript:void(0)" class="close-form-search">Avbryt</a>
            <input type="text" class="form-input-search" autocomplete="off" name="keyword" value="<?php echo $keywords; ?>" placeholder="Ange nyckelord" />
        </form>
    </div>
</section>
<!-- MIDDLE BAR / END -->

@if(count($data['questions']) > 0)

<!-- LIST QUESTION -->
<section class="list-question-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-question">
                    @foreach($data['questions'] as $record)
                    <li class="post-{{{ $record->postid }}} question type-question status-publish hentry question-item" data-id="{{{ $record->postid }}}">
                        <div class="avatar-user">
                            <a href="/question/{{{ $record->url_key }}}">
                                <img src="{{$record->user_picture}}" class="avatar" alt="{{$record->handle}}" />
                            </a>
                        </div>
                        <div class="info-user">
                            <span title="{{ Config::get('badges.'.$record->usertype.'.type') }}" class="user-badge">{{ Config::get('badges.'.$record->usertype.'.type') }}</span>
                            <ul class="info-review-question">
                                <li>{{$record->views}}<i class="fa fa-eye"></i></li>
                                <li class="">{{$record->acount}}<i class="fa fa-check-circle-o"></i></li>
                                <li>@if($record->netvotes > 0) {{$record->netvotes}} @else 0 @endif<i class="fa fa-chevron-circle-up"></i></li>
                            </ul>
                        </div>
                        <div class="content-question">
                            <h2 class="title-question">
                                <a href="/question/{{{ $record->url_key }}}">{{{ $record->title }}}</a>
                            </h2>
                            <div class="info-tag-time">
                                @if(!empty($record->tags) and count($record->tags) > 0)
                                <ul class="list-tag collapse">
                                    @foreach($record->tags as $tag)
                                    <li><a href="/tag/{{ $tag->url_key }}">{{ $tag->name }}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                                <span class="time-categories">
                                    Asked by <a href="/user/{{$record->user_url_key}}">{{$record->handle}}</a> on {{ Question::formattedCreatedDate($record->created) }} in <a href="/category/{{ $record->cat_url_key }}">{{ $record->categoryname }}</a>.
                                </span>
                            </div>
                        </div>
                    </li>
                    @endforeach    
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- LIST QUESTION / END -->

<section class="list-pagination-wrapper">
    <?php
    if (count($data['questions']) > 0) {
        echo $data['questions']->appends(array('limit' => $data['basic']['page_limit']))->links('pagination::slider');
    }
    ?>
</section>
<!-- PAGINATIONS QUESTION / END -->

@else
<div class="main-questions-list">
    <ul id="main_questions_list" >
        <h2>Inga resultat hittades</h2>
    </ul>
</div>

@endif
@stop