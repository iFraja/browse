@extends('mobile.layouts.default')
@section('content')

<!-- CONTAINER -->
<div class="wrapper-mobile">
    <!-- BADGES BAR -->
    <section class="tag-bar bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-4">
                    <h1 class="title-page">Titlar</h1>
                </div>
                <div class="col-md-8 col-xs-8 collapse">
                    <form class="find-tag-form">
                        <i class="fa fa-chevron-circle-right"></i>
                        <input type="text" name="" id="" placeholder="Find a user">
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- BADGES BAR / END -->

    <!-- POINT SYSTEM -->
    <section class="point-system-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="point-system">
                        <h1>Poängsystem</h1>
                        <span>Ditt rykte blir bättre när användare röstar på dina inlägg</span>
                        <ul class="list-point">
                            <li>
                                <span class="point-circle">+ 2</span>
                                <p class="point-text"><i class="fa fa-star"></i>Ställ en fråga</p>
                            </li>
                            <li>
                                <span class="point-circle">+ 1</span>
                                <p class="point-text"><i class="fa fa-star"></i>vote</p>
                            </li>
                            <li>
                                <span class="point-circle">+ 2</span>
                                <p class="point-text"><i class="fa fa-star"></i>answer</p>
                            </li>
                             <li>
                                <span class="point-circle">+ 3</span>
                                <p class="point-text"><i class="fa fa-star"></i>Det bästa svaret</p>
                            </li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- POINT SYSTEM / END -->

    <!-- POINT SYSTEM -->
    <section class="point-system-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="point-system">
                        <h1>Titlar System</h1>
                        <span style="margin-bottom:20px;">Ditt rykte blir bättre när användare röstar på dina inlägg</span>
                        <div class="badges-point-system">
                            <div class="list-bag-profile-wrapper user-list">
                                <!-- <span class="badges-profile">Professor</span> -->
                                <span class="user-badge" style="background:#780978;">
                                    Professor                                </span>                        
                                <span class="point-profile">
                                    <span>3000<i class="fa fa-star"></i></span>Poäng som krävs                                </span>
                            </div>
                            </div>
                        <div class="badges-point-system">
                            <div class="list-bag-profile-wrapper user-list">
                                <!-- <span class="badges-profile">Professor</span> -->
                                <span class="user-badge" style="background:#ffbf00;">
                                    Elit                                </span>                        
                                <span class="point-profile">
                                    <span>1000<i class="fa fa-star"></i></span>Poäng som krävs                                </span>
                            </div>
                            

                        </div>
                        <div class="badges-point-system">
                            <div class="list-bag-profile-wrapper user-list">
                                <!-- <span class="badges-profile">Professor</span> -->
                                <span class="user-badge" style="background:#138209;">
                                    Veteran                                </span>                        
                                <span class="point-profile">
                                    <span>500<i class="fa fa-star"></i></span>Poäng som krävs                                </span>
                            </div>
                            

                        </div>
                        <div class="badges-point-system">
                            <div class="list-bag-profile-wrapper user-list">
                                <!-- <span class="badges-profile">Professor</span> -->
                                <span class="user-badge" style="background:#450ead;">
                                    Amatör                                </span>                        
                                <span class="point-profile">
                                    <span>250<i class="fa fa-star"></i></span>Poäng som krävs                                </span>
                            </div>
                            

                        </div>
                        <div class="badges-point-system">
                            <div class="list-bag-profile-wrapper user-list">
                                <!-- <span class="badges-profile">Professor</span> -->
                                <span class="user-badge" style="background:#a35429;">
                                    Nybörjare                                </span>                        
                                <span class="point-profile">
                                    <span>50<i class="fa fa-star"></i></span>Poäng som krävs                                </span>
                            </div>
                            

                        </div>
                        <div class="badges-point-system">
                            <div class="list-bag-profile-wrapper user-list">
                                <!-- <span class="badges-profile">Professor</span> -->
                                <span class="user-badge" style="background:#04aad4;">
                                    Ny medlem                                </span>                        
                                <span class="point-profile">
                                    <span>0<i class="fa fa-star"></i></span>Poäng som krävs                                </span>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- POINT SYSTEM / END -->
</div>
<!-- CONTAINER / END -->

@stop