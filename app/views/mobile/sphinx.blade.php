@extends('mobile.layouts.default')
@section('content')
<?php 
function paginate($base_url, $query_str, $total_pages, $current_page, $paginate_limit){
    $page_array = array ();
    $dotshow = true;
    for ( $i = 1; $i <= $total_pages; $i ++ ){
        if ($i == 1 || $i == $total_pages || ($i >= $current_page - $paginate_limit && $i <= $current_page + $paginate_limit) ){
            $dotshow = true;
            if ($i != $current_page){
                $page_array[$i]['url'] = $base_url . "/" . $query_str . "/" . $i;
                $page_array[$i]['text'] = strval ($i);
            }else if ($dotshow == true){
                $dotshow = false;
                $page_array[$i]['text'] = "...";
            }
        } 
    }
    return $page_array;
}
?>
<!-- TOP BAR -->
<section class="top-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <span class="top-bar-title">Sök fråga: <em>{{{$data['basic']['head']}}}</em></span>
            </div>
            <?php
                $categories = DB::table('categories')->where('display_ask_question', 'y')->remember(100)->get();
            ?>
            <div class="col-md-8 col-xs-8">
                <div class="select-categories-wrapper">
                    <div class="select-categories">
                        <select class="select-grey-bg" id="move_to_category">
                            <option value="">Select Categories</option>
                            <?php foreach($categories as $cate){ ?>
                                <option value="/category/<?php echo $cate->url_key; ?>" @if($data['basic']['catid'] == $cate->categoryid) selected="selected" @endif ><?php echo $cate->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <a href="javascript:void(0)" class="icon-search-top-bar"><i class="fa fa-search"></i></a>
            </div>
        </div>
    </div>
</section>
<!-- TOP BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar" style="visibility: hidden">
                    <li class="@if ($data['basic']['sort_type'] == 'recent') active @endif">
                        <a href="{{$data['basic']['base_url']}}">Senaste</a>
                    </li> 
                    <li class="@if ($data['basic']['sort_type'] == 'hot') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/hot">Gillat!</a>
                    </li>
                    <li class="@if ($data['basic']['sort_type'] == 'votes') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/votes">Flest röster</a>
                    </li>
                    <li style="display: none" class="@if ($data['basic']['sort_type'] == 'answers') active @endif">
                        <a href="{{$data['basic']['base_url']}}/sort/answers">Flest svar</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php
$keywords = '';
if(isset($data['keywords'])){
    $keywords = $data['keywords'];
} 
?>
    <div class="form-search-wrapper" >
        <form id="form-search" method="GET" action="/questions/search">
            <a href="javascript:void(0)" class="clear-text-search"><i class="fa fa-times-circle"></i></a>
            <a href="javascript:void(0)" class="close-form-search">Avbryt</a>
            <input type="text" class="form-input-search" autocomplete="off" name="keyword" value="<?php echo $keywords; ?>" placeholder="Ange nyckelord" />
        </form>
    </div>
</section>
<!-- MIDDLE BAR / END -->

@if($data['total_question'] > 0)

<!-- LIST QUESTION -->
<section class="list-question-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-question">
                    @foreach($data['questions'] as $index=>$record)
                    <?php $record =  $record['attrs']; $record['postid'] =  $index;  ?>
                    <li class="post-{{{ $record['postid'] }}} question type-question status-publish hentry question-item" data-id="{{{ $record['postid'] }}}">
                        <div class="avatar-user">
                            <a href="/question/{{{ $record['url_key'] }}}">
                                <img src="{{$record['user_picture']}}" class="avatar" alt="{{{ $record['handle'] }}}" />
                            </a>
                        </div>
                        <div class="info-user">
                            <span title="{{ Config::get('badges.'.$record['usertype'].'.type') }}" class="user-badge">{{ Config::get('badges.'.$record['usertype'].'.type') }}</span>
                            <ul class="info-review-question">
                                <li>{{{ $record['views'] }}}<i class="fa fa-eye"></i></li>
                                <li class="">{{{ $record['acount'] }}}<i class="fa fa-check-circle-o"></i></li>
                                <li>@if($record['netvotes'] > 0) {{$record['netvotes']}} @else 0 @endif<i class="fa fa-chevron-circle-up"></i></li>
                            </ul>
                        </div>
                        <div class="content-question">
                            <h2 class="title-question">
                                <a href="/question/{{{ $record['url_key'] }}}">{{{ $record['title'] }}}</a>
                            </h2>
                            
                            <div class="info-tag-time">
                                <?php
                                    if($record['tag_ids']!=""){
                                    $tag_ids = explode(',', $record['tag_ids']);    
                                    $tags_data = DB::table('tags')->whereIn('id', $tag_ids)->get();
                                    if(!empty($tags_data) and count($tags_data) > 0){
                                    ?>    
                                    <ul class="list-tag">
                                       <?php foreach($tags_data as $tag): ?>
                                        <li><a href="/tag/<?php echo $tag->url_key; ?>"><?php echo $tag->name; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <?php } } ?>    
                                <span class="time-categories">
                                    Asked by <a href="/user/{{{ $record['user_url_key'] }}}">{{{ $record['handle'] }}}</a> on {{ Question::formattedCreatedDate($record['created']) }} i <a href="/category/{{ $record['cat_url_key'] }}">{{ $record['categoryname'] }}</a>.
                                </span>
                            </div>
                        </div>
                    </li>
                    @endforeach    
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- LIST QUESTION / END -->

<section class="list-pagination-wrapper">
    <?php
        $page_base_url = $data['basic']['base_url'] . '/limit/' . $data['basic']['page_limit'];
        if ($data['total_question'] > 0) {
            $total_pages = ceil($data['total_question'] / $data['basic']['page_limit']);
            $pages = paginate($page_base_url, "page", $total_pages, $data['basic']['page'], $data['basic']['page_limit']);
        ?>
    <ul>
        
        <?php foreach ($pages as $page) { ?>
            <?php if (isset($page['url'])) { ?>
                <li><a href="<?php echo $page['url'] ?>"><?php echo $page['text'] ?></a></li>
            <?php
            } else {
                $class = '';
                $text = $page['text'];
                if ($page['text'] == '...') {
                    $class = 'active';
                    $text = $data['basic']['page'];
                }
                ?>   
                <li class="<?php echo $class; ?>"><span><?php echo $text; ?></span></li>
            <?php } ?>    
        <?php } ?>
    </ul>
    <?php } ?>
</section>
<!-- PAGINATIONS QUESTION / END -->

@else
<div class="main-questions-list">
    <ul id="main_questions_list" >
        <h2>Inga resultat hittades</h2>
    </ul>
</div>

@endif
@stop