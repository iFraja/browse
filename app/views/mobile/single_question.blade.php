@extends('mobile.layouts.default')
@section('content')
<!-- CONTENT QUESTION -->
<section class="list-question-wrapper" id="question_content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-qna-wrapper">
                    <div class="avatar-user">
                        <a href="/user/{{{$data['question']['user_url_key']}}}">
                            <img src="{{{$data['question']['user_picture']}}}" class="avatar" alt="" />                            </a>
                    </div>
                    <div class="info-user" >
                        <span title="{{ Config::get('badges.'.$data['question']['usertype'].'.type') }}" class="user-badge">{{ Config::get('badges.'.$data['question']['usertype'].'.type') }}</span>
                    </div>
                    <div class="content-question">
                        <h2 class="title-question">
                            <a href="javascript:void(0)">{{$data['question']['title']}}</a>
                        </h2>
                        <div class="details">
                            {{$data['question']['content']}}
                            <p>&nbsp;</p>
                        </div>
                        <div class="info-tag-time">
                            @if($data['question']['tags'])
                            <ul class="list-tag">
                                @foreach($data['question']['tags'] as $tag)
                                    <li><a href="tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                            @endif
                            <span class="time-categories">
                                Asked by <a href="/user/{{{$data['question']['user_url_key']}}}">{{{$data['question']['handle']}}}</a> on {{ Question::formattedCreatedDate($data['question']['created']) }} in  <a href="/category/{{ $data['question']['cat_url_key'] }}">{{ $data['question']['categoryname'] }}</a>.
                            </span>
                        </div>
                        <div class="vote-wrapper">

                            <a href="javascript:void(0)" data-name="vote_up" class="action vote vote-up ">
                                <i class="fa fa-angle-up"></i>
                            </a>

                            <span class="number-vote">{{$data['question']['netvotes']}}</span>

                            <a href="javascript:void(0)" data-name="vote_down" class="action vote vote-down ">
                                <i class="fa fa-angle-down"></i>
                            </a>
                            
                            @if(count($data['answers']) > 0)
                            <a href="javascript:void(0)" class="answer-active-label has-best-answer">
                                <i class="fa fa-check"></i>Answered                                
                            </a>
                            @endif
                            
                            @if(Auth::check() and Auth::user()->userid <> 1)
                            <a class="report" onclick="dispay_report_form();" data-name="report" data-original-title="Report" data-toggle="tooltip" href="javascript:void(0)" style="color: red; font-size:20px;float: right; margin-right: 10px;"  >
                                <i class="fa fa-exclamation-triangle"></i>
                                Rapportera
                            </a>
                            @endif 
                        </div>
                    </div>
                </div>
                
                <div class="cmt-in-report-wrapper">
                <form class="form-post-answers form-post-report">
                    <textarea name="message" id="message" rows="4" placeholder="Har du något att säga? Skriv ditt meddelande här."></textarea>
                    <input type="submit" class="btn-submit" name="submit" id="" value="Skicka">
                    <a href="javascript:void(0)" class="close-form-post-report" onclick="dispay_report_form();">Avbryt</a>
                    <input type="hidden" name="post_id_report" id="post_id_report" value="{{{$data['question']['postid']}}}" >
                </form>
                </div>
                
                <!-- SHARE -->
                <div class="share">
                    <ul class="list-share">
                        <li>
                            <a class="share-social" href="javascript:void(0)" data-toggle="popover" data-placement="top"  data-container="body" data-content='<ul class="socials-share"><li><a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(Request::url())}}%2F&t={{$data['question']['title']}}?" target="_blank" class="btn-fb"><i class="fa fa-facebook"></i></a></li><li><a target="_blank" href="http://twitter.com/share?text={{$data['question']['title']}}?&url={{urlencode(Request::url())}}%2F" class="btn-tw"><i class="fa fa-twitter"></i></a></li><li class="ggplus"><a target="_blank"  href="https://plus.google.com/share?url={{urlencode(Request::url())}}%2F" class="btn-gg"><i class="fa fa-google-plus"></i></a></li></ul>' data-html="true">
                                Share <i class="fa fa-share"></i>
                            </a>
                        </li>
                        <!-- <li class="collapse">
                            <a href="javascript:void(0)">Rapportera<i class="fa fa-flag"></i></a>
                        </li> -->
                        <li>
                            <a href="javascript:void(0)" class="mb-show-comments">
                                Comment({{count($data['comments'][$data['question']['postid']])}})&nbsp;<i class="fa fa-comment"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- SHARE / END -->
                
                
                <!-- COMMENT IN COMMENT -->
                <div class="cmt-in-cmt-wrapper">
                    <ul class="mobile-comments-list">
                       @if(count($data['comments'][$data['question']['postid']]) > 0)
                       @foreach($data['comments'][$data['question']['postid']] as $comment)
                        <li data-id="{{$comment['postid']}}">
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <a class="avatar-cmt-in-cmt" href="/user/{{{$comment['user_url_key']}}}">
                                        <img alt="" class="avatar" src="{{$comment['user_picture']}}">
                                    </a>
                                    <p class="cmt-author">
                                        <a title="admin" href="/user/{{{$comment['user_url_key']}}}">
                                            {{{$comment['handle']}}}                
                                        </a>
                                    </p>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <div class="content-cmt-in-cmt">
                                        <p>{{$comment['content']}}</p>
                                        <span class="time-cmt-in-cmt">{{ Question::formattedCreatedDate($comment['created']) }}. <!-- <a href="javascript:void(0)">Edit&nbsp;&nbsp;<i class="fa fa-pencil"></i></a> --></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="clearfix"></div>
                                    <div style="width:90%;" class="line"></div>
                                </div>
                            </div>
                        </li>   
                       @endforeach
                       @endif
                    </ul>
                    
                    <form class="form-post-answers create-comment collapse">
                        <input type="hidden" name="qa_nonce"        value="33f40184eb" />
                        <input type="hidden" name="comment_post_ID" value="{{$data['question']['postid']}}" />
                        <input type="hidden" name="comment_type"    value="question" />
                        @if(Auth::check())
                        <input type="hidden" name="user_id"  value="{{Auth::user()->userid}}" />
                        @else
                        <input type="hidden" name="user_id" value="" />
                        @endif
                        <textarea name="post_content" id="post_content" rows="4" placeholder="Type your comment "></textarea>
                        <input type="submit" class="btn-submit" name="submit" id="" value="Lägg till kommentar">
                        <a href="javascript:void(0)" class="close-form-post-answers">Avbryt</a>
                    </form>
                    <a href="javascript:void(0)" class="add-cmt-in-cmt">Lägg till kommentar</a>
                    
                </div>
                <!-- COMMENT IN COMMENT / END -->
            </div>
        </div>
    </div>
</section>
<!-- CONTENT QUESTION / END -->

<!-- LABEL -->
<section class="label-vote-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span><span class="number">{{count($data['answers'])}}</span> Answers</span>
                <div class="select-categories-wrapper">
                    <div class="select-categories" style="display: none">
                        <select class="select-grey-bg" id="move_to_order">
                            <option value="">Vote</option>
                            <option  value="">Oldest</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- LABEL / END -->
<!-- CONTENT ANSWERS LOOP -->
<div id="answers_main_list">
  <?php $answer_array = array(); ?>
  @if($data['answers'])
  @foreach($data['answers'] as $answer)
  <?php $answer_array[]['ID'] = $answer['postid']; ?>
    <!-- CONTENT ANSWERS -->
    <section id="{{$answer['postid']}}" class="list-answers-wrapper answer-item">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="content-qna-wrapper">
                        <div class="avatar-user">
                            <a href="/user/{{$answer['user_url_key']}}">
                                <img src="{{$answer['user_picture']}}" class="avatar" alt="" />                        </a>
                        </div>
                        <div class="info-user">
                        </div>
                        <div class="content-question">

                            <div class="details" style="padding-left: 50px;">
                                <p>{{$answer['content']}}</p>
                            </div>
                            <div class="info-tag-time">
                                <span class="time-categories">
                                    Answered by <a href="/user/{{$answer['user_url_key']}}">{{$answer['name']}}</a> on {{ Question::formattedCreatedDate($answer['created']) }}..
                                </span>
                            </div>
                            <div class="vote-wrapper">

                                <a href="javascript:void(0)" data-name="vote_up" class="action vote vote-up ">
                                    <i class="fa fa-angle-up"></i>
                                </a>

                                <span class="number-vote">{{$answer['netvotes']}}</span>

                                <a href="javascript:void(0)" data-name="vote_down" class="action vote vote-down ">
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                
                                @if($data['question']['best_answer_id'] == 0)
                                @if(Auth::check())
                                @if($data['question']['userid'] == Auth::user()->userid)
                                <a class="action answer-active-label pending-answers" data-name="accept-answer" href="javascript:void(0)">
                                    Accept                            
                                </a>
                                @endif
                                @endif
                                @endif
                                @if($answer['postid'] == $data['question']['best_answer_id'])
                                    <a class=" answer-active-label best-answers" href="javascript:void(0)"><i class="fa fa-check"></i>Best answer</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- SHARE -->
                    <div class="share">
                        <ul class="list-share">
                            <li>
                                <a class="share-social" href="javascript:void(0)" data-toggle="popover" data-placement="top"  data-container="body" data-content='<ul class="socials-share"><li><a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(Request::url())}}%2F&t={{$data['question']['title']}}?" target="_blank" class="btn-fb"><i class="fa fa-facebook"></i></a></li><li><a target="_blank" href="http://twitter.com/share?text={{$data['question']['title']}}?&url={{urlencode(Request::url())}}%2F" class="btn-tw"><i class="fa fa-twitter"></i></a></li><li class="ggplus"><a target="_blank"  href="https://plus.google.com/share?url={{urlencode(Request::url())}}%2F" class="btn-gg"><i class="fa fa-google-plus"></i></a></li></ul>' data-html="true">
                                    Share <i class="fa fa-share"></i>
                                </a>                            
                            </li>
                            <!-- <li>
                                <a href="javascript:void(0)">Rapportera<i class="fa fa-flag"></i></a>
                            </li> -->
                            <li>
                                <a href="javascript:void(0)" class="mb-show-comments">Comment({{count($data['comments'][$answer['postid']])}})&nbsp;<i class="fa fa-comment"></i></a>
                            </li>
                        </ul>
                    </div>
                    <!-- SHARE / END -->
                    <!-- COMMENT IN COMMENT -->
                    <div class="cmt-in-cmt-wrapper">
                        
                        <ul class="mobile-comments-list">
                            @if(count($data['comments'][$answer['postid']]) > 0)
                            @foreach($data['comments'][$answer['postid']] as $comment)
                            <li data-id="{{$comment['postid']}}">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <a href="javascript:void(0)" class="avatar-cmt-in-cmt">
                                            <img src="{{$comment['user_picture']}}" class="avatar" alt="" />
                                        </a>
                                        <p class="cmt-author">
                                            <a href="/user/{{{$comment['user_url_key']}}}"  title="{{{$comment['handle']}}}">
                                                {{{$comment['handle']}}}                
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <div class="content-cmt-in-cmt">
                                            <p>{{$comment['content']}}</p>
                                            <span class="time-cmt-in-cmt">on {{ Question::formattedCreatedDate($comment['created']) }}. <!-- <a href="javascript:void(0)">Edit&nbsp;&nbsp;<i class="fa fa-pencil"></i></a> --></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="clearfix"></div>
                                        <div class="line" style="width:90%;"></div>
                                    </div>
                                </div>
                            </li>   
                            @endforeach
                            @endif
                        </ul>
                        
                        <form class="form-post-answers create-comment collapse">
                            <input type="hidden" name="qa_nonce"        value="33f40184eb" />
                            <input type="hidden" name="comment_post_ID" value="{{$answer['postid']}}" />
                            <input type="hidden" name="comment_type"    value="answer" />
                            @if(Auth::check())
                            <input type="hidden" name="user_id" value="{{Auth::user()->userid}}" />
                            @else
                            <input type="hidden" name="user_id" value="" />
                            @endif
                            <textarea name="post_content" id="post_content" rows="4" placeholder="Type your comment "></textarea>
                            <input type="submit" class="btn-submit" name="submit" id="" value="Lägg till kommentar">
                            <a href="javascript:void(0)" class="close-form-post-answers">Avbryt</a>
                        </form>
                        <a href="javascript:void(0)" class="add-cmt-in-cmt">Lägg till kommentar</a>
                        
                    </div>
                    <!-- COMMENT IN COMMENT / END -->
                </div>
            </div>
        </div>
    </section>
    <!-- CONTENT ANSWERS / END -->   
@endforeach    
@endif    
</div>

<script type="text/javascript">
    currentQuestion = {"ID":{{$data['question']['postid']}},"post_author":"{{$data['question']['userid']}}","post_date":"{{$data['question']['created']}}","post_date_gmt":"{{$data['question']['created']}}","post_content":"{{$data['question']['content']}}","post_title":"{{$data['question']['title']}}","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"{{$data['question']['title']}}","to_ping":"","pinged":"","post_modified":"{{$data['question']['created']}}","post_modified_gmt":"{{$data['question']['created']}}","post_content_filtered":"","post_parent":0,"guid":"{{urlencode(Request::url())}}","menu_order":0,"post_type":"question","post_mime_type":"","comment_count":"0","filter":"raw","question_category":[{"term_id":{{$data['question']['categoryid']}},"name":"{{$data['question']['categoryname']}}","slug":"advice","term_group":0,"term_taxonomy_id":0,"taxonomy":"question_category","description":"","parent":0,"count":3,"filter":"raw"}],"qa_tag":[{"term_id":301,"name":"fxs","slug":"fxs","term_group":0,"term_taxonomy_id":303,"taxonomy":"qa_tag","description":"","parent":0,"count":1,"filter":"raw"}],"et_vote_count":0,"et_view_count":"8","et_answers_count":0,"et_users_follow":"11","et_answer_authors":[],"et_last_author":"","et_vote_up_authors":[],"et_vote_down_authors":[],"et_best_answer":"","id":{{$data['question']['postid']}},"voted_down":true,"voted_up":true,"user_badge":"<span title=\u0022130\u0022 class=\u0022user-badge\u0022 style=\u0022background-color:#a35429;\u0022>Supporter<\/span>","answered":false,"has_category":true,"content_filter":"{{$data['question']['content']}}","content_edit":"{{$data['question']['content']}}","author_name":"{{$data['question']['handle']}}","followed":false,"reported":false};
    @if($data['answers'])
    var answersData  = {{json_encode($answer_array)}};
    @else
    var answersData  = [];    
    @endif
    var commentsData = [];
</script>

<div class="clearfix" style="height:20px;"></div>

<section class="post-answers-wrapper" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class="btn-post-answers" href="javascript:void(0)" style="display: none">Post answer</a>
                <form action="" id="insert_answer" class="form-post-answers" style="display: block">
                    <input type="hidden" value="37fe0a4638" name="qa_nonce">
                    <input type="hidden" value="{{$data['question']['postid']}}" name="post_parent">
                    <textarea placeholder="Type your answer" rows="5" id="post_content" name="post_content"></textarea>
                    <a class="close-form-post-answers" href="javascript:void(0)" onclick="upload_mobile_image();">Ladda upp bild</a>
                    <br><br>
                    <input type="submit" value="Post answer" id="" name="submit" class="btn-submit">
                    <a class="close-form-post-answers" id="close_reply_form" href="javascript:void(0)">Avbryt</a>
                </form>
                <form id="uploadimage" name="uploadimage" action="" method="post" enctype="multipart/form-data" style="display: none;">
                    <input type="file" name="images_upload" id="images_upload"  />
                </form>
            </div>
        </div>
    </div>
</section>

<div class="clearfix" style="height:20px;"></div>



<!-- CONTENT ANSWERS LOOP / END -->
<!-- PAGINATIONS ANSWER -->
<section class="list-pagination-wrapper">
</section>
<!-- PAGINATIONS ANSWER / END -->
<script type="text/javascript" >
    function upload_mobile_image(){
        jQuery('#images_upload').click();
    }
    function dispay_report_form(){
        jQuery('.cmt-in-report-wrapper').toggle();
    }
    
    jQuery(document).ready(function (e) {
        
        jQuery("#images_upload").change(function() { 
            jQuery('#uploadimage').submit();
        });
        
        jQuery("#uploadimage").on('submit',(function(e) {
            e.preventDefault();
            jQuery.ajax({
                url: "/admin-ajax",
                type: "POST",
                dataType: "json",
                data: new FormData(this),
                contentType: false,
                cache: false, 
                processData:false, 
                success: function(response){
                    if(response.success){
                        var post_content = jQuery('textarea#post_content').val();
                        var newhtml = post_content + '<br><img style="width:70%;" src="'+response.data+'" ><br>';
                        jQuery('textarea#post_content').val(newhtml);
                    }
                }
            });
        }));
        
        
    });
    
   
    
</script>
<script type="text/template" id="mobile_answer_item">

    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="content-qna-wrapper">
    <div class="avatar-user">
    <a href="javascript:void(0)">
    {<?php echo '{= avatar }'; ?>}
    </a>
    </div>
    <div class="info-user">
    {<?php echo '{= user_badge }'; ?>}
    </div>
    <div class="content-question">
    <div class="details" style="padding-left: 50px;">
    {<?php echo '{= content_filter }'; ?>}
    </div>
    <div class="info-tag-time">
    <span class="time-categories">
    Answered by <a href="{<?php echo '{= author_url }'; ?>}">{<?php echo '{= author_name }'; ?>}</a> {<?php echo '{= human_date }'; ?>}.
    </span>
    </div>
    <div class="vote-wrapper">

    <a href="javascript:void(0)" data-name="vote_up" class="action vote vote-up">
    <i class="fa fa-angle-up"></i>
    </a>

    <span class="number-vote">0</span>

    <a href="javascript:void(0)" data-name="vote_down" class="action vote vote-down">
    <i class="fa fa-angle-down"></i>
    </a>
    <# if(currentUser.ID == parent_author) { #>
    <a href="javascript:void(0)" data-name="accept-answer" class="action answer-active-label pending-answers">Accept</a>
    <# } #>
    </div>
    </div>
    </div>
    <!-- SHARE -->
    <div class="share">
    <ul class="list-share">
    <li>
    <a class="share-social" href="javascript:void(0)" data-toggle="popover" data-placement="top" data-container="body" data-content="<ul class=&quot;socials-share&quot;><li><a href=&quot;https://www.facebook.com/sharer/sharer.php?u={<?php echo '{= guid }'; ?>}&amp;t={<?php echo '{= post_title }'; ?>}&quot; target=&quot;_blank&quot; class=&quot;btn-fb&quot;><i class=&quot;fa fa-facebook&quot;></i></a></li><li><a target=&quot;_blank&quot; href=&quot;http://twitter.com/share?text={<?php echo '{= post_title }'; ?>}&amp;url={<?php echo '{= guid }'; ?>}&quot; class=&quot;btn-tw&quot;><i class=&quot;fa fa-twitter&quot;></i></a></li><li class=&quot;ggplus&quot;><a target=&quot;_blank&quot;  href=&quot;https://plus.google.com/share?url={<?php echo '{= guid }'; ?>}&quot; class=&quot;btn-gg&quot;><i class=&quot;fa fa-google-plus&quot;></i></a></li></ul>" data-html="true">
    Share <i class="fa fa-share"></i>
    </a>
    </li>
    <!--<li>
    <a href="javascript:void(0)">Report<i class="fa fa-flag"></i></a>
    </li>-->
    <li class="active-comment">
    <a href="javascript:void(0)" class="mb-show-comments">Comment(0)&nbsp;<i class="fa fa-comment"></i></a>
    </li>
    </ul>
    </div>
    <!-- SHARE / END -->
    <!-- COMMENT IN COMMENT -->
    <div class="cmt-in-cmt-wrapper">
    <ul class="mobile-comments-list"></ul>
    <form class="form-post-answers create-comment collapse">
    <input type="hidden" name="qa_nonce"        value="b6e1eb4b31" />
    <input type="hidden" name="comment_post_ID" value="{<?php echo '{= ID }'; ?>}" />
    <input type="hidden" name="comment_type"    value="answer" />
    <input type="hidden" name="user_id"         value="{<?php echo '{= post_author }'; ?>}" />                     
    <textarea name="post_content" id="post_content" rows="4" placeholder="Type your comment "></textarea>
    <input type="submit" class="btn-submit" name="submit" id="" value="Lägg till kommentar">
    <a href="javascript:void(0)" class="close-form-post-answers">Avbryt</a>
    </form>                    
    <a href="javascript:void(0)" class="add-cmt-in-cmt">Lägg till kommentar</a>
    </div>
    <!-- COMMENT IN COMMENT / END -->
    </div>
    </div>
    </div>

</script>
<script type="text/template" id="mobile_comment_item">
    <div class="row">
    <div class="col-md-3 col-xs-3">
    <a href="javascript:void(0)" class="avatar-cmt-in-cmt">
    {<?php echo '{= avatar }'; ?>}
    </a>
    <p class="cmt-author">
    <a href="{<?php echo '{= author_url }'; ?>}"  title="{<?php echo '{= author }'; ?>}">
    {<?php echo '{= author }'; ?>}
    </a>
    </p>
    </div>
    <div class="col-md-9 col-xs-9">
    <div class="content-cmt-in-cmt">
    {<?php echo '{= content_filter }'; ?>}
    <span class="time-cmt-in-cmt">{<?php echo '{= human_date }'; ?>}.</span>
    </div>
    </div>
    <div class="col-md-12">
    <div class="clearfix"></div>
    <div class="line" style="width:90%;"></div>
    </div>
    </div>
</script>

@stop