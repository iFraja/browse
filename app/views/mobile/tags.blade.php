@extends('mobile.layouts.default')
@section('content')
<!-- TAGS BAR -->
<section class="tag-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <h1 class="title-page">Tags</h1>
            </div>
            <div class="col-md-8 col-xs-8">
                <form class="find-tag-form" action="/tags" method="GET">
                    <i class="fa fa-chevron-circle-right"></i>
                    <input type="text" name="ckey" id="ckey" value="{{$data['keyword']}}" placeholder="Find a tag">
                </form>
            </div>
        </div>
    </div>
</section>
<!-- TAGS BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li class="active">
                        <a href="/tags">Name</a>
                    </li>
                    <li class="">
                        <a href="/tags/popular">Populära</a>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
</section>
<!-- MIDDLE BAR / END -->

@if(isset($data['alphas']) and isset($data['tags']))
<!-- LIST TAG -->
<section class="list-tags-wrapper">
    <div class="container">
        <div class="row">
            <ul class="list-tags">
                @foreach($data['alphas'] as $alpha)
                @if(count($data['tags'][$alpha]) > 0)
                <li>
                    <div class="col-md-1 col-xs-1">
                        <span class="big-charater"><a href="/tags/{{{ $alpha }}}" >{{{ strtoupper($alpha) }}}</a></span>
                    </div>
                    <div class="col-md-11 col-xs-11">
                        <div class="tags-wrapper">
                            <ul class="tags mobile-tags-list">
                               @foreach($data['tags'][$alpha] as $tag) 
                                <li data-id="1">
                                    <span class="tag"><a href="/tag/{{ $tag->url_key }}">{{{ $tag->name }}}</a></span>
                                </li>
                                @endforeach
                            </ul>
                            <?php if(Request::segment(2) == ''){ ?>
                            <a href="/tags/{{{ $alpha }}}" class="more-tag-link">Touch here to see more {{{ strtoupper($alpha) }}} tags</a>
                            <?php } ?>
                        </div>
                    </div>
                </li>
                @endif
                @endforeach
            </ul>
        </div>
    </div>
</section>
<!-- LIST TAG / END -->
@endif
@stop