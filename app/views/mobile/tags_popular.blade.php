@extends('mobile.layouts.default')
@section('content')

<!-- TAGS BAR -->
<section class="tag-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <h1 class="title-page">Tags</h1>
            </div>
            <div class="col-md-8 col-xs-8">
                <form class="find-tag-form" action="/tags" method="GET">
                    <i class="fa fa-chevron-circle-right"></i>
                    <input type="text" name="ckey" id="ckey" value="" placeholder="Find a tag">
                </form>
            </div>
        </div>
    </div>
</section>
<!-- TAGS BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li class="">
                        <a href="/tags">Namn</a>
                    </li>
                    <li class="active">
                        <a href="/tags/popular">Populära</a>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
</section>
<!-- MIDDLE BAR / END -->

@if(isset($data['tags']))
<section class="list-tags-wrapper">
    <div class="container">
        <div class="row">
            <ul class="list-tags">
                <div class="col-md-12 col-xs-12">
                    <div class="tags-wrapper">
                        <ul class="tags">
                            @foreach($data['tags'] as $tag)  
                            <li>
                                <span class="tag"><a href="/tag/{{ $tag->url_key }}">{{{ $tag->name }}}</a></span>
                                <span class="time-tag">&nbsp;</span>
                            </li>
                            @endforeach  
                        </ul>
                    </div>
                </div>
            </ul>
        </div>
    </div>
</section>
@endif

@stop




