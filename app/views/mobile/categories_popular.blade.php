@extends('mobile.layouts.default')
@section('content')
<!-- TAGS BAR -->
<section class="tag-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <h1 class="title-page">Categories</h1>
            </div>
            <div class="col-md-8 col-xs-8">
                <form class="find-tag-form" action="/categories" method="GET">
                    <i class="fa fa-chevron-circle-right"></i>
                    <input type="text" name="ckey" id="ckey" value="" placeholder="Find a categories">
                </form>
            </div>
        </div>
    </div>
</section>
<!-- TAGS BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li class="">
                        <a href="/categories">Namn</a>
                    </li>
                    <li class="active">
                        <a href="/categories/popular">Populära</a>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
</section>
<!-- MIDDLE BAR / END -->

@if(isset($data['categories']))
<section class="list-categories-wrapper">
    <div class="container">
        <div class="row">
            <ul class="list-categories">
                <div class="col-md-12 col-xs-12">
                    <div class="categories-wrapper">
                        <ul class="categories mobile-categories-list popular-list">        
                            @foreach($data['categories'] as $category)
                            <li class="popular">
                                <span class="tag">
                                    <a href="/category/{{ $category->url_key }}">{{{ $category->title }}}</a>
                                </span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </ul>
        </div>
    </div>
</section>
@endif
@stop



