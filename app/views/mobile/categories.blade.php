@extends('mobile.layouts.default')
@section('content')
<!-- TAGS BAR -->
<section class="tag-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <h1 class="title-page">Kategorier</h1>
            </div>
            <div class="col-md-8 col-xs-8">
                <form class="find-tag-form" action="/categories" method="GET">
                    <i class="fa fa-chevron-circle-right"></i>
                    <input type="text" name="ckey" id="ckey" value="{{$data['keyword']}}" placeholder="Find a categories">
                </form>
            </div>
        </div>
    </div>
</section>
<!-- TAGS BAR / END -->

<!-- MIDDLE BAR -->
<section class="middle-bar bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-middle-bar">
                    <li class="active">
                        <a href="/categories">Namn</a>
                    </li>
                    <li class="">
                        <a href="/categories/popular">Populära</a>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
</section>
<!-- MIDDLE BAR / END -->

@if(isset($data['alphas']) and isset($data['categories']))
<!-- LIST CATEGORIES -->
<section class="list-categories-wrapper">
    <div class="container">
        <div class="row">
            <ul class="list-categories">
              @foreach($data['alphas'] as $alpha)
              @if(count($data['categories'][$alpha]) > 0)
                <li>
                    <div class="col-md-1 col-xs-1">
                        <span class="big-charater"><a href="/categories/{{{ $alpha }}}" >{{{ strtoupper($alpha) }}}</a></span>
                    </div>
                    <div class="col-md-11 col-xs-11">
                        <div class="categories-wrapper">
                            <ul class="categories mobile-categories-list">
                                @foreach($data['categories'][$alpha] as $category)
                                <li data-id="1" class="col-xs-6">
                                    <span class="categorie">
                                        <a href="/category/{{ $category->url_key }}">{{{ $category->title }}}</a>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                            
                        </div>
                    </div>
                </li>
                @endif
                @endforeach
            </ul>
        </div>
    </div>
</section>
<!-- LIST CATEGORIES / END -->
@endif

@stop


