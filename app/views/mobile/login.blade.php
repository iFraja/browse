<!DOCTYPE html>
<html lang="en-US">
    <!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
    <!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <meta charset="utf-8">
        <title>Login | Questions ans Answers</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="description" content="Just another WordPress site">
        <link rel="shortcut icon" href="/qaengine/img/fe-favicon.png"/>
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
        <link href='/qaengine/css/intro.css' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
        <script type="text/javascript">
            ae_globals = {"ajaxURL":"/admin-ajax","imgURL":"","posts_per_page":"12","homeURL":"/","user_confirm":0,"pending_questions":0,"pending_answers":0,"introURL":"/","gplus_client_id":false,"plupload_config":{"max_file_size":"3mb","url":"/admin-ajax","flash_swf_url":"/qaengine\/qa-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"/qaengine\/qa-includes\/js\/plupload\/plupload.silverlight.xap"}}		
        </script>
        <meta name='robots' content='noindex,follow' />
        <link rel='stylesheet' id='bootstrap-css'  href='/qaengine/css/libs/bootstrap.min.css?ver=4.1' type='text/css' media='all' />
        <link rel='stylesheet' id='mobile-style-css'  href='/qaengine/mobile/css/main.css?ver=4.1' type='text/css' media='all' />
        <link rel='stylesheet' id='font-awesome-css'  href='/qaengine/css/libs/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/jquery.js?ver=1.11.1'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/underscore.min.js?ver=1.6.0'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/backbone.min.js?ver=1.1.2'></script>
        <script type='text/javascript' src='/qaengine/js/functions.js?ver=4.1'></script>
        <script type='text/javascript'>
        /* <![CDATA[ */
        var qa_front = {"form_auth": {"error_msg": "Please fill out all fields required.", "error_user": "Please enter your user name.", "error_email": "Please enter a valid email address.", "error_username": "Please enter a valid username.", "error_repass": "Please enter the same password as above.", "error_url": "Please enter a valid URL.", "error_cb": "You must accept the term & privacy."}, "texts": {"require_login": "You must be logged in to perform this action.", "enought_points": "You don't have enought points to perform this action.", "create_topic": "Create Topic", "upload_images": "Ladda upp bild", "insert_codes": "Insert Code", "no_file_choose": "Ingen fil vald.", "require_tags": "Please insert at least one tag.", "add_comment": "Lägg till kommentar", "cancel": "Cancel", "sign_up": "Bli medlem", "sign_in": "Logga in", "accept_txt": "Accept", "best_ans_txt": "Best answer", "forgotpass": "Glömt lösenord?", "close_tab": "You have made some changes which you might want to save.", "confirm_account": "You must activate your account first to create questions \/ answers!.", "cancel_auth": "User cancelled login or did not fully authorize.", "banned_account": "You account has been banned, you can't make this action!"}};
        /* ]]> */
</script>
        <script type='text/javascript' src='/qaengine/mobile/js/front.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/mobile/js/jquery.mouseweel.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/mobile/js/script-mobile.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/plupload/plupload.full.min.js?ver=2.1.1'></script>
        <style>
            label.error{color: red;}
        </style>    
    </head>
    <body class="page page-id-9 page-template page-template-page-intro page-template-page-intro-php intro-wrapper">
        <div class="container">
            <div class="row">
                <header id="header" class="intro-header">
                    <div class="col-md-12" id="logo">
                        <a href="/">
                            <img src="/qaengine/img/logo1.png">
                        </a>
                    </div><!-- logo -->
                </header><!-- END HEADER -->

                <div class="clearfix"></div>
                <!-- CONTENT INTRO -->
                <div class="intro-content-wrapper mobile-device">
                    <div class="col-md-7">
                        <div class="intro-text">
                            <h2 class="slide-text">
                               
                            </h2>
                            <h3 class="text-bottom">
                                
                            </h3>
                        </div>
                    </div>
                    <!-- FORM -->
                    <div class="col-md-5">
                        <div class="form-signup-wrapper">
                            <a class="hiddenanchor" id="toregister"></a>
                            <a class="hiddenanchor" id="tologin"></a>
                            <div class="group-btn-intro">
                                <a href="#tologin" class="to_register to_login_active active"> Logga in </a> 
                                <span>or</span>
                                <a href="#toregister" class="to_register to_register_active">Bli medlem</a>
                            </div>
                            <div id="wrapper">
                                <div id="login" class="animate">
                                    <form class="sign-in-intro" id="sign_in" method="POST">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="intro-name">
                                                    <span class="your-email">
                                                        <input type="text" autocomplete="off" id="username" name="username" value="" class="" placeholder="Email">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </p>
                                                <p class="intro-remember collapse">
                                                    <input type="hidden" id="remember" name="remember" value="0" />
                                                    <a class="your-remember" href="javascript:void(0)">
                                                        <i class="fa fa-check-circle-o"></i> Remember me                                                    </a>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="intro-password">
                                                    <span class="your-password">
                                                        <input type="password"  autocomplete="off" id="password" name="password" class="" placeholder="Lösenord">
                                                        <i class="fa fa-key"></i>
                                                    </span>
                                                </p>
                                                <p class="intro-remember">
                                                    <a class="your-fogot-pass collapse" href="javascript:void(0)">
                                                        Fogot Lösenord ?                                                    </a>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="btn-submit-intro">
                                                    <span class="your-submit mobile-device">
                                                        <input type="submit" name="" value="Logga in" class="btn-submit" />
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="register" class="animate ">
                                    <form class="sign-up-intro" id="sign_up" method="POST">
                                        <div class="row">
                                        	<div class="col-md-12">
                                            	<div class="row">
                                        			<div class="col-md-6">
                                                        <p class="intro-name">
                                                            <span class="your-email">
                                                                <input type="text" autocomplete="off" name="email" id="email" value="" class="" placeholder="Email">
                                                                <i class="fa fa-envelope-o"></i>
                                                            </span>
                                                        </p>
                                                     </div>
                                                     <div class="col-md-6">
                                                        <p class="intro-name">
                                                            <span class="your-name">
                                                                <input type="text" autocomplete="off" name="username" id="username" value="" class="" placeholder="Fullständigt namn">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                        </p>
                                                    </div>
                                                 </div>
                                            </div>
                                            <div class="col-md-12">
                                            	<div class="row">
                                        			<div class="col-md-6">
                                                        <p class="intro-password">
                                                            <span class="your-password">
                                                                <input type="password" autocomplete="off" name="password" id="password1" value="" class="" placeholder="Lösenord">
                                                                <i class="fa fa-key"></i>
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="intro-password">
                                                            <span class="your-password">
                                                                <input type="password" autocomplete="off" id="re_password" name="re_password" value="" class="" placeholder="Repeat Lösenord">
                                                                <i class="fa fa-key"></i>
                                                            </span>
                                                        </p>
                                                	</div>
                                                </div>
                                            </div>

                                            
                                            <div class="col-md-12">
                                            	<p class="btn-submit-intro">
                                                    <span class="your-submit">
                                                        <input type="submit" name="" value="Bli medlem" class="btn-submit" />
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix" style="height:50px;"></div>
                        
                    </div>
                    <!-- END FORM -->
                </div>
                
            </div>
        </div>
        <div class="clearfix" style="height:120px;"></div>
        <script type="text/javascript" src="/qaengine/js/libs/selectivizr-min.js"></script>
        <script>
                            jQuery(document).ready(function ($) {
                                var ua = window.navigator.userAgent;
                                var msie = ua.indexOf("MSIE ");

                                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                                    $(".adject").textrotator({
                                        animation: "dissolve",
                                        separator: "|",
                                        speed: 2000
                                    });
                                } else {
                                    $(".adject").textrotator({
                                        animation: "flipUp",
                                        separator: "|",
                                        speed: 2000
                                    });
                                }

                                $(".to_register").click(function () {
                                    $(".group-btn-intro").find('.to_register').removeClass('active');
                                    $(this).addClass('active');
                                });
                                $(".to_login_active").click(function () {
                                    $('.form-signup-wrapper').css({'min-height': '210px'});
                                });
                                $(".to_register_active").click(function () {
                                    $('.form-signup-wrapper').css({'min-height': '300px'});
                                });
                            });
        </script>
        <!-- Style Intro Background -->
        <!-- Style Intro Background -->
        <!--Customizer CSS-->
        <style type="text/css">
        </style>
        <!--/Customizer CSS-->
        <script type='text/javascript' src='/qaengine/includes/aecore/assets/js/appengine.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/bootstrap.min.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/modernizr.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/jquery.simple-text-rotator.min.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/jquery.validate.min.js?ver=4.1'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/core.min.js?ver=1.11.2'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/widget.min.js?ver=1.11.2'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/position.min.js?ver=1.11.2'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/menu.min.js?ver=1.11.2'></script>
        <script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.2'></script>
        <script type="text/javascript" id="frontend_scripts">
                                    (function ($) {
                                        $(document).ready(function () {
                                            if (typeof QAEngine.Views.MobileFront != 'undefined') {
                                                QAEngine.MobileApp = new QAEngine.Views.MobileFront();
                                            }
                                            if (typeof QAEngine.Views.MobileSingleQuestion != 'undefined') {
                                                QAEngine.MobileSingleQuestion = new QAEngine.Views.MobileSingleQuestion();
                                            }
                                        });
                                    })(jQuery);
        </script>
        <script type="text/javascript">
            _.templateSettings = {
                evaluate: /\<\#(.+?)\#\>/g,
                interpolate: /\{\{=(.+?)\}\}/g,
                escape: /\{\{-(.+?)\}\}/g
            };
        </script>
        <script type="text/javascript" id="frontend_scripts">
            (function ($) {
                $(document).ready(function () {

                });
            })(jQuery);
        </script>
            <script type="text/javascript" id="current_user">
                    currentUser = {"id": 0, "ID": 0}
                                    </script>
        <!-- GOOGLE ANALYTICS CODE --><!-- END GOOGLE ANALYTICS CODE -->	</body><!-- END BODY -->
</html>