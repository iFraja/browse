<!DOCTYPE html>
<html lang="en-US">
    <!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
    <!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta charset="utf-8">
        <title>Questions | QA Engine - Enginethemes</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

        <link rel="shortcut icon" href=""/>
        <link href='//fonts.googleapis.com/css?family=Lato:400,700&subset=latin,cyrillic,cyrillic-ext,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="/qaengine/js/libs/selectivizr-min.js"></script>
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
        <link rel='stylesheet' id='bootstrap-css'  href='/qaengine/css/libs/bootstrap.min.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='font-awesome-css'  href='/qaengine/css/libs/font-awesome.min.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='main-style-css'  href='/qaengine/css/main.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='editor-style-css'  href='/qaengine/css/editor.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='push-menu-css'  href='/qaengine/css/libs/push-menu.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='chosen-css'  href='/qaengine/css/libs/chosen.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='custom-style-css'  href='/qaengine/css/custom.css?ver=4.0.1' type='text/css' media='all' />
        <link rel='stylesheet' id='style-css'  href='/qaengine/style.css?ver=4.0.1' type='text/css' media='all' />
        
        <!--  File from WP-Include -->
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/plupload/plupload.full.min.js?ver=2.1.1'></script>
        <!--  File from WP-Include -->

        <!-- This site is using AdRotate v3.10.16 to display their advertisements - https://www.adrotateplugin.com/ -->

        <style type="text/css">
            .tp-caption.demo-1 {
                font-size:35px;
                line-height:45px;
                font-weight:300;
                font-family:"Open Sans";
                color:rgb(0, 0, 0);
                text-decoration:none;
                background-color:transparent;
                text-shadow:none;
                width:260px;
                border-width:0px;
                border-color:rgb(0, 0, 0);
                border-style:none;
            }
        </style>
    </head>
    <body class="archive post-type-archive post-type-archive-question logged-in cbp-spmenu-push">
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 left-sidebar">

                        <div class="widget widget-btn">
                            <button type="button" data-toggle="modal" class="action ask-question">
                                <i class="fa fa-plus"></i> Ställ en fråga                    </button>
                        </div><!-- END BUTTON MODAL QUESTION -->

                        <div class="widget widget-menus">
                            <div class="menu-left-menu-container"><ul id="menu-left-menu" class="menu"><li id="menu-item-1765" class="fa-question-circle menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-1765"><a href="/questions/"><i class="fa fa-question-circle"></i>Questions</a></li>
                                    <li id="menu-item-183" class="fa-tags menu-item menu-item-type-post_type menu-item-object-page menu-item-183"><a href="/tags-4/"><i class="fa fa-tags"></i>Tags</a></li>
                                    <li id="menu-item-18" class="fa-trophy menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="/badges/"><i class="fa fa-trophy"></i>Titlar</a></li>
                                    <li id="menu-item-1742" class="fa-th-list menu-item menu-item-type-post_type menu-item-object-page menu-item-1742"><a href="/categories-3/"><i class="fa fa-th-list"></i>Categories</a></li>
                                    <li id="menu-item-1743" class="fa-users menu-item menu-item-type-post_type menu-item-object-page menu-item-1743"><a href="/users-2/"><i class="fa fa-users"></i>Users</a></li>
                                </ul></div>                </div><!-- END LEFT MENU -->

                        <div class="widget widget-recent-activity">

                            <ul>
                                <li>
                                    <i class="fa fa-thumbs-up"></i>                    <span>
                                        Du har röstat upp ett svar på en fråga <a href="/question/what-are-some-of-your-favorite-deep-awe-inspiring-andor-thought-provoking-movies/">What are some of your favorite deep, awe-inspiring, and/or thought-provoking movies?</a>.                        <br>
                                        <span class="time-activity">
                                            on September 18, 2014                        </span>
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-question-circle"></i>                    <span>
                                        You edited question .                        <br>
                                        <span class="time-activity">
                                            on September 18, 2014                        </span>
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-comments"></i>                    <span>
                                        You edited answer on question .                        <br>
                                        <span class="time-activity">
                                            on September 18, 2014                        </span>
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-comments"></i>                    <span>
                                        You answered question <a href="/question/what-are-key-strategies-to-acquire-first-100k-users-with-zero-marketing-budget/">What are key strategies to acquire first 100K users with zero marketing budget?</a>.                        <br>
                                        <span class="time-activity">
                                            on September 18, 2014                        </span>
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-undo"></i>                    <span>
                                        Your question  was unvoted.                        <br>
                                        <span class="time-activity">
                                            on August 13, 2014                        </span>
                                    </span>
                                </li>
                                <li>
                                    <i class="fa fa-undo"></i>                    <span>
                                        Your unvoted an answer on question .                        <br>
                                        <span class="time-activity">
                                            on July 3, 2014                        </span>
                                    </span>
                                </li>
                            </ul>

                        </div><!-- END widget-recent-activities -->


                        <div class="copyright">
                            &copy;2014 Enginethemes <br>
                            <a href="/term/">Villkor</a>
                        </div>
                    </div><!-- END LEFT-SIDEBAR -->
                </div>
            </div>		</nav>
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 right-sidebar">
                        <div class="widget widget-statistic">
                            <p class="questions-count">
                                Questions<br>
                                <span>14</span>
                            </p>
                            <p class="members-count">
                                Medlemmar
<br>
                                <span>8</span>
                            </p>
                        </div><!-- END widget-statistic -->
                        <div class="widget widget-related-tags">
                            <h3>MOST USED TAGS</h3>
                            <ul>
                                <li>
                                    <a class="q-tag" href="/qa-tag/business-2/">business</a> x 5            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/google/">google</a> x 5            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/marketing-2/">marketing</a> x 5            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/technology-2/">technology</a> x 4            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/apps/">apps</a> x 4            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/movie-2/">movie</a> x 3            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/billionaire/">billionaire</a> x 3            </li>
                                <li>
                                    <a class="q-tag" href="/qa-tag/facebook/">facebook</a> x 3            </li>
                            </ul>
                            <a href="/tags-4/">See more tags</a>
                        </div><!-- END widget-related-tags -->
                        <div class="widget widget-hot-questions">
                            <h3>Gillade frågor</h3>
                            <ul>
                                <li>
                                    <a href="/question/what-are-the-best-mobile-apps-for-traveling/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/128-21.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">What are the best mobile apps for traveling?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/how-to-become-a-billionaire-in-the-next-5-years/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">How to become a billionaire in the next 5 years?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/select-coordinates-which-fall-within-a-radius-of-a-central-point/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">Select coordinates which fall within a radius of a central point?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/why-isnt-music-free/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/128-31.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">Why isn't music free?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/what-are-some-of-your-favorite-deep-awe-inspiring-andor-thought-provoking-movies/">
                                        <span class="topic-avatar">
                                            <img src="http://1.gravatar.com/avatar/3743112a070d24fe4dbd5cb39d6a5e9e?s=30&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D30&amp;r=G" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">What are some of your favorite deep, awe-inspiring, and/or thought-provoking movies?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/what-are-key-strategies-to-acquire-first-100k-users-with-zero-marketing-budget/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/128-11.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">What are key strategies to acquire first 100K users with zero marketing budget?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/what-will-be-the-next-big-thing-after-facebook-and-twitter/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/128-41.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">What will be the next big thing after Facebook and Twitter?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/question/how-do-you-get-a-job-as-a-designer-without-going-to-design-school/">
                                        <span class="topic-avatar">
                                            <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                        <span class="topic-title">How do you get a job as a designer without going to design school?</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- END widget-related-tags -->
                        <div class="widget user-widget">
                            <h3 class="widgettitle">BÄSTA ANVÄNDARE (Poäng)</h3>
                            <div class="hot-user-question">
                                <ul>
                                    <li>
                                        <span class="number">1</span>
                                        <span class="username ">
                                            <a href="/member/admin/" title="admin">
                                                admin                    	</a>
                                        </span>
                                        <span class="points-count" title="1521 Point(s)">
                                            <i class="fa fa-star"></i>
                                            <span>1.5K</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">2</span>
                                        <span class="username ">
                                            <a href="/member/alex/" title="Alex">
                                                Alex                    	</a>
                                        </span>
                                        <span class="points-count" title="515 Point(s)">
                                            <i class="fa fa-star"></i>
                                            <span>515</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">3</span>
                                        <span class="username ">
                                            <a href="/member/marry/" title="Marry">
                                                Marry                    	</a>
                                        </span>
                                        <span class="points-count" title="181 Point(s)">
                                            <i class="fa fa-star"></i>
                                            <span>181</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">4</span>
                                        <span class="username ">
                                            <a href="/member/john/" title="John">
                                                John                    	</a>
                                        </span>
                                        <span class="points-count" title="133 Point(s)">
                                            <i class="fa fa-star"></i>
                                            <span>133</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">5</span>
                                        <span class="username ">
                                            <a href="/member/admindemo/" title="Engine Admin Demo">
                                                Engine Admin Demo                    	</a>
                                        </span>
                                        <span class="points-count" title="69 Point(s)">
                                            <i class="fa fa-star"></i>
                                            <span>69</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget user-widget">
                            <h3 class="widgettitle">BÄSTA ANVÄNDARE (Frågor)</h3>
                            <div class="hot-user-question">
                                <ul>
                                    <li>
                                        <span class="number">1</span>
                                        <span class="username ">
                                            <a href="/member/admin/" title="admin">
                                                admin                    	</a>
                                        </span>
                                        <span class="questions-count" title="5 Question(s)">
                                            <i class="fa fa-question-circle"></i>
                                            <span>5</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">2</span>
                                        <span class="username ">
                                            <a href="/member/alex/" title="Alex">
                                                Alex                    	</a>
                                        </span>
                                        <span class="questions-count" title="4 Question(s)">
                                            <i class="fa fa-question-circle"></i>
                                            <span>4</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">3</span>
                                        <span class="username ">
                                            <a href="/member/smith/" title="Smith">
                                                Smith                    	</a>
                                        </span>
                                        <span class="questions-count" title="2 Question(s)">
                                            <i class="fa fa-question-circle"></i>
                                            <span>2</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">4</span>
                                        <span class="username ">
                                            <a href="/member/marry/" title="Marry">
                                                Marry                    	</a>
                                        </span>
                                        <span class="questions-count" title="1 Question(s)">
                                            <i class="fa fa-question-circle"></i>
                                            <span>1</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">5</span>
                                        <span class="username ">
                                            <a href="/member/john/" title="John">
                                                John                    	</a>
                                        </span>
                                        <span class="questions-count" title="1 Question(s)">
                                            <i class="fa fa-question-circle"></i>
                                            <span>1</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget user-widget">
                            <h3 class="widgettitle">TOP USERS (ANSWERS)</h3>
                            <div class="hot-user-question">
                                <ul>
                                    <li>
                                        <span class="number">1</span>
                                        <span class="username ">
                                            <a href="/member/john/" title="John">
                                                John                    	</a>
                                        </span>
                                        <span class="answers-count" title="5 Answer(s)">
                                            <i class="fa fa-comments"></i>
                                            <span>5</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">2</span>
                                        <span class="username ">
                                            <a href="/member/marry/" title="Marry">
                                                Marry                    	</a>
                                        </span>
                                        <span class="answers-count" title="4 Answer(s)">
                                            <i class="fa fa-comments"></i>
                                            <span>4</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">3</span>
                                        <span class="username ">
                                            <a href="/member/admin/" title="admin">
                                                admin                    	</a>
                                        </span>
                                        <span class="answers-count" title="3 Answer(s)">
                                            <i class="fa fa-comments"></i>
                                            <span>3</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">4</span>
                                        <span class="username ">
                                            <a href="/member/alex/" title="Alex">
                                                Alex                    	</a>
                                        </span>
                                        <span class="answers-count" title="3 Answer(s)">
                                            <i class="fa fa-comments"></i>
                                            <span>3</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">5</span>
                                        <span class="username ">
                                            <a href="/member/smith/" title="Smith">
                                                Smith                    	</a>
                                        </span>
                                        <span class="answers-count" title="3 Answer(s)">
                                            <i class="fa fa-comments"></i>
                                            <span>3</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget user-widget">
                            <h3 class="widgettitle">TOP USERS (LATEST)</h3>
                            <div class="hot-user-question">
                                <ul>
                                    <li>
                                        <span class="number">1</span>
                                        <span class="username latest">
                                            <a href="/member/admindemo/" title="Engine Admin Demo">
                                                Engine Admin Demo                    	</a>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">2</span>
                                        <span class="username latest">
                                            <a href="/member/nana/" title="nana">
                                                nana                    	</a>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">3</span>
                                        <span class="username latest">
                                            <a href="/member/anntt/" title="anntt">
                                                anntt                    	</a>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="number">4</span>
                                        <span class="username latest">
                                            <a href="/member/marry/" title="Marry">
                                                Marry                    	</a>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div><!-- END RIGHT-SIDEBAR -->

                </div>
            </div>		</nav>
        <div class="container-fluid">
            <div class="row">
                <header id="header">
                    <div class="col-md-2 col-xs-2" id="logo">
                        <a href="http://demo.enginethemes.com/qaengine">
                            <img src="/wp-content/uploads/2014/08/logo1.png">
                        </a>
                    </div><!-- logo -->
                    <div class="col-md-8 col-xs-8">
                        <div class="header-menu">
                            <ul>
                                <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="/questions-listing/">Home</a></li>
                                <li id="menu-item-114" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114"><a href="/about-us/">Om oss</a></li>
                                <li id="menu-item-1866" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-1866"><a href="/blog/">Blog</a></li>
                                <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="/term/">Term</a></li>
                                <li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><a href="/contact/">Kontakt</a></li>
                            </ul>
                        </div><!-- menu -->
                        <div class="header-search-wrapper">
                            <section class="buttonset">
                                <button id="showLeftPush"><i class="fa fa-question"></i></button>
                                <button id="showRightPush"><i class="fa fa-bar-chart-o"></i></button>
                            </section>
                            <form id="header_search" method="GET" action="http://demo.enginethemes.com/qaengine" class="disable-mobile">
                                <input type="text" name="keyword" value="" placeholder="Ange nyckelord" autocomplete="off" />
                                <i class="fa fa-search"></i>
                                <div id="search_preview" class="search-preview empty"></div>
                            </form>
                        </div><!-- search -->
                    </div>
                    <div class="col-md-2 col-xs-2 btn-group header-avatar ">
                        <span class="expand dropdown-toggle" type="span" data-toggle="dropdown">
                            <a href="javascript:void(0)" class="dropdown-account " >
                                <span class="avatar"><img src="http://1.gravatar.com/avatar/3743112a070d24fe4dbd5cb39d6a5e9e?s=30&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D30&amp;r=G" class="avatar" alt="" /></span>
                                <span class="display_name">Engine Admin Demo</span>
                                <span class="icon-down"><i class="fa fa-chevron-circle-down"></i></span>
                            </a>
                        </span>
                        <ul class="dropdown-menu dropdown-profile">
                            <li>
                                <a href="/member/admindemo/">
                                    <i class="fa fa-user"></i> User Profile								</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="open-edit-profile edit_profile">
                                    <i class="fa fa-cog"></i> User Settings								</a>
                            </li>

                            <li>
                                <a href="/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fdemo.enginethemes.com%2Fqaengine&amp;_wpnonce=28c3e7bd2a">
                                    <i class="fa fa-power-off"></i> Logga ut								</a>
                            </li>
                        </ul>

                    </div><!-- avatar -->
                </header><!-- END HEADER -->
                <div class="col-md-12 col-xs-12" id="header_sidebar">
                </div>        <div class="col-md-2 disable-mobile left-sidebar">

                    <div class="widget widget-btn">
                        <button type="button" data-toggle="modal" class="action ask-question">
                            <i class="fa fa-plus"></i> Ställ en fråga            </button>
                    </div><!-- END BUTTON MODAL QUESTION -->

                    <div class="widget widget-menus">
                        <div class="menu-left-menu-container"><ul id="menu-left-menu-1" class="menu"><li class="fa-question-circle menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-1765"><a href="/questions/"><i class="fa fa-question-circle"></i>Questions</a></li>
                                <li class="fa-tags menu-item menu-item-type-post_type menu-item-object-page menu-item-183"><a href="/tags-4/"><i class="fa fa-tags"></i>Tags</a></li>
                                <li class="fa-trophy menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="/badges/"><i class="fa fa-trophy"></i>Titlar</a></li>
                                <li class="fa-th-list menu-item menu-item-type-post_type menu-item-object-page menu-item-1742"><a href="/categories-3/"><i class="fa fa-th-list"></i>Categories</a></li>
                                <li class="fa-users menu-item menu-item-type-post_type menu-item-object-page menu-item-1743"><a href="/users-2/"><i class="fa fa-users"></i>Users</a></li>
                            </ul></div>        </div><!-- END LEFT MENU -->

                    <div class="widget widget-recent-activity">

                        <ul>
                            <li>
                                <i class="fa fa-thumbs-up"></i>                    <span>
                                    Du har röstat upp ett svar på en fråga <a href="/question/what-are-some-of-your-favorite-deep-awe-inspiring-andor-thought-provoking-movies/">What are some of your favorite deep, awe-inspiring, and/or thought-provoking movies?</a>.                        <br>
                                    <span class="time-activity">
                                        on September 18, 2014                        </span>
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-question-circle"></i>                    <span>
                                    You edited question .                        <br>
                                    <span class="time-activity">
                                        on September 18, 2014                        </span>
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-comments"></i>                    <span>
                                    You edited answer on question .                        <br>
                                    <span class="time-activity">
                                        on September 18, 2014                        </span>
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-comments"></i>                    <span>
                                    You answered question <a href="/question/what-are-key-strategies-to-acquire-first-100k-users-with-zero-marketing-budget/">What are key strategies to acquire first 100K users with zero marketing budget?</a>.                        <br>
                                    <span class="time-activity">
                                        on September 18, 2014                        </span>
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-undo"></i>                    <span>
                                    Your question  was unvoted.                        <br>
                                    <span class="time-activity">
                                        on August 13, 2014                        </span>
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-undo"></i>                    <span>
                                    Your unvoted an answer on question .                        <br>
                                    <span class="time-activity">
                                        on July 3, 2014                        </span>
                                </span>
                            </li>
                        </ul>

                    </div><!-- END widget-recent-activities -->


                    <div class="copyright">
                        &copy;2014 Enginethemes <br>
                        <a href="/term/">Villkor</a>
                    </div>
                </div><!-- END LEFT-SIDEBAR -->    <div class="col-md-8 main-content">

                    <div class="row">
                        <div class="col-md-12 ads-wrapper">
                            <li id="adrotate_widgets-2" class="widget widget_adrotate_widgets"><span style="font-weight: bold; color: #f00;">Error, no Ad ID set! Check your syntax!</span></li>
                        </div>
                    </div><!-- END WIDGET BANNER -->


                    <div class="clearfix"></div>

                    <div class="row select-category">
                        <div class="col-md-6 col-xs-6 current-category">
                            <span>Frågor & Svar</span>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="select-categories-wrapper">
                                <div class="select-categories">
                                    <select name='cat' id='move_to_category' class='select-grey-bg' >
                                        <option value='0' selected='selected'>Filter by category</option>
                                        <option class="level-0" value="/question-category/technology/">Technology</option>
                                        <option class="level-0" value="/question-category/science/">Science</option>
                                        <option class="level-0" value="/question-category/business/">Business</option>
                                        <option class="level-0" value="/question-category/movie/">Movie</option>
                                        <option class="level-0" value="/question-category/traval/">Travel</option>
                                        <option class="level-0" value="/question-category/music/">Music</option>
                                        <option class="level-0" value="/question-category/food/">Food</option>
                                        <option class="level-0" value="/question-category/education/">Education</option>
                                        <option class="level-0" value="/question-category/design/">Design</option>
                                        <option class="level-0" value="/question-category/cooking/">Cooking</option>
                                        <option class="level-0" value="/question-category/photography/">Photography</option>
                                        <option class="level-0" value="/question-category/marketing/">Marketing</option>
                                        <option class="level-0" value="/question-category/mobile/">Mobile</option>
                                        <option class="level-0" value="/question-category/computer/">Computer</option>
                                        <option class="level-0" value="/question-category/fashion/">Fashion</option>
                                        <option class="level-0" value="/question-category/history/">History</option>
                                        <option class="level-0" value="/question-category/programmer/">Programmer</option>
                                        <option class="level-0" value="/question-category/dictionary/">Dictionary</option>
                                        <option class="level-0" value="/question-category/bloging/">Bloging</option>
                                        <option class="level-0" value="/question-category/software/">Software</option>
                                        <option class="level-0" value="/question-category/hardware/">Hardware</option>
                                        <option class="level-0" value="/question-category/magazines/">Magazines</option>
                                        <option class="level-0" value="/question-category/seo/">SEO</option>
                                        <option class="level-0" value="/question-category/health/">Health</option>
                                        <option class="level-0" value="/question-category/financial/">Financial</option>
                                        <option class="level-0" value="/question-category/security/">Security</option>
                                        <option class="level-0" value="/question-category/sports/">Sports</option>
                                        <option class="level-0" value="/question-category/automotive/">Automotive</option>
                                        <option class="level-0" value="/question-category/board-games/">Board Games</option>
                                        <option class="level-0" value="/question-category/b2b/">B2B</option>
                                        <option class="level-0" value="/question-category/beauty-products/">Beauty Products</option>
                                        <option class="level-0" value="/question-category/camps/">Camps</option>
                                        <option class="level-0" value="/question-category/banking/">Banking</option>
                                        <option class="level-0" value="/question-category/backup-and-data-recovery/">Backup and Data Recovery</option>
                                        <option class="level-0" value="/question-category/biography/">Biography</option>
                                        <option class="level-0" value="/question-category/books/">Books</option>
                                        <option class="level-0" value="/question-category/accessories/">Accessories</option>
                                        <option class="level-0" value="/question-category/accounting/">Accounting</option>
                                        <option class="level-0" value="/question-category/advice/">Advice</option>
                                        <option class="level-0" value="/question-category/arts/">Arts</option>
                                        <option class="level-0" value="/question-category/awards/">Awards</option>
                                        <option class="level-0" value="/question-category/car/">Car</option>
                                        <option class="level-0" value="/question-category/chemistry/">Chemistry</option>
                                        <option class="level-0" value="/question-category/childcare/">Childcare</option>
                                        <option class="level-0" value="/question-category/classifieds/">Classifieds</option>
                                        <option class="level-0" value="/question-category/cleaning/">Cleaning</option>
                                        <option class="level-0" value="/question-category/climbing/">Climbing</option>
                                        <option class="level-0" value="/question-category/clothing/">Clothing</option>
                                        <option class="level-0" value="/question-category/comics/">Comics</option>
                                        <option class="level-0" value="/question-category/conferences/">Conferences</option>
                                        <option class="level-0" value="/question-category/consulting/">Consulting</option>
                                        <option class="level-0" value="/question-category/data-formats/">Data formats</option>
                                        <option class="level-0" value="/question-category/dentistry/">Dentistry</option>
                                        <option class="level-0" value="/question-category/databases/">Databases</option>
                                        <option class="level-0" value="/question-category/directories/">Directories</option>
                                        <option class="level-0" value="/question-category/download/">Download</option>
                                        <option class="level-0" value="/question-category/ebook/">Ebook</option>
                                        <option class="level-0" value="/question-category/e-commerce/">E-Commerce</option>
                                        <option class="level-0" value="/question-category/economics/">Economics</option>
                                        <option class="level-0" value="/question-category/emulators/">Emulators</option>
                                        <option class="level-0" value="/question-category/entertainment/">Entertainment</option>
                                    </select>
                                </div>
                            </div>
                        </div>            
                    </div><!-- END SELECT-CATEGORY -->
                    <div class="clearfix"></div>
                    <div class="row q-filter-waypoints collapse" id="q_filter_waypoints">
                        <div class="col-md-2 col-xs-2">
                            <button type="button" data-toggle="modal" class="action ask-question">
                                <i class="fa fa-plus"></i> Ställ en fråga	            </button>
                        </div>
                        <div class="col-md-8 col-sm-10 col-xs-10">
                            <div class="row">
                                <div class="col-md-2 hidden-xs hidden-sm">
                                    <span class="q-f-title">
                                        Frågor & Svar						</span>
                                </div><!-- END TITLE -->
                                <div class="col-md-5 col-sm-6 col-xs-6">
                                    <ul class="q-f-sort">
                                        <li>
                                            <a class="active" href="/questions/">
                                                Latest		                        </a>
                                        </li>
                                        <li>
                                            <a class="" href="/qaengine/questions/?sort=vote">
                                                Votes		                        </a>
                                        </li>
                                        <li>
                                            <a class="" href="/qaengine/questions/?sort=unanswer">
                                                Unanswered		                        </a>
                                        </li>
                                    </ul><!-- END FILTER -->
                                </div>
                                <div class="col-md-5 col-sm-6 col-xs-6 categories-wrapper">
                                    <div class="select-categories-wrapper">
                                        <div class="select-categories">
                                            <select class="select-grey-bg" id="move_to_category">
                                                <option>Filter by category</option>
                                                <option  value="/question-category/technology/">
                                                    Technology	</option>
                                                <option  value="/question-category/science/">
                                                    Science	</option>
                                                <option  value="/question-category/business/">
                                                    Business	</option>
                                                <option  value="/question-category/movie/">
                                                    Movie	</option>
                                                <option  value="/question-category/traval/">
                                                    Travel	</option>
                                                <option  value="/question-category/music/">
                                                    Music	</option>
                                                <option  value="/question-category/food/">
                                                    Food	</option>
                                                <option  value="/question-category/education/">
                                                    Education	</option>
                                                <option  value="/question-category/design/">
                                                    Design	</option>
                                                <option  value="/question-category/cooking/">
                                                    Cooking	</option>
                                                <option  value="/question-category/photography/">
                                                    Photography	</option>
                                                <option  value="/question-category/marketing/">
                                                    Marketing	</option>
                                                <option  value="/question-category/mobile/">
                                                    Mobile	</option>
                                                <option  value="/question-category/computer/">
                                                    Computer	</option>
                                                <option  value="/question-category/fashion/">
                                                    Fashion	</option>
                                                <option  value="/question-category/history/">
                                                    History	</option>
                                                <option  value="/question-category/programmer/">
                                                    Programmer	</option>
                                                <option  value="/question-category/dictionary/">
                                                    Dictionary	</option>
                                                <option  value="/question-category/bloging/">
                                                    Bloging	</option>
                                                <option  value="/question-category/software/">
                                                    Software	</option>
                                                <option  value="/question-category/hardware/">
                                                    Hardware	</option>
                                                <option  value="/question-category/magazines/">
                                                    Magazines	</option>
                                                <option  value="/question-category/seo/">
                                                    SEO	</option>
                                                <option  value="/question-category/health/">
                                                    Health	</option>
                                                <option  value="/question-category/financial/">
                                                    Financial	</option>
                                                <option  value="/question-category/security/">
                                                    Security	</option>
                                                <option  value="/question-category/sports/">
                                                    Sports	</option>
                                                <option  value="/question-category/automotive/">
                                                    Automotive	</option>
                                                <option  value="/question-category/board-games/">
                                                    Board Games	</option>
                                                <option  value="/question-category/b2b/">
                                                    B2B	</option>
                                                <option  value="/question-category/beauty-products/">
                                                    Beauty Products	</option>
                                                <option  value="/question-category/camps/">
                                                    Camps	</option>
                                                <option  value="/question-category/banking/">
                                                    Banking	</option>
                                                <option  value="/question-category/backup-and-data-recovery/">
                                                    Backup and Data Recovery	</option>
                                                <option  value="/question-category/biography/">
                                                    Biography	</option>
                                                <option  value="/question-category/books/">
                                                    Books	</option>
                                                <option  value="/question-category/accessories/">
                                                    Accessories	</option>
                                                <option  value="/question-category/accounting/">
                                                    Accounting	</option>
                                                <option  value="/question-category/advice/">
                                                    Advice	</option>
                                                <option  value="/question-category/arts/">
                                                    Arts	</option>
                                                <option  value="/question-category/awards/">
                                                    Awards	</option>
                                                <option  value="/question-category/car/">
                                                    Car	</option>
                                                <option  value="/question-category/chemistry/">
                                                    Chemistry	</option>
                                                <option  value="/question-category/childcare/">
                                                    Childcare	</option>
                                                <option  value="/question-category/classifieds/">
                                                    Classifieds	</option>
                                                <option  value="/question-category/cleaning/">
                                                    Cleaning	</option>
                                                <option  value="/question-category/climbing/">
                                                    Climbing	</option>
                                                <option  value="/question-category/clothing/">
                                                    Clothing	</option>
                                                <option  value="/question-category/comics/">
                                                    Comics	</option>
                                                <option  value="/question-category/conferences/">
                                                    Conferences	</option>
                                                <option  value="/question-category/consulting/">
                                                    Consulting	</option>
                                                <option  value="/question-category/data-formats/">
                                                    Data formats	</option>
                                                <option  value="/question-category/dentistry/">
                                                    Dentistry	</option>
                                                <option  value="/question-category/databases/">
                                                    Databases	</option>
                                                <option  value="/question-category/directories/">
                                                    Directories	</option>
                                                <option  value="/question-category/download/">
                                                    Download	</option>
                                                <option  value="/question-category/ebook/">
                                                    Ebook	</option>
                                                <option  value="/question-category/e-commerce/">
                                                    E-Commerce	</option>
                                                <option  value="/question-category/economics/">
                                                    Economics	</option>
                                                <option  value="/question-category/emulators/">
                                                    Emulators	</option>
                                                <option  value="/question-category/entertainment/">
                                                    Entertainment	</option>
                                            </select>
                                        </div>
                                    </div><!-- END SELECT CATEGORIES -->
                                    <div class="number-of-questions-wrapper">
                                        <div class="number-of-questions">
                                            <select id="filter-numbers" class="select-grey-bg">
                                                <option  value="/questions/?numbers=20">20</option>
                                                <option  value="/questions/?numbers=15">15</option>
                                                <option selected value="/questions/?numbers=12">12</option>
                                                <option  value="/questions/?numbers=10">10</option>
                                                <option  value="/questions/?numbers=5">5</option>
                                            </select>
                                        </div>
                                    </div><!-- END POSTS PER PAGE -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row question-filter" id="question_filter">
                        <div class="col-md-6 col-xs-6 sort-questions">
                            <ul>
                                <li>
                                    <a class="active" href="/questions/">
                                        Latest                        </a>
                                </li>
                                <li>
                                    <a class="" href="/questions/?sort=vote">Votes</a>
                                </li>
                                <li>
                                    <a class="" href="/questions/?sort=unanswer">Unanswered</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="number-of-questions-wrapper">
                                <span class="number-of-questions-text">Frågor per sida: </span>
                                <div class="number-of-questions">
                                    <select id="filter-numbers" class="select-grey-bg">
                                        <option  value="/questions/?numbers=20">20</option>
                                        <option  value="/questions/?numbers=15">15</option>
                                        <option selected value="/questions/?numbers=12">12</option>
                                        <option  value="/questions/?numbers=10">10</option>
                                        <option  value="/questions/?numbers=5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div><!-- END QUESTIONS-FILTER -->
                    <div class="main-questions-list">
                        <ul id="main_questions_list">
                            <li class="post-1728 question type-question status-publish hentry question-item" data-id="1728" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-are-some-of-your-favorite-deep-awe-inspiring-andor-thought-provoking-movies/" class="question-title">
                                            What are some of your favorite deep, awe-inspiring, and/or thought-provoking movies?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/facebook/ ">
                                                        facebook                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/google/ ">
                                                        google                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/movie-2/ ">
                                                        movie                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/music-2/ ">
                                                        music                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/social/ ">
                                                        social                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/admindemo/">
                                                <span class="author-avatar">
                                                    <img src="http://1.gravatar.com/avatar/3743112a070d24fe4dbd5cb39d6a5e9e?s=30&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D30&amp;r=G" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Engine Admin Demo</span>
                                            </a>
                                            <span title="69" class="user-badge" style="background-color:#04aad4;">Train</span>                <span class="question-time">
                                                Asked on June 9, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/movie/">Movie.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                23                </span>
                                            views            </li>
                                        <li class="active">
                                            <span class="question-answers">
                                                1                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                1                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-188 question type-question status-publish hentry question-item" data-id="188" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/select-coordinates-which-fall-within-a-radius-of-a-central-point/" class="question-title">
                                            Select coordinates which fall within a radius of a central point?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>I have a database of coordinates in the schema: ID:Latitude:Longitude:name:desc I&#8217;ve set up my google maps application to show the &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/apps/ ">
                                                        apps                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/business-2/ ">
                                                        business                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/glass/ ">
                                                        glass                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/google/ ">
                                                        google                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/technology-2/ ">
                                                        technology                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/admin/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">admin</span>
                                            </a>
                                            <span title="1521" class="user-badge" style="background-color:#780978;">Professor</span>                <span class="question-time">
                                                Asked on June 9, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/technology/">Technology.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                14                </span>
                                            views            </li>
                                        <li class="active">
                                            <span class="question-answers">
                                                0                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                1                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-86 question type-question status-publish hentry question-item" data-id="86" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-are-the-best-mobile-apps-for-traveling/" class="question-title">
                                            What are the best mobile apps for traveling?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>Which mobile apps would you recommend for frequent travelers? On any phone (Android, iPhone, Blackberry, Palm Pre, iPad ect).</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/android/ ">
                                                        android                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/apple/ ">
                                                        apple                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/apps/ ">
                                                        apps                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/google/ ">
                                                        google                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/technology-2/ ">
                                                        technology                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/marry/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-21.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Marry</span>
                                            </a>
                                            <span title="181" class="user-badge" style="background-color:#450ead;">Reviewer</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/mobile/">Mobile.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                28                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                8                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                2                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-78 question type-question status-publish hentry question-item" data-id="78" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/how-to-become-a-billionaire-in-the-next-5-years/" class="question-title">
                                            How to become a billionaire in the next 5 years?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>People do not become billionaires by accident. Demystify as many of the variables as you can before you assemble a &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/billionaire/ ">
                                                        billionaire                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/business-2/ ">
                                                        business                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/admin/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">admin</span>
                                            </a>
                                            <span title="1521" class="user-badge" style="background-color:#780978;">Professor</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/business/">Business.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                16                </span>
                                            views            </li>
                                        <li class="active">
                                            <span class="question-answers">
                                                3                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                2                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-71 question type-question status-publish hentry question-item" data-id="71" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/why-isnt-music-free/" class="question-title">
                                            Why isn&#8217;t music free?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>Why is the music industry so set on the business model of selling music when there are a plethora of &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/apps/ ">
                                                        apps                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/facebook/ ">
                                                        facebook                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/google/ ">
                                                        google                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/marketing-2/ ">
                                                        marketing                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/social/ ">
                                                        social                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/smith/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-31.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Smith</span>
                                            </a>
                                            <span title="21" class="user-badge" style="background-color:#04aad4;">Train</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/music/">Music.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                10                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                2                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                1                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-63 question type-question status-publish hentry question-item" data-id="63" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-are-good-ways-to-learn-to-become-the-best-digital-marketer/" class="question-title">
                                            What are good ways to learn to become the best digital marketer?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>It&#8217;s important to learn from the influencers of your industry. There are a few that come to mind immediately are: &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/apple/ ">
                                                        apple                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/apps/ ">
                                                        apps                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/business-2/ ">
                                                        business                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/important/ ">
                                                        important                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/marketing-2/ ">
                                                        marketing                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/smith/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-31.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Smith</span>
                                            </a>
                                            <span title="21" class="user-badge" style="background-color:#04aad4;">Train</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/marketing/">Marketing.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                9                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                0                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                1                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-59 question type-question status-publish hentry question-item" data-id="59" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-are-key-strategies-to-acquire-first-100k-users-with-zero-marketing-budget/" class="question-title">
                                            What are key strategies to acquire first 100K users with zero marketing budget?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>Make a great product but y’all know that so I’ll just dive into the tactics.. Lorem ipsum dolor sit amet, &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/billionaire/ ">
                                                        billionaire                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/buget/ ">
                                                        buget                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/business-2/ ">
                                                        business                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/marketing-2/ ">
                                                        marketing                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/users/ ">
                                                        users                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/john/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-11.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">John</span>
                                            </a>
                                            <span title="133" class="user-badge" style="background-color:#a35429;">Supporter</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/marketing/">Marketing.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                10                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                1                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                1                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-57 question type-question status-publish hentry question-item" data-id="57" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-were-the-latest-trends-in-marketing-for-2013/" class="question-title">
                                            What were the latest trends in marketing for 2013?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>What strategies should marketers be focussing in the year of new age media ? Lorem ipsum dolor sit amet, consectetur &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/demo/ ">
                                                        demo                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/marketers/ ">
                                                        marketers                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/marketing-2/ ">
                                                        marketing                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/media/ ">
                                                        media                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/alex/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-41.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Alex</span>
                                            </a>
                                            <span title="515" class="user-badge" style="background-color:#ffbf00;">Vice Professor</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/marketing/">Marketing.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                6                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                0                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                0                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-53 question type-question status-publish hentry question-item" data-id="53" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-will-be-the-next-big-thing-after-facebook-and-twitter/" class="question-title">
                                            What will be the next big thing after Facebook and Twitter?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>Why?..And How Soon Do You Think It Will Be? In other words, what will be the next big thing in &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/facebook/ ">
                                                        facebook                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/social/ ">
                                                        social                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/technology-2/ ">
                                                        technology                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/twiter/ ">
                                                        twiter                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/web/ ">
                                                        web                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/alex/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-41.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Alex</span>
                                            </a>
                                            <span title="515" class="user-badge" style="background-color:#ffbf00;">Vice Professor</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/technology/">Technology.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                7                </span>
                                            views            </li>
                                        <li class="active">
                                            <span class="question-answers">
                                                1                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                0                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-51 question type-question status-publish hentry question-item" data-id="51" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/how-to-type-characters-in-google-glass/" class="question-title">
                                            How to type characters in Google Glass?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>In my Glass application, I have created a preference fragment for users to enter some important information such as name, &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/applications/ ">
                                                        applications                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/glass/ ">
                                                        glass                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/google/ ">
                                                        google                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/google-glass/ ">
                                                        google glass                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/technology-2/ ">
                                                        technology                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/alex/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/128-41.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">Alex</span>
                                            </a>
                                            <span title="515" class="user-badge" style="background-color:#ffbf00;">Vice Professor</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/technology/">Technology.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                6                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                0                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                0                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-49 question type-question status-publish hentry question-item" data-id="49" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/what-is-it-like-to-be-a-billionaire/" class="question-title">
                                            What is it like to be a billionaire?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>It is a pain in the neck. Thousands of people depend on me.I can not take a day off and just forget &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/billionaire/ ">
                                                        billionaire                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/business-2/ ">
                                                        business                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/marketing-2/ ">
                                                        marketing                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/paranoid/ ">
                                                        paranoid                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/steal/ ">
                                                        steal                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/admin/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">admin</span>
                                            </a>
                                            <span title="1521" class="user-badge" style="background-color:#780978;">Professor</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/business/">Business.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                7                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                0                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                0                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li><li class="post-42 question type-question status-publish hentry question-item" data-id="42" >
                                <div class="col-md-8 col-xs-8 q-left-content">
                                    <div class="q-ltop-content">
                                        <a href="/question/how-do-you-get-a-job-as-a-designer-without-going-to-design-school/" class="question-title">
                                            How do you get a job as a designer without going to design school?            </a>
                                    </div>
                                    <div class="q-lbtm-content">
                                        <div class="question-excerpt">
                                            <p>The biggest mistake is jumping into Photoshop too fast. Learning Photoshop does not make you a designer, just like buying &#8230;</p>
                                        </div>
                                        <div class="question-cat">
                                            <ul class="question-tags">
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/designer/ ">
                                                        designer                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/illustrator/ ">
                                                        Illustrator                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/photoshop/ ">
                                                        photoshop                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/school/ ">
                                                        school                        </a>
                                                </li>
                                                <li>
                                                    <a class="q-tag" href="/qa-tag/work/ ">
                                                        work                        </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <a href="/member/admin/">
                                                <span class="author-avatar">
                                                    <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                                <span class="author-name">admin</span>
                                            </a>
                                            <span title="1521" class="user-badge" style="background-color:#780978;">Professor</span>                <span class="question-time">
                                                Asked on June 5, 2014 in                </span>

                                            <span class="question-category">
                                                <a href="/question-category/design/">Design.</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- end left content -->
                                <div class="col-md-4 col-xs-4 q-right-content">
                                    <ul class="question-statistic">
                                        <li>
                                            <span class="question-views">
                                                10                </span>
                                            views            </li>
                                        <li class="">
                                            <span class="question-answers">
                                                1                </span>
                                            answers            </li>
                                        <li>
                                            <span class="question-votes">
                                                0                </span>
                                            votes            </li>
                                    </ul>
                                </div><!-- end right content -->
                            </li>                                                                                             
                        </ul>
                    </div><!-- END MAIN-QUESTIONS-LIST -->
                    <div class="row paginations home">
                        <div class="col-md-12">
                            <ul class='page-numbers'>
                                <li><span class='page-numbers current'>1</span></li>
                                <li><a class='page-numbers' href='/questions/page/2/'>2</a></li>
                                <li><a class="next page-numbers" href="/questions/page/2/">></a></li>
                            </ul>

                        </div>
                    </div><!-- END MAIN-PAGINATIONS -->

                    <div class="clearfix"></div>   

                    <div class="row">
                        <div class="col-md-12 ads-wrapper btm-ads-wrapper">
                            <li id="adrotate_widgets-3" class="widget widget_adrotate_widgets"><span style="font-weight: bold; color: #f00;">Error, no Ad ID set! Check your syntax!</span></li>
                        </div>
                    </div><!-- END WIDGET BANNER -->

                </div>
                <div class="col-md-2 disable-mobile right-sidebar">
                    <div class="widget widget-statistic">
                        <p class="questions-count">
                            Questions<br>
                            <span>14</span>
                        </p>
                        <p class="members-count">
                            Medlemmar<br>
                            <span>8</span>
                        </p>
                    </div><!-- END widget-statistic -->
                    <div class="widget widget-related-tags">
                        <h3>MOST USED TAGS</h3>
                        <ul>
                            <li>
                                <a class="q-tag" href="/qa-tag/business-2/">business</a> x 5            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/google/">google</a> x 5            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/marketing-2/">marketing</a> x 5            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/technology-2/">technology</a> x 4            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/apps/">apps</a> x 4            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/movie-2/">movie</a> x 3            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/billionaire/">billionaire</a> x 3            </li>
                            <li>
                                <a class="q-tag" href="/qa-tag/facebook/">facebook</a> x 3            </li>
                        </ul>
                        <a href="/tags-4/">See more tags</a>
                    </div><!-- END widget-related-tags -->
                    <div class="widget widget-hot-questions">
                        <h3>Gillade frågor</h3>
                        <ul>
                            <li>
                                <a href="/question/what-are-the-best-mobile-apps-for-traveling/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/128-21.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">What are the best mobile apps for traveling?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/how-to-become-a-billionaire-in-the-next-5-years/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">How to become a billionaire in the next 5 years?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/select-coordinates-which-fall-within-a-radius-of-a-central-point/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">Select coordinates which fall within a radius of a central point?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/why-isnt-music-free/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/128-31.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">Why isn't music free?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/what-are-some-of-your-favorite-deep-awe-inspiring-andor-thought-provoking-movies/">
                                    <span class="topic-avatar">
                                        <img src="http://1.gravatar.com/avatar/3743112a070d24fe4dbd5cb39d6a5e9e?s=30&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D30&amp;r=G" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">What are some of your favorite deep, awe-inspiring, and/or thought-provoking movies?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/what-are-key-strategies-to-acquire-first-100k-users-with-zero-marketing-budget/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/128-11.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">What are key strategies to acquire first 100K users with zero marketing budget?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/what-will-be-the-next-big-thing-after-facebook-and-twitter/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/128-41.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">What will be the next big thing after Facebook and Twitter?</span>
                                </a>
                            </li>
                            <li>
                                <a href="/question/how-do-you-get-a-job-as-a-designer-without-going-to-design-school/">
                                    <span class="topic-avatar">
                                        <img src="/wp-content/uploads/2014/08/1282.jpg" class="avatar" alt="" />                    </span>
                                    <span class="topic-title">How do you get a job as a designer without going to design school?</span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- END widget-related-tags -->
                    <div class="widget user-widget">
                        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Poäng)</h3>
                        <div class="hot-user-question">
                            <ul>
                                <li>
                                    <span class="number">1</span>
                                    <span class="username ">
                                        <a href="/member/admin/" title="admin">
                                            admin                    	</a>
                                    </span>
                                    <span class="points-count" title="1521 Point(s)">
                                        <i class="fa fa-star"></i>
                                        <span>1.5K</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="username ">
                                        <a href="/member/alex/" title="Alex">
                                            Alex                    	</a>
                                    </span>
                                    <span class="points-count" title="515 Point(s)">
                                        <i class="fa fa-star"></i>
                                        <span>515</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="username ">
                                        <a href="/member/marry/" title="Marry">
                                            Marry                    	</a>
                                    </span>
                                    <span class="points-count" title="181 Point(s)">
                                        <i class="fa fa-star"></i>
                                        <span>181</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">4</span>
                                    <span class="username ">
                                        <a href="/member/john/" title="John">
                                            John                    	</a>
                                    </span>
                                    <span class="points-count" title="133 Point(s)">
                                        <i class="fa fa-star"></i>
                                        <span>133</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">5</span>
                                    <span class="username ">
                                        <a href="/member/admindemo/" title="Engine Admin Demo">
                                            Engine Admin Demo                    	</a>
                                    </span>
                                    <span class="points-count" title="69 Point(s)">
                                        <i class="fa fa-star"></i>
                                        <span>69</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget user-widget">
                        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Frågor)</h3>
                        <div class="hot-user-question">
                            <ul>
                                <li>
                                    <span class="number">1</span>
                                    <span class="username ">
                                        <a href="/member/admin/" title="admin">
                                            admin                    	</a>
                                    </span>
                                    <span class="questions-count" title="5 Question(s)">
                                        <i class="fa fa-question-circle"></i>
                                        <span>5</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="username ">
                                        <a href="/member/alex/" title="Alex">
                                            Alex                    	</a>
                                    </span>
                                    <span class="questions-count" title="4 Question(s)">
                                        <i class="fa fa-question-circle"></i>
                                        <span>4</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="username ">
                                        <a href="/member/smith/" title="Smith">
                                            Smith                    	</a>
                                    </span>
                                    <span class="questions-count" title="2 Question(s)">
                                        <i class="fa fa-question-circle"></i>
                                        <span>2</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">4</span>
                                    <span class="username ">
                                        <a href="/member/marry/" title="Marry">
                                            Marry                    	</a>
                                    </span>
                                    <span class="questions-count" title="1 Question(s)">
                                        <i class="fa fa-question-circle"></i>
                                        <span>1</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">5</span>
                                    <span class="username ">
                                        <a href="/member/john/" title="John">
                                            John                    	</a>
                                    </span>
                                    <span class="questions-count" title="1 Question(s)">
                                        <i class="fa fa-question-circle"></i>
                                        <span>1</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget user-widget">
                        <h3 class="widgettitle">TOP USERS (ANSWERS)</h3>
                        <div class="hot-user-question">
                            <ul>
                                <li>
                                    <span class="number">1</span>
                                    <span class="username ">
                                        <a href="/member/john/" title="John">
                                            John                    	</a>
                                    </span>
                                    <span class="answers-count" title="5 Answer(s)">
                                        <i class="fa fa-comments"></i>
                                        <span>5</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="username ">
                                        <a href="/member/marry/" title="Marry">
                                            Marry                    	</a>
                                    </span>
                                    <span class="answers-count" title="4 Answer(s)">
                                        <i class="fa fa-comments"></i>
                                        <span>4</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="username ">
                                        <a href="/member/admin/" title="admin">
                                            admin                    	</a>
                                    </span>
                                    <span class="answers-count" title="3 Answer(s)">
                                        <i class="fa fa-comments"></i>
                                        <span>3</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">4</span>
                                    <span class="username ">
                                        <a href="/member/alex/" title="Alex">
                                            Alex                    	</a>
                                    </span>
                                    <span class="answers-count" title="3 Answer(s)">
                                        <i class="fa fa-comments"></i>
                                        <span>3</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">5</span>
                                    <span class="username ">
                                        <a href="/member/smith/" title="Smith">
                                            Smith                    	</a>
                                    </span>
                                    <span class="answers-count" title="3 Answer(s)">
                                        <i class="fa fa-comments"></i>
                                        <span>3</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget user-widget">
                        <h3 class="widgettitle">TOP USERS (LATEST)</h3>
                        <div class="hot-user-question">
                            <ul>
                                <li>
                                    <span class="number">1</span>
                                    <span class="username latest">
                                        <a href="/member/admindemo/" title="Engine Admin Demo">
                                            Engine Admin Demo                    	</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="username latest">
                                        <a href="/member/nana/" title="nana">
                                            nana                    	</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="username latest">
                                        <a href="/member/anntt/" title="anntt">
                                            anntt                    	</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="number">4</span>
                                    <span class="username latest">
                                        <a href="/member/marry/" title="Marry">
                                            Marry                    	</a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div><!-- END RIGHT-SIDEBAR -->			</div><!-- END ROW -->
        </div><!-- END CONTAINER-FLUID -->
        <div style="display:none;">
            <div id="wp-temp_id-wrap" class="wp-core-ui wp-editor-wrap tmce-active"><link rel='stylesheet' id='dashicons-css'  href='/wp-includes/css/dashicons.min.css?ver=4.0.1' type='text/css' media='all' />
                <link rel='stylesheet' id='editor-buttons-css'  href='/wp-includes/css/editor.min.css?ver=4.0.1' type='text/css' media='all' />
                <div id="wp-temp_id-editor-container" class="wp-editor-container"><textarea class="wp-editor-area" rows="20" tabindex="5" autocomplete="off" cols="40" name="post_content" id="temp_id"></textarea></div>
            </div>

        </div>

        <!-- MODAL LOGIN / REGISTER -->
        <div class="modal fade modal-submit-questions" id="login_register" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title modal-title-sign-in" id="myModalLabel">Sign In</h4>
                    </div>
                    <div class="modal-body">
                        <form id="signin_form" class="form_modal_style">
                            <label for="username">
                                Username or Email	        	</label>
                            <input type="text" class="email_user"  name="username" id="username" />

                            <label for="password">
                                Password	            </label>
                            <input type="password" class="password_user" id="password" name="password">

                            <a href="javascript:void(0)" class="link_forgot_pass">Forgot password</a>
                            <div class="clearfix"></div>
                            <input type="submit" name="submit" value="Sign in" class="btn-submit">

                            <a href="javascript:void(0)" class="link_sign_up">Sign up</a>

                            <ul class="social-icon clearfix"> 
                                <!-- google plus login -->
                                <li class="gp">
                                    <a id="signinButton" href="#" class="sc-icon color-google">
                                        <i class="fa fa-google-plus-square"></i>
                                    </a>
                                </li>
                                <!-- twitter plus login -->
                                <li class="tw">
                                    <a href="http://demo.enginethemes.com/qaengine?action=twitterauth" class="sc-icon color-twitter">
                                        <i class="fa fa-twitter-square"></i>
                                    </a>
                                </li>
                                <!-- facebook plus login -->
                                <li class="fb">
                                    <a href="#" id="facebook_auth_btn" class="sc-icon color-facebook">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </li>
                            </ul>   

                        </form>

                        <form id="signup_form" class="form_modal_style">
                            <label for="username">
                                Username	        	</label>
                            <input type="text" class="name_user" name="username" id="username" />

                            <label for="email">
                                Email	            </label>
                            <input type="text" class="email_user" name="email" id="email" />

                            <label for="password1">
                                Password	            </label>
                            <input type="password" class="password_user_signup" id="password1" name="password" />

                            <label for="re_password">
                                Retype Password	            </label>
                            <input type="password" class="repeat_password_user_signup" id="re_password" name="re_password" />


                            <div class="clearfix"></div>

                            <input type="submit" name="submit" value="Sign up" class="btn-submit">
                            <a href="javascript:void(0)" class="link_sign_in">Sign in</a>

                            <div class="clearfix"></div>

                            <p class="policy-sign-up">
                                By clicking "Sign up" you indicate that you have read and agree to the <a target="_blank" href="/term/">privacy policy</a> and <a target="_blank" href="/term/">terms of service.</a>	            </p>
                        </form>

                        <form id="forgotpass_form" class="form_modal_style collapse">
                            <label>Ange din e-postadress här</label>
                            <input type="text" class="name_user" name="email" id="email" />
                            <input type="submit" name="submit" value="Skicka" class="btn-submit">
                        </form>

                    </div>
                </div>
            </div>
        </div>	<!-- MODAL LOGIN / REGISTER -->

        <!-- MODAL RESET PASSWORD -->
        <div class="modal fade modal-submit-questions" id="reset_password" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title modal-title-sign-in" id="myModalLabel">Reset Password</h4>
                    </div>
                    <div class="modal-body">

                        <form id="resetpass_form" class="form_modal_style">
                            <input type="hidden" id="user_login" name="user_login" value="" />
                            <input type="hidden" id="user_key" name="user_key" value="">            	
                            <label>Enter your new password here</label>
                            <input type="password" class="name_user" name="new_password" id="new_password" />
                            <input type="password" class="name_user" name="re_new_password" id="re_new_password" />
                            <input type="submit" name="submit" value="Reset" class="btn-submit">
                        </form>	 

                    </div>
                </div>
            </div>
        </div>	<!-- MODAL RESET PASSWORD -->

        <!-- MODAL EDIT PROFILE / CHANGE PASS -->
        <div class="modal fade modal-submit-questions" id="edit_profile" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                    </div>
                    <div class="modal-body">
                        <div class="author-edit" id="user_avatar_container">
                            <span class="author-avatar image" id="user_avatar_thumbnail">
                                <img src="http://1.gravatar.com/avatar/3743112a070d24fe4dbd5cb39d6a5e9e?s=80&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D80&amp;r=G" class="avatar" alt="" />            </span>
                            <div class="edit-info-avatar">
                                <a href="javascript:void(0);" class="upload-avatar-btn" id="user_avatar_browse_button">
                                    Upload New Avatar                </a>
                                <a href="javascript:void(0);" class="link_change_password">Change Password</a>
                                <a href="javascript:void(0);" class="link_change_profile">Change Profile</a>
                            </div>
                            <span class="et_ajaxnonce" id="644338b2b8"></span>
                        </div>

                        <form id="submit_edit_profile" class="form_modal_style edit_profile_form">
                            <label>Full name</label>
                            <input type="text" class="submit-input" maxlength="40" id="display_name" name="display_name" value="Engine Admin Demo">

                            <label>Location</label>
                            <input type="text" class="submit-input" maxlength="40" id="user_location" name="user_location" value="">

                            <label>Facebook</label>
                            <input type="text" class="submit-input" maxlength="80" id="user_facebook" name="user_facebook" value="">

                            <label>Twitter</label>
                            <input type="text" class="submit-input" maxlength="80" id="user_twitter" name="user_twitter" value="">

                            <label>Google+</label>
                            <input type="text" class="submit-input" maxlength="80" id="user_gplus" name="user_gplus" value="">

                            <label>Email</label>
                            <input type="text" class="submit-input" id="user_email" name="user_email" value="demo@enginethemes.com">
                            <input type="checkbox" name="show_email"  id="show_email" /> <label for="show_email" class="checkbox-email">Make this email public.</label>

                            <div class="clearfix"></div>

                            <label>Description</label>
                            <textarea  maxlength="350" class="submit-textarea" id="description" name="description"></textarea>

                            <div class="clearfix"></div>

                            <input type="submit" name="submit" value="Update Profile" class="btn-submit update_profile">
                        </form>

                        <form id="submit_edit_password" class="form_modal_style edit_password_form">
                            <label>Old Password</label>
                            <input type="password" class="submit-input" id="old_password" name="old_password">
                            <label>New Password</label>
                            <input type="password" class="submit-input" id="new_password1" name="new_password">
                            <label>Repeat New Password</label>
                            <input type="password" class="submit-input" id="re_password" name="re_password">
                            <input type="submit" name="submit" value="Change Password" class="btn-submit update_profile">
                        </form>
                    </div>
                </div>
            </div>
        </div>	<!-- MODAL EDIT PROFILE / CHANGE PASS -->

        <!-- MODAL INSERT NEW QUESTION -->
        <!-- MODAL SUBMIT QUESTIONS -->
        <div class="modal fade modal-submit-questions" id="modal_submit_questions" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Ställ en fråga</h4>
                    </div>
                    <div class="modal-body">
                        <form id="submit_question">
                            <input type="hidden" id="qa_nonce" name="qa_nonce" value="bb05c42931">
                            <input type="text" class="submit-input" id="question_title" name="post_title" placeholder="Your Question" />
                            <div class="select-categories">
                                <select id="question_category" name="question_category" class="categories-select">
                                    <option value="">Select Category</option>
                                    <option value="accessories">
                                        Accessories			</option>
                                    <option value="accounting">
                                        Accounting			</option>
                                    <option value="advice">
                                        Advice			</option>
                                    <option value="arts">
                                        Arts			</option>
                                    <option value="automotive">
                                        Automotive			</option>
                                    <option value="awards">
                                        Awards			</option>
                                    <option value="b2b">
                                        B2B			</option>
                                    <option value="backup-and-data-recovery">
                                        Backup and Data Recovery			</option>
                                    <option value="banking">
                                        Banking			</option>
                                    <option value="beauty-products">
                                        Beauty Products			</option>
                                    <option value="biography">
                                        Biography			</option>
                                    <option value="bloging">
                                        Bloging			</option>
                                    <option value="board-games">
                                        Board Games			</option>
                                    <option value="books">
                                        Books			</option>
                                    <option value="business">
                                        Business			</option>
                                    <option value="camps">
                                        Camps			</option>
                                    <option value="car">
                                        Car			</option>
                                    <option value="chemistry">
                                        Chemistry			</option>
                                    <option value="childcare">
                                        Childcare			</option>
                                    <option value="classifieds">
                                        Classifieds			</option>
                                    <option value="cleaning">
                                        Cleaning			</option>
                                    <option value="climbing">
                                        Climbing			</option>
                                    <option value="clothing">
                                        Clothing			</option>
                                    <option value="comics">
                                        Comics			</option>
                                    <option value="computer">
                                        Computer			</option>
                                    <option value="conferences">
                                        Conferences			</option>
                                    <option value="consulting">
                                        Consulting			</option>
                                    <option value="cooking">
                                        Cooking			</option>
                                    <option value="data-formats">
                                        Data formats			</option>
                                    <option value="databases">
                                        Databases			</option>
                                    <option value="dentistry">
                                        Dentistry			</option>
                                    <option value="design">
                                        Design			</option>
                                    <option value="dictionary">
                                        Dictionary			</option>
                                    <option value="directories">
                                        Directories			</option>
                                    <option value="download">
                                        Download			</option>
                                    <option value="e-commerce">
                                        E-Commerce			</option>
                                    <option value="ebook">
                                        Ebook			</option>
                                    <option value="economics">
                                        Economics			</option>
                                    <option value="education">
                                        Education			</option>
                                    <option value="emulators">
                                        Emulators			</option>
                                    <option value="entertainment">
                                        Entertainment			</option>
                                    <option value="fashion">
                                        Fashion			</option>
                                    <option value="financial">
                                        Financial			</option>
                                    <option value="food">
                                        Food			</option>
                                    <option value="hardware">
                                        Hardware			</option>
                                    <option value="health">
                                        Health			</option>
                                    <option value="history">
                                        History			</option>
                                    <option value="magazines">
                                        Magazines			</option>
                                    <option value="marketing">
                                        Marketing			</option>
                                    <option value="mobile">
                                        Mobile			</option>
                                    <option value="movie">
                                        Movie			</option>
                                    <option value="music">
                                        Music			</option>
                                    <option value="photography">
                                        Photography			</option>
                                    <option value="programmer">
                                        Programmer			</option>
                                    <option value="science">
                                        Science			</option>
                                    <option value="security">
                                        Security			</option>
                                    <option value="seo">
                                        SEO			</option>
                                    <option value="software">
                                        Software			</option>
                                    <option value="sports">
                                        Sports			</option>
                                    <option value="technology">
                                        Technology			</option>
                                    <option value="traval">
                                        Travel			</option>
                                </select>
                            </div>
                            <div class="wp-editor-container">
                                <textarea name="post_content" id="insert_question"></textarea>
                            </div>

                            <div id="question-tags-container">
                                <input data-provide="typeahead" type="text" class="submit-input tags-input" id="question_tags" name="question_tags" placeholder="Tag(max 5 tags)" />
                                <span class="tip-add-tag">Press enter to add new tag</span>
                                <ul class="tags-list" id="tag_list"></ul>
                            </div>

                            <input id="add_tag_text" type="hidden" value="You must have 1000 points to add tag. Current, you have to select existed tags." />

                            <button id="btn_submit_question" class="btn-submit-question">SUBMIT QUESTION</button>
                            <p class="term-texts">
                                By posting your question, you agree to the <a target="_blank" href="/term/">privacy policy</a> and <a target="_blank" href="/term/">terms of service.</a>					</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL SUBMIT QUESTIONS -->		
        <!-- MODAL INSERT NEW QUESTION -->

        <!-- MODAL UPLOAD IMAGE -->
        <div class="modal fade modal-submit-questions" id="upload_images" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title modal-title-sign-in" id="myModalLabel">Lägg till bild</h4>
                    </div>
                    <div class="modal-body">
                        <div class="upload-location" id="images_upload_container">
                            <span class="title">Ladda upp bild</span>
                            <div class="input-file">

                                <input type="button" disabled="disabled" style="opacity:0.7;" value="Browse" class="bg-button-file button" id="images_upload_browse_button">                        
                                <span class="filename">Admin has disabled this function.</span>
                                <span class="et_ajaxnonce" id="7b0eab7781"></span> 
                            </div>
                        </div> 
                        <div class="upload-url">
                            <span class="title">Lägg till bild via en URL</span>
                            <div class="input-url">
                                <input type="text" placeholder="https://www.domain.com/images.jpg" id="external_link" class="form-control">
                                <div class="button-event">
                                    <button type="button" id="insert" class="btn">Insert</button>
                                    <a href="javascript:void(0)" class="btn-cancel collapse" data-dismiss="modal">
                                        Cancel	                    	</a>
                                </div>
                            </div>                  
                        </div>					 
                    </div>
                </div>
            </div>
        </div>	<!-- MODAL UPLOAD IMAGE -->

        <!-- TAG TEMPLATE -->
        <script type="text/template" id="tag_item">

            <input type="hidden" name="tags[]" value="{{= stripHTML(name) }}" />
            {{= stripHTML(name) }} <a href="javascript:void(0)" class="delete"><i class="fa fa-times"></i></a>

        </script>
        <script type="text/javascript">
                function stripHTML(html)
                {
                    var tmp = document.createElement("DIV");
                    tmp.innerHTML = html;
                    return tmp.textContent || tmp.innerText;
                }
        </script>
        <!-- TAG TEMPLATE -->
        <!-- MODAL REPORT -->
        <div class="modal fade reportFormModal" id="reportFormModal" style="display:none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4 class="modal-title">Report </h4>
                    </div>
                    <div class="modal-body">
                        <form id="report_form" class="form_modal_style">
                            <label>Your message</label>
                            <textarea id="txt_report" rows="5" placeholder="Got something to say? Type your message here." name= "message"></textarea>
                            <input type="submit" data-loading-text="Loading..." class="btn" value ="Skicka" />
                        </form>
                    </div>
                </div>
            </div>
        </div>	<!-- END MODAL REPORT -->
        <!-- CONTACT REPORT -->
        <div class="modal fade reportFormModal" id="contactFormModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4 class="modal-title">
                            Contact 				</h4>
                    </div>
                    <div class="modal-body">
                        <form id="contact_form" class="form_modal_style">
                            <input type="hidden" id="user_id" value="" />
                            <label>Your message:</label>
                            <textarea id="txt_msg" name="txt_msg" rows="5" placeholder="Got something to say? Type your message here." name= "message"></textarea>
                            <input type="submit" data-loading-text="Loading..." class="btn" value ="Skicka" />
                        </form>
                    </div>
                </div>
            </div>
        </div>	<!-- END CONTACT REPORT -->
        <!-- SEARCH PREVIEW TEMPLATE -->
        <script type="text/template" id="search_preview_template">
            <# _.each(questions, function(question){ #>
            <div class="i-preview">
            <a href="{{= question.permalink }}">
            <div class="i-preview-content">
            <span class="i-preview-title">
            {{= question.post_title.replace( search_term, '<strong>' + search_term + "</strong>" ) }}
            </span>
            </div>
            </a>
            </div>
            <# }); #>
            <div class="i-preview i-preview-showall">
            <# if ( total > 0 && pages > 1 ) { #>
            <a href="{{= search_link }}">View all {{= total }} results</a>
            <# } else if ( pages == 1) { #>
            <a href="{{= search_link }}">View all results</a>
            <# } else { #>
            <a> Inga resultat hittades </a>
            <# } #>
            </div>
        </script>	<!-- SEARCH PREVIEW TEMPLATE -->
        <!--Customizer CSS-->
        <style type="text/css">
        </style>
        <!--/Customizer CSS-->
        <script type='text/javascript'>
            /* <![CDATA[ */
            var click_object = {"ajax_url": "http:\/\/demo.enginethemes.com\/qaengine\/wp-admin\/admin-ajax.php"};
            /* ]]> */
        </script>
        
        <!--  File from WP-Plugin -->
        <script type='text/javascript' src='/wp-content/plugins/adrotate/library/jquery.adrotate.clicktracker.js?ver=0.7'></script>
        <!--  File from WP-Plugin -->
        
        <!--  File from WP-Include -->
        <script type='text/javascript' src='/qaengine/wp-includes/js/underscore.min.js?ver=1.6.0'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/backbone.min.js?ver=1.1.2'></script>
        <!--  File from WP-Include -->
        
        <script type='text/javascript' src='/qaengine/includes/aecore/assets/js/appengine.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/functions.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/bootstrap.min.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/modernizr.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/jquery.simple-text-rotator.min.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/jquery.validate.min.js?ver=4.0.1'></script>
        
        <!--  File from WP-Include -->
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.10.4'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.10.4'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/ui/jquery.ui.position.min.js?ver=1.10.4'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/ui/jquery.ui.menu.min.js?ver=1.10.4'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/jquery/ui/jquery.ui.autocomplete.min.js?ver=1.10.4'></script>
        <!--  File from WP-Include -->
        
        <script type='text/javascript' src='/qaengine/js/libs/waypoints.min.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/waypoints-sticky.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/chosen.jquery.min.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/libs/classie.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/js/scripts.js?ver=4.0.1'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var qa_front = {"form_auth": {"error_msg": "Please fill out all fields required.", "error_user": "Please enter your user name.", "error_email": "Please enter a valid email address.", "error_username": "Please enter a valid username.",
                "error_repass": "Lösenordet matchar inte.",
                "error_url": "Vänligen ange en giltig URL.",
                "error_cb": "Du måste godkänna våra villkor."},
                "texts": {"require_login": "Du måste vara inloggad för att få tillgång till detta.",
                    "enought_points": " Du har inte tillräckligt hög ranking för att utföra detta.",
                    "create_topic": "Skapa ämne",
                    "upload_images": "Upload Images",
                    "insert_codes": "Ange kod",
                    "no_file_choose": "No file chosen.",
                    "require_tags": "Vänligen ange minst 1 tagg.",
                    "add_comment": "Lägg till kommentar",
                    "cancel": "Cancel",
                    "sign_up": "Sign Up",
                    "sign_in": "Sign In",
                    "accept_txt": "Accept",
                    "best_ans_txt": "Best answer",
                    "forgotpass": "Forgot Password",
                    "close_tab": "Du har gjort ändringar som du kanske vill spara.",
                    "confirm_account": "Du måste aktivera ditt konto innan du kan ställa eller besvara en fråga.",
                    "cancel_auth": "Användaren avbröt sitt login eller fullföljde inte registreringen.",
                    "banned_account": "Ditt konto är spärrad, du kan inte utföra detta!"}};
            /* ]]> */
        </script>
        <script type='text/javascript' src='/qaengine/js/front.js?ver=4.0.1'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var facebook_auth = {"appID": "780058875379839", "auth_url": "http:\/\/demo.enginethemes.com\/qaengine\/?action=authentication"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='/qaengine/js/facebookauth.js?ver=4.0.1'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var google_auth = {"appID": "207245237692-ptiqsf0m5tmav257148d9bf5qfoah444.apps.googleusercontent.com", "auth_url": "http:\/\/demo.enginethemes.com\/qaengine\/?action=authentication"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='/qaengine/js/googleauth.js?ver=4.0.1'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wordCountL10n = {"type": "w"};
            /* ]]> */
        </script>
        <!--  File from WP-Admin -->
        <script type='text/javascript' src='/js/word-count.min.js?ver=4.0.1'></script>
        <!--  File from WP-Admin -->
        
        <script type='text/javascript'>
            /* <![CDATA[ */
            var userSettings = {"url": "\/qaengine\/", "uid": "11", "time": "1418640751", "secure": ""};
            /* ]]> */
        </script>
        
        <!--  File from WP-Include -->
        <script type='text/javascript' src='/qaengine/wp-includes/js/utils.min.js?ver=4.0.1'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/editor.min.js?ver=4.0.1'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wpLinkL10n = {"title": "Insert\/edit link", "update": "Update", "save": "Add Link", "noTitle": "(no title)", "noMatchesFound": "No matches found."};
            /* ]]> */
        </script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/wplink.min.js?ver=4.0.1'></script>
            
        <script type="text/javascript">
            tinyMCEPreInit = {
                baseURL: "/qaengine/wp-includes/js/tinymce",
                suffix: ".min",
                mceInit: {'temp_id': {theme: "modern", skin: "lightgray", language: "en", formats: {
                            alignleft: [
                                {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: {textAlign: 'left'}},
                                {selector: 'img,table,dl.wp-caption', classes: 'alignleft'}
                            ],
                            aligncenter: [
                                {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: {textAlign: 'center'}},
                                {selector: 'img,table,dl.wp-caption', classes: 'aligncenter'}
                            ],
                            alignright: [
                                {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: {textAlign: 'right'}},
                                {selector: 'img,table,dl.wp-caption', classes: 'alignright'}
                            ],
                            strikethrough: {inline: 'del'}
                        }, relative_urls: false, remove_script_host: false, convert_urls: false, browser_spellcheck: true, fix_list_elements: true, entities: "38,amp,60,lt,62,gt", entity_encoding: "raw", keep_styles: false, preview_styles: "font-family font-size font-weight font-style text-decoration text-transform", wpeditimage_disable_captions: false, wpeditimage_html5_captions: false, plugins: "charmap,colorpicker,hr,lists,media,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpgallery,wplink,wpdialogs,wpview,image", external_plugins: {"qaimage": "http:\/\/demo.enginethemes.com\/qaengine\/wp-content\/themes\/qaengine\/js\/plugins\/feimage\/editor_plugin_src.js", "qacode": "http:\/\/demo.enginethemes.com\/qaengine\/wp-content\/themes\/qaengine\/js\/plugins\/fecode\/editor_plugin.js", "autoresize": "http:\/\/demo.enginethemes.com\/qaengine\/wp-content\/themes\/qaengine\/js\/plugins\/autoresize\/editor_plugin.js", "autolink": "http:\/\/demo.enginethemes.com\/qaengine\/wp-content\/themes\/qaengine\/js\/plugins\/autolink\/plugin.min.js"}, content_css: "/qaengine/css/editor_content.css", selector: "#temp_id", resize: "vertical", menubar: false, wpautop: true, indent: false, toolbar1: "bold,|,italic,|,underline,|,link,unlink,|,bullist,numlist,qaimage,qacode", toolbar2: "", toolbar3: "", toolbar4: "", tabfocus_elements: ":prev,:next", body_class: "temp_id post-type-question post-status-publish", height: "150", autoresize_min_height: "150", force_p_newlines: false, statusbar: false, force_br_newlines: false, forced_root_block: "", setup: function (ed) {
                            ed.on("keyup", function (e) {
                                if (typeof hasChange == "undefined") {
                                    hasChange = true;
                                }

                                var content = ed.getContent(),
                                        textarea = jQuery("#insert_question"),
                                        container = textarea.parent();
                                label = container.find("label.error");

                                if (content) {
                                    label.hide();
                                    textarea.val(content).removeClass("error").addClass("valid");
                                } else {
                                    label.show();
                                    textarea.removeClass("valid").addClass("error");
                                }
                            });
                            ed.on("focus", function (e) {
                                if (currentUser.ID == 0)
                                    QAEngine.App.openAuthModal(e);
                            });
                            ed.onPaste.add(function (ed, e) {
                                if (typeof hasChange == "undefined") {
                                    hasChange = true;
                                }
                            });
                        }}},
                qtInit: {},
                ref: {plugins: "charmap,colorpicker,hr,lists,media,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpgallery,wplink,wpdialogs,wpview,image", theme: "modern", language: "en"},
                load_ext: function (url, lang) {
                    var sl = tinymce.ScriptLoader;
                    sl.markDone(url + '/langs/' + lang + '.js');
                    sl.markDone(url + '/langs/' + lang + '_dlg.js');
                }
            };
        </script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/tinymce/tinymce.min.js?ver=4104-20140822'></script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/tinymce/plugins/compat3x/plugin.min.js?ver=4104-20140822'></script>
        <script type='text/javascript'>
            tinymce.addI18n('en', {"New document": "New document", "Formats": "Formats", "Headings": "Headings", "Heading 1": "Heading 1", "Heading 2": "Heading 2", "Heading 3": "Heading 3", "Heading 4": "Heading 4", "Heading 5": "Heading 5", "Heading 6": "Heading 6", "Blocks": "Blocks", "Paragraph": "Paragraph", "Blockquote": "Blockquote", "Div": "Div", "Pre": "Pre", "Address": "Address", "Inline": "Inline", "Underline": "Underline", "Strikethrough": "Strikethrough", "Subscript": "Subscript", "Superscript": "Superscript", "Clear formatting": "Clear formatting", "Bold": "Bold", "Italic": "Italic", "Code": "Code", "Source code": "Source code", "Font Family": "Font Family", "Font Sizes": "Font Sizes", "Align center": "Align center", "Align right": "Align right", "Align left": "Align left", "Justify": "Justify", "Increase indent": "Increase indent", "Decrease indent": "Decrease indent", "Cut": "Cut", "Copy": "Copy", "Paste": "Paste", "Select all": "Select all", "Undo": "Undo", "Redo": "Redo", "Ok": "OK", "Cancel": "Cancel", "Close": "Close", "Visual aids": "Visual aids", "Bullet list": "Bulleted list", "Numbered list": "Numbered list", "Square": "Square", "Default": "Default", "Circle": "Circle", "Disc": "Disc", "Lower Greek": "Lower Greek", "Lower Alpha": "Lower Alpha", "Upper Alpha": "Upper Alpha", "Upper Roman": "Upper Roman", "Lower Roman": "Lower Roman", "Name": "Name", "Anchor": "Anchor", "Anchors": "Anchors", "Document properties": "Document properties", "Robots": "Robots", "Title": "Title", "Keywords": "Keywords", "Encoding": "Encoding", "Description": "Description", "Author": "Author", "Insert\/edit image": "Insert\/edit image", "General": "General", "Advanced": "Advanced", "Source": "Source", "Border": "Border", "Constrain proportions": "Constrain proportions", "Vertical space": "Vertical space", "Image description": "Image description", "Style": "Style", "Dimensions": "Dimensions", "Insert image": "Insert image", "Insert date\/time": "Insert date\/time", "Insert\/edit video": "Insert\/edit video", "Poster": "Poster", "Alternative source": "Alternative source", "Paste your embed code below:": "Paste your embed code below:", "Insert video": "Insert video", "Embed": "Embed", "Special character": "Special character", "Right to left": "Right to left", "Left to right": "Left to right", "Emoticons": "Emoticons", "Nonbreaking space": "Nonbreaking space", "Page break": "Page break", "Paste as text": "Paste as text", "Preview": "Preview", "Print": "Print", "Save": "Save", "Fullscreen": "Fullscreen", "Horizontal line": "Horizontal line", "Horizontal space": "Horizontal space", "Restore last draft": "Restore last draft", "Insert\/edit link": "Insert\/edit link", "Remove link": "Remove link", "Could not find the specified string.": "Could not find the specified string.", "Replace": "Replace", "Next": "Next", "Prev": "Prev", "Whole words": "Whole words", "Find and replace": "Find and replace", "Replace with": "Replace with", "Find": "Find", "Replace all": "Replace all", "Match case": "Match case", "Spellcheck": "Check Spelling", "Finish": "Finish", "Ignore all": "Ignore all", "Ignore": "Ignore", "Insert table": "Insert table", "Delete table": "Delete table", "Table properties": "Table properties", "Row properties": "Table row properties", "Cell properties": "Table cell properties", "Row": "Row", "Rows": "Rows", "Column": "Column", "Cols": "Cols", "Cell": "Cell", "Header cell": "Header cell", "Header": "Header", "Body": "Body", "Footer": "Footer", "Insert row before": "Insert row before", "Insert row after": "Insert row after", "Insert column before": "Insert column before", "Insert column after": "Insert column after", "Paste row before": "Paste table row before", "Paste row after": "Paste table row after", "Delete row": "Delete row", "Delete column": "Delete column", "Cut row": "Cut table row", "Copy row": "Copy table row", "Merge cells": "Merge table cells", "Split cell": "Split table cell", "Height": "Height", "Width": "Width", "Caption": "Caption", "Alignment": "Alignment", "Left": "Left", "Center": "Center", "Right": "Right", "None": "None", "Row group": "Row group", "Column group": "Column group", "Row type": "Row type", "Cell type": "Cell type", "Cell padding": "Cell padding", "Cell spacing": "Cell spacing", "Scope": "Scope", "Insert template": "Insert template", "Templates": "Templates", "Background color": "Background color", "Text color": "Text color", "Show blocks": "Show blocks", "Show invisible characters": "Show invisible characters", "Words: {0}": "Words: {0}", "Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.": "Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.\n\nIf you\u2019re looking to paste rich content from Microsoft Word, try turning this option off. The editor will clean up text pasted from Word automatically.", "Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help": "Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help", "You have unsaved changes are you sure you want to navigate away?": "The changes you made will be lost if you navigate away from this page.", "Your browser doesn't support direct access to the clipboard. Please use the Ctrl+X\/C\/V keyboard shortcuts instead.": "Your browser does not support direct access to the clipboard. Please use keyboard shortcuts or your browser\u2019s edit menu instead.", "Insert": "Insert", "File": "File", "Edit": "Edit", "Tools": "Tools", "View": "View", "Table": "Table", "Format": "Format", "Keyboard Shortcuts": "Keyboard Shortcuts", "Toolbar Toggle": "Toolbar Toggle", "Insert Read More tag": "Insert Read More tag", "Read more...": "Read more...", "Distraction Free Writing": "Distraction Free Writing"});
            tinymce.ScriptLoader.markDone('/qaengine/wp-includes/js/tinymce/langs/en.js');
        </script>
        <script type='text/javascript' src='/qaengine/wp-includes/js/tinymce/langs/wp-langs-en.js?ver=4104-20140822'></script>
        <script type="text/javascript">
            tinyMCEPreInit.load_ext("/qaengine/js/plugins/feimage", "en");
            tinymce.PluginManager.load("qaimage", "/qaengine/js/plugins/feimage/editor_plugin_src.js");
            tinyMCEPreInit.load_ext("/qaengine/js/plugins/fecode", "en");
            tinymce.PluginManager.load("qacode", "/qaengine/js/plugins/fecode/editor_plugin.js");
            tinyMCEPreInit.load_ext("/qaengine/js/plugins/autoresize", "en");
            tinymce.PluginManager.load("autoresize", "/qaengine/js/plugins/autoresize/editor_plugin.js");
            tinyMCEPreInit.load_ext("/qaengine/js/plugins/autolink", "en");
            tinymce.PluginManager.load("autolink", "/qaengine/js/plugins/autolink/plugin.min.js");

            var ajaxurl = "/qaengine/wp-admin/admin-ajax.php";
            (function () {
                var init, edId, qtId, firstInit, wrapper;

                if (typeof tinymce !== 'undefined') {
                    for (edId in tinyMCEPreInit.mceInit) {
                        if (firstInit) {
                            init = tinyMCEPreInit.mceInit[edId] = tinymce.extend({}, firstInit, tinyMCEPreInit.mceInit[edId]);
                        } else {
                            init = firstInit = tinyMCEPreInit.mceInit[edId];
                        }

                        wrapper = tinymce.DOM.select('#wp-' + edId + '-wrap')[0];

                        if ((tinymce.DOM.hasClass(wrapper, 'tmce-active') || !tinyMCEPreInit.qtInit.hasOwnProperty(edId)) &&
                                !init.wp_skip_init) {

                            try {
                                tinymce.init(init);

                                if (!window.wpActiveEditor) {
                                    window.wpActiveEditor = edId;
                                }
                            } catch (e) {
                            }
                        }
                    }
                }

                if (typeof quicktags !== 'undefined') {
                    for (qtId in tinyMCEPreInit.qtInit) {
                        try {
                            quicktags(tinyMCEPreInit.qtInit[qtId]);

                            if (!window.wpActiveEditor) {
                                window.wpActiveEditor = qtId;
                            }
                        } catch (e) {
                        }
                        ;
                    }
                }

                if (typeof jQuery !== 'undefined') {
                    jQuery('.wp-editor-wrap').on('click.wp-editor', function () {
                        if (this.id) {
                            window.wpActiveEditor = this.id.slice(3, -5);
                        }
                    });
                } else {
                    for (qtId in tinyMCEPreInit.qtInit) {
                        document.getElementById('wp-' + qtId + '-wrap').onclick = function () {
                            window.wpActiveEditor = this.id.slice(3, -5);
                        }
                    }
                }
            }());
        </script>
        <div id="wp-link-backdrop" style="display: none"></div>
        <div id="wp-link-wrap" class="wp-core-ui" style="display: none">
            <form id="wp-link" tabindex="-1">
                <input type="hidden" id="_ajax_linking_nonce" name="_ajax_linking_nonce" value="b2f91f148a" />		<div id="link-modal-title">
                    Insert/edit link			<button type="button" id="wp-link-close"><span class="screen-reader-text">Close</span></button>
                </div>
                <div id="link-selector">
                    <div id="link-options">
                        <p class="howto">Enter the destination URL</p>
                        <div>
                            <label><span>URL</span><input id="url-field" type="text" name="href" /></label>
                        </div>
                        <div>
                            <label><span>Title</span><input id="link-title-field" type="text" name="linktitle" /></label>
                        </div>
                        <div class="link-target">
                            <label><span>&nbsp;</span><input type="checkbox" id="link-target-checkbox" /> Open link in a new window/tab</label>
                        </div>
                    </div>
                    <p class="howto"><a href="#" id="wp-link-search-toggle">Or link to existing content</a></p>
                    <div id="search-panel">
                        <div class="link-search-wrapper">
                            <label>
                                <span class="search-label">Search</span>
                                <input type="search" id="search-field" class="link-search-field" autocomplete="off" />
                                <span class="spinner"></span>
                            </label>
                        </div>
                        <div id="search-results" class="query-results" tabindex="0">
                            <ul></ul>
                            <div class="river-waiting">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div id="most-recent-results" class="query-results" tabindex="0">
                            <div class="query-notice" id="query-notice-message">
                                <em class="query-notice-default">No search term specified. Showing recent items.</em>
                                <em class="query-notice-hint screen-reader-text">Search or use up and down arrow keys to select an item.</em>
                            </div>
                            <ul></ul>
                            <div class="river-waiting">
                                <span class="spinner"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="submitbox">
                    <div id="wp-link-cancel">
                        <a class="submitdelete deletion" href="#">Cancel</a>
                    </div>
                    <div id="wp-link-update">
                        <input type="submit" value="Add Link" class="button button-primary" id="wp-link-submit" name="wp-link-submit">
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            _.templateSettings = {
                evaluate: /\<\#(.+?)\#\>/g,
                interpolate: /\{\{=(.+?)\}\}/g,
                escape: /\{\{-(.+?)\}\}/g
            };
        </script>
        <script type="text/javascript" id="frontend_scripts">
            (function ($) {
                $(document).ready(function () {


                    if (typeof QAEngine.Views.Front != 'undefined') {
                        QAEngine.App = new QAEngine.Views.Front();
                    }

                    if (typeof QAEngine.Views.Intro != 'undefined') {
                        QAEngine.Intro = new QAEngine.Views.Intro();
                    }

                    if (typeof QAEngine.Views.UserProfile != 'undefined') {
                        QAEngine.UserProfile = new QAEngine.Views.UserProfile();
                    }

                    if (typeof QAEngine.Views.Single_Question != 'undefined') {
                        QAEngine.Single_Question = new QAEngine.Views.Single_Question();
                    }


                    /*======= Open Reset Password Form ======= */

                    /*======= Open Reset Password Form ======= */

                    /*======= Open Confirmation Message Modal ======= */

                });
            })(jQuery);
        </script>
        <script type="text/javascript" id="current_user">
                currentUser = {"ID": "11", "user_login": "admindemo", "user_nicename": "admindemo", "user_email": "demo@enginethemes.com", "user_url": "", "user_registered": "2014-06-09 08:08:29", "user_activation_key": "", "user_status": "0", "display_name": "Engine Admin Demo", "et_avatar": "<img src=\"http:\/\/1.gravatar.com\/avatar\/3743112a070d24fe4dbd5cb39d6a5e9e?s=64&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D64&amp;r=G\" class=\"avatar\" alt=\"Engine Admin Demo\" \/>", "qa_following_questions": "", "user_facebook": "", "user_twitter": "", "user_gplus": "", "user_location": "", "register_status": "", "key_confirm": "", "et_question_count": "1", "et_answer_count": "1", "description": "", "qa_point": "69", "show_email": "", "ban_expired": "January 1, 1970", "is_ban": false, "id": "11", "cap": {"vote_up": true, "add_comment": true, "vote_down": true, "create_tag": true, "edit_question": true, "edit_answer": true, "edit": true}}</script>
        <!-- GOOGLE ANALYTICS CODE --><!-- END GOOGLE ANALYTICS CODE --><!-- AdRotate JS -->
        <script type="text/javascript">
        </script>
        <!-- /AdRotate JS -->

    </body><!-- END BODY -->
</html>
