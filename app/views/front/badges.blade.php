@extends('layouts.default')
@section('content')
<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>Titlar</span>
        </div>
    </div><!-- END SELECT-CATEGORY -->

    <div class="row points-system">
        <div class="col-md-12">
            <h3>Poängsystem</h3>
            <p>Ditt rykte blir bättre när användare röstar på dina inlägg</p>
        </div>
        <div class="clearfix"></div>
        <ul class="points-define">
            <li class="col-md-3">
                <div>
                    <span class="points-count">
                        +2                            </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Ställ en fråga                            </span>
                </div>
            </li>             
            <li class="col-md-3">
                <div>
                    <span class="points-count">
                        +1                            </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        vote                            </span>
                </div>
            </li>    
            <li class="col-md-3">    
                <div>
                    <span class="points-count">
                        +2                            </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Svar                            </span>
                </div>
            </li> 
            <li class="col-md-3">    
                <div>
                    <span class="points-count">
                        +3                            </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Det bästa svaret                            </span>
                </div>
            </li>                                                    
        </ul>

    </div>

    <div class="row badges-system">
        <div class="col-md-12">
            <h3>Titelsystem</h3>
            <p>Ditt rykte blir bättre när användare röstar på dina inlägg</p>
        </div>
        <div class="col-md-12 badge-content">
            <div class="border">
                <div class="col-md-3 question-cat">
                    <span style="background:#780978;" class="user-badge">
                        Professor                        </span><br>
                    <span class="points-count">
                        3000                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng som krävs                        </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 badge-content">
            <div class="border">
                <div class="col-md-3 question-cat">
                    <span style="background:#ffbf00;" class="user-badge">
                        Elit                        </span><br>
                    <span class="points-count">
                        1000                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng som krävs                        </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 badge-content">
            <div class="border">
                <div class="col-md-3 question-cat">
                    <span style="background:#138209;" class="user-badge">
                        Veteran                        </span><br>
                    <span class="points-count">
                        500                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng som krävs                        </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 badge-content">
            <div class="border">
                <div class="col-md-3 question-cat">
                    <span style="background:#450ead;" class="user-badge">
                        Amatör                        </span><br>
                    <span class="points-count">
                        250                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng som krävs                        </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 badge-content">
            <div class="border">
                <div class="col-md-3 question-cat">
                    <span style="background:#a35429;" class="user-badge">
                        Nybörjare                        </span><br>
                    <span class="points-count">
                        50                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng som krävs                        </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 badge-content">
            <div class="border">
                <div class="col-md-3 question-cat">
                    <span style="background:#04aad4;" class="user-badge">
                        Ny medlem                        </span><br>
                    <span class="points-count">
                        0                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng som krävs                        </span>
                </div>
            </div>
        </div>

    </div>

</div>

@stop
