@extends('layouts.default')
@section('content')
<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>Kategorier</span>
        </div>
        <div class="col-md-6 col-xs-6 select-categories input-find-tags">
            <form class="form-input-search" autocomplete="off" method="GET" action="/categories">
                <input autocomplete="off" type="text" name="ckey" id="ckey" value="{{$data['keyword']}}" class="search-users" placeholder="Hitta kategorier" />
                <i class="fa fa-chevron-circle-right"></i>
            </form>
        </div>                       
    </div><!-- END SELECT-CATEGORY -->
   
    <div class="row question-filter">
        <div class="col-md-6 col-xs-6 sort-questions">
            <ul>
                <li>
                    <a href="/categories" class="active">Namn</a>
                </li>
                <li>
                    <a class="" href="/categories/popular">Populära</a>
                </li>                    
            </ul>
        </div>
    </div>
    
    @if(isset($data['alphas']) and isset($data['categories']))
    <div class="main-tag-list">
        <div class="tags-list row ">            
            @foreach($data['alphas'] as $alpha)
            @if(count($data['categories'][$alpha]) > 0)
            <div class="col-md-12">
                <span class="character"><a href="/categories/{{{ $alpha }}}" >{{{ strtoupper($alpha) }}}</a></span>
            </div>
            <div class="clearfix"></div>
            
            @foreach($data['categories'][$alpha] as $category)
            <div class="tag-item cat-item col-md-2 col-xs-2">
                <a href="/category/{{ $category->url_key }}" class="q-tag">{{{ $category->title }}}</a>
            </div>            
            @endforeach
            <div class="col-md-12">
                <div class="clearfix"></div>
                <div class="grey-line"></div>
            </div>
            @endif
            @endforeach
            
        </div>
        <div class="clearfix"></div>
    </div><!-- END MAIN-TAGS-LIST -->    
    @endif
</div>

@stop
