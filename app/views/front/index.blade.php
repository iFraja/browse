@extends('layouts.default') 
@section('content')
<div class="col-md-8 main-content"> 
    <div class="clearfix"></div>
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category"><span>{{{$data['basic']['head']}}}</span></div>        
        <div class="col-md-6 col-xs-6">
            <div class="select-categories-wrapper">
                <div class="select-categories">
                    <?php
                            $categories = DB::table('categories')->where('display_ask_question', 'y')->remember(100)->get();
                        ?>
                    <select name='cat' id='move_to_category' class='select-grey-bg' >
                        <option value='0' selected='selected'>Sortera efter kategori</option>
                        <?php foreach($categories as $cate){ ?>
                        <option value="/category/<?php echo $cate->url_key; ?>" @if($data['basic']['catid'] == $cate->categoryid) selected="selected" @endif ><?php echo $cate->title; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
        </div>            
    </div><!-- END SELECT-CATEGORY -->
    

    <div class="clearfix"></div>
    <div class="row q-filter-waypoints collapse" id="q_filter_waypoints">
        <div class="col-md-2 col-xs-2">
            <button type="button" data-toggle="modal" class="action ask-question">
                <i class="fa fa-plus"></i> Ställ en fråga
            </button>
        </div>
        <div class="col-md-8 col-sm-10 col-xs-10">
            <div class="row">
                <div class="col-md-2 hidden-xs hidden-sm">
                    <span class="q-f-title">{{{$data['basic']['head']}}}</span>
                </div><!-- END TITLE -->                
                <div class="col-md-7 col-sm-6 col-xs-6">
                    <ul class="q-f-sort">
                        <li>
                            <a class="@if ($data['basic']['sort_type'] == 'recent') active @endif" href="{{$data['basic']['base_url']}}">Senaste</a>
                        </li> 
                        <li>
                            <a class="@if ($data['basic']['sort_type'] == 'hot') active @endif" href="{{$data['basic']['base_url']}}/sort/hot">Gillat!</a>
                        </li>
                        <li>
                            <a class="@if ($data['basic']['sort_type'] == 'votes') active @endif" href="{{$data['basic']['base_url']}}/sort/votes">Flest röster</a>
                        </li>
                        <li>
                            <a class="@if ($data['basic']['sort_type'] == 'answers') active @endif" href="{{$data['basic']['base_url']}}/sort/answers">Flest svar</a>
                        </li>
                        @if(Auth::check() and Auth::user()->userid == 1 )
                        <li>
                            <a class="@if ($data['basic']['sort_type'] == 'flagged') active @endif" href="{{$data['basic']['base_url']}}/sort/flagged">Flaggade frågor</a>
                        </li>
                        @endif
                    </ul><!-- END FILTER -->
                </div> 
                <div class="col-md-3 col-sm-6 col-xs-6 categories-wrapper">
                    <div class="select-categories-wrapper" style="width: 100px;">
                        <div class="select-categories">
                            <select class="select-grey-bg" id="move_to_category">
                                <option>Sortera efter kategori</option>
                                <?php foreach($categories as $cate){ ?>
                                <option value="/category/<?php echo $cate->url_key; ?>" @if($data['basic']['catid'] == $cate->categoryid) selected="selected" @endif><?php echo $cate->title; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div><!-- END SELECT CATEGORIES -->
                    <div class="number-of-questions-wrapper">
                        <div class="number-of-questions">
                            <select id="filter-numbers" class="select-grey-bg"> 
                                <option value="{{$data['basic']['base_url']}}?limit=10" @if ($data['basic']['page_limit'] == 10) selected="selected" @endif >10</option>
                                <option value="{{$data['basic']['base_url']}}?limit=50" @if ($data['basic']['page_limit'] == 50) selected="selected" @endif>50</option>
                                <option value="{{$data['basic']['base_url']}}?limit=100" @if ($data['basic']['page_limit'] == 100) selected="selected" @endif>100</option>
                                <option value="{{$data['basic']['base_url']}}?limit=150" @if ($data['basic']['page_limit'] == 150) selected="selected" @endif>150</option>
                                <option value="{{$data['basic']['base_url']}}?limit=200" @if ($data['basic']['page_limit'] == 200) selected="selected" @endif>200</option>
                            </select>
                        </div>
                    </div><!-- END POSTS PER PAGE -->
                </div>
            </div>
        </div>
    </div>
    <div class="row question-filter" id="question_filter">
        <div class="col-md-7 col-xs-6 sort-questions">
            <ul>
                <li>
                    <a class="@if ($data['basic']['sort_type'] == 'recent') active @endif" href="{{$data['basic']['base_url']}}">Senaste</a>
                </li> 
                <li>
                    <a class="@if ($data['basic']['sort_type'] == 'hot') active @endif" href="{{$data['basic']['base_url']}}/sort/hot">Gillat!</a>
                </li>
                <li>
                    <a class="@if ($data['basic']['sort_type'] == 'votes') active @endif" href="{{$data['basic']['base_url']}}/sort/votes">Flest röster</a>
                </li>
                <li>
                    <a class="@if ($data['basic']['sort_type'] == 'answers') active @endif" href="{{$data['basic']['base_url']}}/sort/answers">Flest svar</a>
                </li>
                @if(Auth::check() and Auth::user()->userid == 1 )
                <li>
                    <a class="@if ($data['basic']['sort_type'] == 'flagged') active @endif" href="{{$data['basic']['base_url']}}/sort/flagged">Flaggade frågor</a>
                </li>
                @endif
            </ul>
        </div> 
        <div class="col-md-5 col-xs-6">
            <div class="number-of-questions-wrapper">
                <span class="number-of-questions-text">Frågor per sida: </span>
                <div class="number-of-questions">
                    <select id="filter-numbers" class="select-grey-bg"  >
                        <option value="{{$data['basic']['base_url']}}?limit=10" @if ($data['basic']['page_limit'] == 10) selected="selected" @endif >10</option>
                        <option value="{{$data['basic']['base_url']}}?limit=50" @if ($data['basic']['page_limit'] == 50) selected="selected" @endif>50</option>
                        <option value="{{$data['basic']['base_url']}}?limit=100" @if ($data['basic']['page_limit'] == 100) selected="selected" @endif>100</option>
                        <option value="{{$data['basic']['base_url']}}?limit=150" @if ($data['basic']['page_limit'] == 150) selected="selected" @endif>150</option>
                        <option value="{{$data['basic']['base_url']}}?limit=200" @if ($data['basic']['page_limit'] == 200) selected="selected" @endif>200</option>
                    </select>
                </div>
            </div>
        </div>
    </div><!-- END QUESTIONS-FILTER -->
    @if(count($data['questions']) > 0)
    <div class="main-questions-list">
        <ul id="main_questions_list">
            @foreach($data['questions'] as $record)
            <li class="post-{{{ $record->postid }}} question type-question status-publish hentry question-item" data-id="{{{ $record->postid }}}" >
                <div class="col-md-8 col-xs-8 q-left-content">
                    <div class="q-ltop-content">
                        <a href="/question/{{{ $record->url_key }}}" class="question-title">{{{ $record->title }}}</a>
                    </div>
                    <div class="q-lbtm-content">
                        
                        <div class="question-excerpt">
                            @if($record->content)
                                <p>{{{ substr(strip_tags($record->content), 150) }}} &#8230;</p>
                                @else
                                <p>&nbsp;</p>
                            @endif
                        </div>
                        
                        <div class="question-cat">
                            
                            @if(!empty($record->tags) and count($record->tags) > 0)
                            <ul class="question-tags" >
                                @foreach($record->tags as $tag)
                                <li><a class="q-tag" href="/tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                            @endif
                            <div class="clearfix"></div>
                            <a href="/user/{{$record->user_url_key}}">
                                <span class="author-avatar">
                                    <img src="{{$record->user_picture}}" class="avatar" alt="" />                    </span>
                                <span class="author-name">
                                    Av 
                                    @if($record->handle ==NULL)
                                    me
                                    @else
                                    {{{ $record->handle }}}
                                    @endif
                                </span>
                            </a>
                            
                            <span title="{{ Config::get('badges.'.$record->usertype.'.type') }}" class="user-badge" style="background-color:{{ Config::get('badges.'.$record->usertype.'.color') }};">{{ Config::get('badges.'.$record->usertype.'.type') }}</span>
                            
                            <span class="question-time">
                                 {{ Question::formattedCreatedDate($record->created) }} i
                            </span>

                            <span class="question-category">
                                <a href="/category/{{ $record->cat_url_key }}">{{ $record->categoryname }}</a>
                            </span>
                        </div>
                    </div>
                </div><!-- end left content -->
                <div class="col-md-4 col-xs-4 q-right-content">
                    <ul class="question-statistic">
                        <li><span class="question-views">{{{ $record->views }}}</span> @if($record->views > 1)Visningar @else Visningar @endif</li>
                        <li class="active"><span class="question-answers">{{$record->acount}}</span> @if($record->acount > 1) Svar @else Svar @endif</li>
                        <li><span class="question-votes">{{$record->netvotes}}</span> @if($record->netvotes > 1) Röstat @else Röstat @endif</li>
                    </ul>
                </div><!-- end right content -->
                @if(Auth::check() and Auth::user()->userid == 1 and $data['basic']['sort_type'] == 'flagged')
                <div class="col-md-12 col-xs-12">
                    <ul class="q-right-content" style="width: 100%;text-align: right">
                    <li>
                        <a  data-original-title="Remove Flagged" data-toggle="tooltip" href="javascript:void(0)" onclick="remove_flagged({{$record->postid}})" style="font-size: 20px;color: red">
                        <i class="fa fa-exclamation-triangle"></i>
                    </a>
                </li>
                <li>
                        <a  data-original-title="Delete" data-toggle="tooltip" href="javascript:void(0)" onclick="delete_question({{$record->postid}})" style="font-size: 20px;color: red">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </li>
                </ul>
                </div>
                @endif
            </li>
            @endforeach
        </ul>    
    </div><!-- END MAIN-QUESTIONS-LIST -->
    @else
    <div class="main-questions-list">
        <ul id="main_questions_list" >
            <h2>Inga resultat hittades</h2>
        </ul>
    </div>
    @endif
    <div class="row paginations home">
        <div class="col-md-12">
            <?php 
              
              if(count($data['questions']) > 0){
                echo $data['questions']->appends(array('limit' => $data['basic']['page_limit']))->links();
              }
            ?>
        </div>
    </div><!-- END MAIN-PAGINATIONS -->

    <div class="clearfix"></div>   
</div>
<script>
    function remove_flagged(question_id){
        if(question_id != ""){
            var c = confirm('Bekräfta att du vill ta bort flagga från fråga');
            if(c === true){
                jQuery.post( "/admin-ajax", { method: "remove_flagged", content: question_id })
                .done(function( post_question_id ){
                    jQuery('.post-'+post_question_id).fadeOut(500);
                });
            }
        }
    }
    
    function delete_question(question_id){
        if(question_id != ""){
            var c = confirm('Bekräfta att du vill radera frågan');
            if(c === true){
                jQuery.post( "/admin-ajax", { method: "delete_question", content: question_id })
                .done(function( post_question_id ){
                    jQuery('.post-'+post_question_id).fadeOut(500);
                });
            }
        }
    }
    
</script>    
@stop
