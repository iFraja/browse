@extends('layouts.default')
@section('content')
<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>
                {{$data['user'][0]->username}}'s Profil                </span>
        </div>
    </div><!-- END SELECT-CATEGORY -->
    <div class="row user-statistic highlight">
        
        <div class="col-md-5 col-xs-12 user-info">
            <span class="avatar-80">
                <img alt="" class="avatar" src="{{$data['user'][0]->user_picture}}">
            </span>
            
                <p class="contact-block">
                    @if($data['user'][0]->email_me == 'y')
                    @if(Auth::check()) 
                    @if($data['user'][0]->userid != Auth::user()->userid)
                    <button id="inbox" class="inbox">Kontakt</button>
                    @endif
                    @endif
                    @endif
                    <br >
                    @if(Auth::check()) 
                    @if($data['user'][0]->userid == Auth::user()->userid)
                    <a href="javascript:void(0)" class="edit_profile">Redigera profil</a>
                    @endif
                    @endif
                </p>
                
            <ul>
                <li class="name">{{$data['user'][0]->username}}</li>
                <li class="location">
                    <i class="fa fa-map-marker"></i>
                    {{$data['user'][0]->location}}
                </li>
                
                <li class="email">
                    <i class="fa fa-envelope"></i>
                    @if($data['user'][0]->post_me == 'y')
                    <a href="mailto:{{$data['user'][0]->email}}" >{{$data['user'][0]->email}}</a>
                    @else
                     E-postadress är dold.
                     @endif
                </li>
                
            </ul>
        </div> 
        <div class="col-md-7 col-xs-12 user-post-count">
            <div class="row">
                <div class="col-md-4 col-xs-4 question-cat">
                    <span style="background-color:{{ Config::get('badges.'.$data['user'][0]->usertype.'.color') }};" class="user-badge" title="{{ Config::get('badges.'.$data['user'][0]->usertype.'.type') }}">{{ Config::get('badges.'.$data['user'][0]->usertype.'.type') }}</span>                        <br>
                    <span class="points-count">
                        {{$data['user'][0]->points}}                        </span>
                    <span class="star">
                        <i class="fa fa-star"></i><br>
                        Poäng                        </span>
                </div>
                <div class="col-md-4 col-xs-4">
                    <p class="questions-count">
                        @if($data['total_questions'] > 1) Frågor @else Frågor @endif<br>
                        <span>{{$data['total_questions']}}</span>
                    </p>
                </div>
                <div class="col-md-4 col-xs-4">
                    <p class="answers-count">
                        @if($data['total_answers'] > 1) Svar @else Svar @endif<br>
                        <span>{{$data['total_answers']}}</span>
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 description">
                </div>
            </div>
        </div> 
      
        <div class="col-md-12 clearfix">
            {{$data['user'][0]->description}}
        </div>   
     
    </div><!-- END USER-STATISTIC -->
    
    <div class="row question-filter">
        <div class="col-md-12 sort-questions"> 
            <ul>
                <li>
                    <a href="/user/{{$data['user'][0]->url_key}}" class="@if($data['type'] == 'question') active @endif">Frågor</a>
                </li>
                <li>
                    <a href="/user/{{$data['user'][0]->url_key}}/sort/answer" class="@if($data['type'] == 'answer') active @endif">Svar</a>
                </li>
                <li>
                    <a href="/user/{{$data['user'][0]->url_key}}/sort/comment" class="@if($data['type'] == 'comment') active @endif">Kommentar</a>
                </li>
            </ul>
        </div>
    </div><!-- END QUESTIONS-FILTER -->
    <div class="main-questions-list">
       @if($data['type'] == 'question') 
        <ul id="main_questions_list">
            @foreach($data['questions'] as $record)
            <li class="post-{{{ $record->postid }}} question type-question status-publish hentry question-item" data-id="{{{ $record->postid }}}" >
                <div class="col-md-8 col-xs-8 q-left-content">
                    <div class="q-ltop-content">
                        <a href="/question/{{{ $record->url_key }}}" class="question-title">{{{ $record->title }}}</a>
                    </div>
                    <div class="q-lbtm-content">
                        
                        <div class="question-excerpt">
                            @if($record->content)
                                <p>{{{ substr(strip_tags($record->content), 150) }}} &#8230;</p>
                                @else
                                <p>&nbsp;</p>
                            @endif
                        </div>
                        
                        <div class="question-cat">
                            @if(!empty($record->tags) and count($record->tags) > 0)
                            <ul class="question-tags" >
                                @foreach($record->tags as $tag)
                                <li><a class="q-tag" href="/tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                            @endif
                            <div class="clearfix"></div>
                            <a href="/user/{{$record->user_url_key}}">
                                <span class="author-avatar">
                                    <img src="{{$record->user_picture}}" class="avatar" alt="" />                    </span>
                                <span class="author-name">
                                    Av 
                                    @if($record->handle ==NULL)
                                    me
                                    @else
                                    {{{ $record->handle }}}
                                    @endif
                                </span>
                            </a> 
                            <span title="{{ Config::get('badges.'.$record->usertype.'.type') }}" class="user-badge" style="background-color:{{ Config::get('badges.'.$record->usertype.'.color') }};">{{ Config::get('badges.'.$record->usertype.'.type') }}</span>                <span class="question-time">
                                {{ Question::formattedCreatedDate($record->created) }} i               </span>

                            <span class="question-category">
                                <a href="/category/{{ $record->cat_url_key }}">{{ $record->categoryname }}</a>
                            </span>
                        </div>
                    </div>
                </div><!-- end left content -->
                <div class="col-md-4 col-xs-4 q-right-content">
                    <ul class="question-statistic">
                        <li><span class="question-views">{{{ $record->views }}}</span> @if($record->views > 1) Visningar @else Visningar @endif</li>
                        <li class="active"><span class="question-answers">{{$record->acount}}</span> @if($record->acount > 1) Svar @else Svar @endif</li>
                        <li><span class="question-votes">{{$record->netvotes}}</span> @if($record->netvotes > 1) Röstat @else Röstat @endif</li>
                    </ul>
                </div><!-- end right content -->
            </li>
            @endforeach
        </ul>
        @elseif($data['type'] == 'answer')
        <ul id="main_questions_list">
            @foreach($data['answer_questions'] as $record)
            <li class="post-{{{ $record->postid }}} question type-question status-publish hentry question-item" data-id="{{{ $record->postid }}}" >
                <div class="col-md-8 col-xs-8 q-left-content">
                    <div class="q-ltop-content">
                        <a href="/question/{{{ $record->url_key }}}" class="question-title">RE: {{{ $record->title }}}</a>
                    </div>
                    <div class="q-lbtm-content">
                        
                        <div class="question-excerpt">
                            @if($record->content)
                                <p>{{{ substr(strip_tags($record->content), 150) }}} &#8230;</p>
                                @else
                                <p>&nbsp;</p>
                            @endif
                        </div>
                        
                        <div class="question-cat">
                            
                            <a href="/user/{{$record->user_url_key}}">
                                <span class="author-avatar">
                                    <img src="{{$record->user_picture}}" class="avatar" alt="" />                    </span>
                                <span class="author-name">
                                    Av 
                                    @if($record->handle ==NULL)
                                    me
                                    @else
                                    {{{ $record->handle }}}
                                    @endif
                                </span>
                            </a> 
                            <span title="{{ Config::get('badges.'.$record->usertype.'.type') }}" class="user-badge" style="background-color:{{ Config::get('badges.'.$record->usertype.'.color') }};">{{ Config::get('badges.'.$record->usertype.'.type') }}</span>                <span class="question-time">
                                {{ Question::formattedCreatedDate($record->created) }} i               </span>

                            <span class="question-category">
                                <a href="/category/{{ $record->cat_url_key }}">{{ $record->categoryname }}</a>
                            </span>
                            <div class="clearfix"></div>
                            @if(!empty($record->tags) and count($record->tags) > 0)
                            <ul class="question-tags" >
                                @foreach($record->tags as $tag)
                                <li><a class="q-tag" href="/tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                            @endif
                            
                        </div>
                        
                        @foreach($data['answers'] as $record1)
                          @if($record1->parentid == $record->postid)
                            <div class="quote-answer-style">
                                <div>
                                    <span class=""></span><p>{{$record1->content}}</p>
                                </div>
                            </div>
                          @endif
                        @endforeach 
                        
                    </div>
                </div><!-- end left content -->
                <div class="col-md-4 col-xs-4 q-right-content">
                    <ul class="question-statistic">
                        <li><span class="question-views">{{{ $record->views }}}</span> @if($record->views > 1)Visningar @else Visningar @endif</li>
                        <li class="active"><span class="question-answers">{{$record->acount}}</span> @if($record->acount > 1) Svar @else Svar @endif</li>
                        <li><span class="question-votes">{{$record->netvotes}}</span> @if($record->netvotes > 1) Röstat @else Röstat @endif</li>
                    </ul>
                </div><!-- end right content -->
            </li>
            @endforeach
        </ul>
        @elseif($data['type'] == 'comment')        
        <ul id="main_questions_list">
            @foreach($data['answer_comments'] as $record)
            <li class="post-{{{ $record->postid }}} question type-question status-publish hentry question-item" data-id="{{{ $record->postid }}}" >
                <div class="col-md-12 col-xs-12 q-left-content">
                    <div class="q-lbtm-content">
                        <div class="question-excerpt">
                            @if($record->content)
                                <p>{{{ strip_tags($record->content) }}} &#8230;</p>
                                @else
                                <p>&nbsp;</p>
                            @endif
                        </div>
                        
                        <div class="question-cat">
                            
                            <a href="/user/{{$record->user_url_key}}">
                                <span class="author-avatar">
                                    <img src="{{$record->user_picture}}" class="avatar" alt="" />                    </span>
                                <span class="author-name">
                                    Av 
                                    @if($record->handle ==NULL)
                                    me
                                    @else
                                    {{{ $record->handle }}}
                                    @endif
                                </span>
                            </a>
                            <span title="{{ Config::get('badges.'.$record->usertype.'.type') }}" class="user-badge" style="background-color:{{ Config::get('badges.'.$record->usertype.'.color') }};">{{ Config::get('badges.'.$record->usertype.'.type') }}</span>                <span class="question-time">
                                Svar on {{ Question::formattedCreatedDate($record->created) }}                </span>

                            
                            
                           
                            
                        </div>
                        
                        @foreach($data['comments'] as $record1)
                          @if($record1->parentid == $record->postid)
                            <div class="quote-answer-style">
                                <div>
                                    <span class=""></span><p>{{$record1->content}}</p>
                                </div>
                            </div>
                          @endif
                        @endforeach 
                        
                    </div>
                </div><!-- end left content -->
                <!-- end right content -->
            </li>
            @endforeach
        </ul>
       @endif
    </div><!-- END MAIN-QUESTIONS-LIST -->
    <div class="row paginations home">
        <div class="col-md-12">
            <?php
                if($data['type'] == 'comment'){
                    echo $data['comments']->links();
                }else if($data['type'] == 'answer'){
                    echo $data['answers']->links();
                }else{
                    echo $data['questions']->links();
                }
            ?>
        </div>
    </div><!-- END MAIN-PAGINATIONS -->
</div>
@stop
