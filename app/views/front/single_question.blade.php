@extends('layouts.default')
@section('content')
<?php

function get_all_string_between($string, $start, $end)
{
    $result = array();
    $string = " ".$string;
    $offset = 0;
    while(true)
    {
        $ini = strpos($string,$start,$offset);
        if ($ini == 0)
            break;
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        $result[] = substr($string,$ini,$len);
        $offset = $ini+$len;
    }
    return $result;
}
function parse_content($content){
    $result = get_all_string_between($content, '[img]', '[/img]');
    if(count($result) > 0){
        foreach($result as $res){
         $text_need_to_rep = "[img]".$res."[/img]";
         $rep_text = '<img src="'.$res.'" >';
         $content = str_replace($text_need_to_rep, $rep_text, $content);   
        }
    } 
    return $content;
}
?>
<div class="col-md-8 main-content single-content">
    <div class="row select-category single-head">
        <div class="col-md-2 col-xs-2">
            <span class="back">
                <i class="fa fa-angle-double-left"></i> <a href="/">Hem</a>
            </span>
        </div>
        <div class="col-md-8 col-xs-8">
            <h3>{{$data['question']['title']}}</h3>
        </div>
    </div><!-- END SELECT-CATEGORY -->
    <div id="question_content" class="row question-main-content question-item" data-id="{{$data['question']['postid']}}">
        <!-- Vote section -->
        <div class="col-md-2 col-xs-2 vote-block">
            <!-- vote group -->
            <ul>    
                <!-- vote up -->
                <li title="Rösta upp.">
                    <a   href="javascript:void(0)" data-name="vote_up"  
                         class="action vote vote-up img-circle " >
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </li>
                <!--// vote up -->

                <!--vote point -->
                <li>
                    <span class="vote-count">{{$data['question']['netvotes']}}</span>
                </li>
                <!--// vote point -->
                <!-- vote down -->
                <li title="Rösta ner">
                    <a  href="javascript:void(0)" data-name="vote_down" 
                        class="action vote vote-down img-circle ">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </li>	
                <!--// vote down -->
            </ul>
            <!--// vote group -->
        </div>            <!--// Vote section -->
        <div class="col-md-9 col-xs-9 q-right-content">
            @if(Auth::check())
            <ul class="post-controls" style="display: block;" >
                @if(Auth::user()->userid == 1)
                <li>
                    <a class="post-edit action" data-name="edit" data-original-title="Edit" data-toggle="tooltip" href="javascript:void(0)" >
                        <i class="fa fa-pencil"></i>
                    </a>
                </li>
                <li>
                    <a class="post-delete action" data-name="delete" data-original-title="Delete" data-toggle="tooltip" href="javascript:void(0)">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </li>
                @endif
                <!-- Follow Action -->
                <li style="display: none;">
                    <a class="action follow" data-name="follow" data-original-title="Follow" data-toggle="tooltip" href="javascript:void(0)" >
                        <i class="fa fa-plus-square"></i>
                    </a>
                </li>
                <!-- // Follow Action -->
                <!-- report Action -->
                @if(Auth::user()->userid <> 1)
                <li>
                    <a class="action report" data-name="report" data-original-title="Report" data-toggle="tooltip" href="javascript:void(0)" style="color: red; font-size:20px;"  >
                       <i class="fa fa-exclamation-triangle"></i>
                        Rapportera

                    </a>
                </li>
               @endif 
                <!--// Report Action -->
            </ul>
            @endif
            <!-- question tag -->
            
            <div class="top-content" >
                @if(count($data['answers']) > 0)
                    <span class="answered"><i class="fa fa-check"></i> Besvarad</span>
                @endif
                <?php $current_tags_list = array(); $tag_counter = 0; ?>
                @if($data['question']['tags'])
                <ul class="question-tags">
                    @foreach($data['question']['tags'] as $tag)
                        <?php 
                            $current_tags_list[$tag_counter]['term_id'] = $tag->id;
                            $current_tags_list[$tag_counter]['slug'] = $tag->url_key;
                            $current_tags_list[$tag_counter]['name'] = $tag->name;
                            $tag_counter++;
                        ?>
                        <li><a class="q-tag" href="tag/{{ $tag->url_key }} ">{{ $tag->name }}</a></li>
                    @endforeach
                </ul>
                @endif
                <?php $current_tags_list = json_encode($current_tags_list); ?>
            </div>
            <!--// question tag -->
            <div class="clearfix"></div>

            <div class="question-content">
                {{ parse_content($data['question']['content']) }}
                <p>&nbsp;</p>
            </div>

            <div class="row">
                <div class="col-md-8 col-xs-8 question-cat">
                    <a href="/user/{{{$data['question']['user_url_key']}}}">
                        <span class="author-avatar">
                            <img src="{{{$data['question']['user_picture']}}}" class="avatar" alt="" />                            </span>
                        <span class="author-name">{{{$data['question']['handle']}}}</span>
                    </a>
                    
                    <span style="background-color:{{ Config::get('badges.'.$data['question']['usertype'].'.color') }};" class="user-badge" title="{{ Config::get('badges.'.$data['question']['usertype'].'.type') }}">{{ Config::get('badges.'.$data['question']['usertype'].'.type') }}</span>
                    <span class="question-time">
                         {{ Question::formattedCreatedDate($data['question']['created']) }} i                        </span>
                    <span class="question-category">
                        <a href="/category/{{ $data['question']['cat_url_key'] }}">{{ $data['question']['categoryname'] }}</a>
                    </span>
                </div>
                <div class="col-md-4 col-xs-4 question-control">
                    <ul>
                        <li>
                            <a class="share-social" href="javascript:void(0)" data-toggle="popover" data-placement="top"  data-container="body" data-content='<ul class="socials-share"><li><a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(Request::url())}}%2F&t={{$data['question']['title']}}?" target="_blank" class="btn-fb"><i class="fa fa-facebook"></i></a></li><li><a target="_blank" href="http://twitter.com/share?text={{$data['question']['title']}}?&url={{urlencode(Request::url())}}%2F" class="btn-tw"><i class="fa fa-twitter"></i></a></li><li class="ggplus"><a target="_blank"  href="https://plus.google.com/share?url={{urlencode(Request::url())}}%2F" class="btn-gg"><i class="fa fa-google-plus"></i></a></li></ul>' data-html="true">
                                Dela <i class="fa fa-share"></i>
                            </a>
                        </li>
                        <!-- <li class="collapse">
                            <a href="javascript:void(0)">
                                Report <i class="fa fa-flag"></i>
                            </a>
                        </li> -->
                        <li>
                            <a href="#container_{{$data['question']['postid']}}" class="show-comments ">
                                Kommentar({{count($data['comments'][$data['question']['postid']])}})  <i class="fa fa-comment"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="comments-container @if(count($data['comments'][$data['question']['postid']]) == 0) collapse @endif" id="container_{{$data['question']['postid']}}">
                
                <div class="comments-wrapper">
                    @if(count($data['comments'][$data['question']['postid']]) > 0)
                    @foreach($data['comments'][$data['question']['postid']] as $comment)
                    <div data-id="{{$comment['postid']}}" class="row comment-item">
                        <div class="col-md-2 comment-avatar">
                            <img alt="" class="avatar" src="{{$comment['user_picture']}}">        
                            <p class="cmt-author">
                                <a title="{{{$comment['handle']}}}" href="/user/{{{$comment['user_url_key']}}}">{{{$comment['handle']}}}</a>
                            </p>
                        </div>
                        <div class="col-md-10 comment-content">
                            <div class="cm-content-wrap">
                                <div class="cm-wrap"><p>{{ parse_content($comment['content']) }}</p>
                                </div>
                                <span class="comment-time">{{ Question::formattedCreatedDate($comment['created']) }} </span>
                                @if(Auth::check())
                                @if($comment['userid'] == Auth::user()->userid or Auth::user()->userid == 1)
                                <span class="comment-edit" >
                                    <a class="edit-comment" href="javascript:void(0)">Redigera <i class="fa fa-pencil"></i></a>
                                    @if(Auth::user()->userid == 1)
                                    <a class="delete-comment" href="javascript:void(0)">Radera <i class="fa fa-times"></i></a>
                                    @endif
                                </span>
                                @endif
                                @endif
                            </div><!-- END COMMENT CONTENT -->
                            @if(Auth::check())
                            @if($comment['userid'] == Auth::user()->userid or Auth::user()->userid == 1)
                            <div class="cm-content-edit collapse">
                                <form class="edit-comment">
                                    <input type="hidden" name="qa_nonce" value="323e6c41a0" />
                                    <div class="wp-editor-container">
                                        <textarea name="comment_content" id="edit_comment_{{$comment['postid']}}">{{$comment['content']}}</textarea>
                                    </div>
                                    <div class="row submit-wrapper">
                                        <div class="col-md-3">
                                            <button id="submit_reply" class="btn-submit">Uppdatera</button>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javascript:void(0)" class="cancel-comment">Avbryt</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @endif
                            @endif                          
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
                
                <a data-container="body" data-toggle="popover" data-content="" class="add-comment" data-id="{{$data['question']['postid']}}" href="javascript:void(0)">Skriv kommentar</a>
                <div class="clearfix"></div>
                
                <form class="child-reply" method="POST">
                    <input type="hidden" name="qa_nonce"        value="a5acca4f54" />
                    <input type="hidden" name="comment_post_ID" value="{{$data['question']['postid']}}" />
                    <input type="hidden" name="comment_type"    value="question" />
                    @if(Auth::check())
                    <input type="hidden" name="user_id" value="{{Auth::user()->userid}}" />
                    @else
                    <input type="hidden" name="user_id" value="" />
                    @endif
                    <div id="editor_wrap_{{$data['question']['postid']}}" class="child-answer-wrap collapse">
                        <div class="wp-editor-container">
                            <textarea name="post_content" id="insert_answer_{{$data['question']['postid']}}"></textarea>
                        </div>
                        <div class="row submit-wrapper">
                            <div class="col-md-3 col-xs-3">
                                <button id="submit_reply" class="btn-submit">
                                    Lägg till kommentar                    </button>
                            </div>
                            <div class="col-md-9 col-xs-9">
                                <a href="javascript:void(0)" class="hide-comment">Avbryt</a>
                            </div>
                        </div>
                    </div>
                </form><!-- END SUBMIT FORM COMMENT -->
                
            </div><!-- END COMMENTS CONTAINER -->
            
            @if(Auth::check() and Auth::user()->userid == 1)
            <?php
            $results = DB::select( DB::raw("SELECT qa_flagged.flagged_reasons, qa_flagged.added_date, qa_users.username, qa_users.user_picture, qa_users.url_key  FROM qa_flagged, qa_users WHERE qa_flagged.user_id = qa_users.userid and qa_flagged.question_id = ".$data['question']['postid']) );
            if(count($results) > 0) {    
            ?>
            <div class="clearfix"></div>
            <h3>Flagged Reasons</h3>
            <div class="comments-container">
                <div class="comments-wrapper">
                    @foreach($results as $res)
                    <div class="row comment-item">
                        <div class="col-md-2 comment-avatar">
                            <img alt="" class="avatar" src="{{$res->user_picture}}">        
                            <p class="cmt-author">
                                <a title="{{{$res->username}}}" href="/user/{{{$res->url_key}}}">{{{$res->username}}}</a>
                            </p>
                        </div>
                        <div class="col-md-10 comment-content">
                            <div class="cm-content-wrap">
                                <div class="cm-wrap"><p>{{ parse_content($res->flagged_reasons) }}</p>
                                </div>
                                <span class="comment-time">{{ Question::formattedCreatedDate($res->added_date) }} </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <?php } ?>
            @endif
            
        </div>
    </div><!-- END QUESTION-MAIN-CONTENT -->

    <!-- END WIDGET BANNER -->

    <div class="row answers-filter" id="answers_filter">
        <div class="max-col-md-8">
            <div class="col-md-6 col-xs-6">
                <span class="answers-count"><span class="number">
                        {{count($data['answers'])}}</span> Svar                    </span>
            </div>
        </div>
    </div>

    <!-- ANSWERS LOOP -->
    <div id="answers_main_list">
     <?php $answer_array = array(); ?>   
      @if($data['answers'])
      <?php $ans_counter = 1; ?>
      @foreach($data['answers'] as $answer) 
      <?php $answer_array[]['ID'] = $answer['postid']; ?>
       <div id="{{$answer['postid']}}" class="row question-main-content question-item answer-item">
                <div class="col-md-2 col-xs-2 vote-block">
<!-- vote group -->
<ul>    
<!-- vote up -->
<li title="" data-original-title="Rösta upp.">
    <a class="action vote vote-up img-circle " data-name="vote_up" href="javascript:void(0)" >
            <i class="fa fa-chevron-up"></i>
    </a>
</li>
<!--// vote up -->

<!--vote point -->
<li data-original-title="" title="">
    <span class="vote-count">{{$answer['netvotes']}}</span>
</li>
<!--// vote point -->
<!-- vote down -->
<li title="" data-original-title="Rösta ner">
    <a class="action vote vote-down img-circle " data-name="vote_down" href="javascript:void(0)" >
            <i class="fa fa-chevron-down"></i>
    </a>
</li>	
<!--// vote down -->


{{--@if($data['question']['best_answer_id'] == 0)--}}
    {{--@if($answer['best_answer_id'] == 0)--}}
        @if(Auth::check())
            @if($data['question']['userid'] == Auth::user()->userid or Auth::user()->userid == 1)
              @if($answer['postid'] != $data['question']['best_answer_id'])
            <li title="" data-original-title="Mark as best answer" >
                <a class="action accept-answer img-circle active" data-name="accept-answer" href="javascript:void(0)" data-original-title="" title="">
                    <span title="Ny medlem" class="user-badge" style="background-color:#04aad4;width: 130px; margin-left: -50px;">Markera som bästa svaret</span>
                </a>
            </li>
            @endif
        @endif
    @endif

{{--@endif--}}
</ul>
<!--// vote group -->
</div>
<div class="col-md-9 col-xs-9 q-right-content">
<ul class="post-controls" >
    @if(Auth::check()):
    <li>
        {{--{{ echo '<pre>'; print_r($answer); }}--}}
            @if($data['question']['userid'] == Auth::user()->userid or Auth::user()->userid == 1 )
                <a href="javascript:void(0)" data-name="edit" class="post-edit action">
                    <i class="fa fa-pencil"></i>
                </a>
            @endif
    </li>
    <li>
        @if(Auth::user()->userid == 1)
             <a href="javascript:void(0)" data-name="delete" class="post-delete action" >
                <i class="fa fa-trash-o"></i>
             </a>
        @endif
    </li>
    @endif
    <li style="display: none">
        <a href="javascript:void(0)" data-name="report" class="report action" >
            <i class="fa fa-exclamation-triangle"></i>
        </a>
    </li>
</ul>
<div class="top-content">

    @if($answer['postid'] == $data['question']['best_answer_id'] and $data['question']['best_answer_id'] > 0)
    <span class="answered best-answer">
        <i class="fa fa-check"></i> Bästa svar
    </span>
    @elseif ($data['question']['best_answer_id'] == 0 and $ans_counter == 1 and $answer['netvotes'] > 0)
    <span class="answered best-answer">
        <i class="fa fa-check"></i> Bästa svar
    </span>
    @endif
</div>
<div class="clearfix"></div>

<div class="question-content">
<p>{{ parse_content($answer['content']) }}</p>
</div>

<div class="post-content-edit collapse">
<form class="edit-post">
<input type="hidden" value="9bc034c7f3" name="qa_nonce">
<div class="wp-editor-container">
    <textarea id="edit_post_{{$answer['postid']}}" name="post_content">{{$answer['content']}}</textarea>
</div>
<div class="row submit-wrapper">
    <div class="col-md-2 col-xs-2">
        <button class="btn-submit" id="submit_reply">Uppdatera</button>
    </div>
    <div class="col-md-2 col-xs-2">
        <a class="action cancel-edit-post" data-name="cancel-post-edit" href="javascript:void(0);" data-original-title="" title="">
            Avbryt                    </a>
    </div>
</div>
</form>
</div><!-- END EDIT POST FORM -->

<div class="row cat-infomation">
<!-- Answer owner infomation -->
<div class="col-md-8 col-xs-8 question-cat">
<a href="/user/{{$answer['user_url_key']}}">
    <span class="author-avatar">
        <img alt="" class="avatar" src="{{$answer['user_picture']}}">                </span>
    <span class="author-name">{{$answer['name']}}</span>
</a>

<span style="background-color:{{ Config::get('badges.'.$answer['usertype'].'.color') }};" class="user-badge" title="{{ Config::get('badges.'.$answer['usertype'].'.type') }}">{{ Config::get('badges.'.$answer['usertype'].'.type') }}</span>
             <span class="question-time">
        Besvarad {{ Question::formattedCreatedDate($answer['created']) }}.              </span>
</div>
    <!--// Answer owner infomation -->

<div class="col-md-4 col-xs-4 question-control">
    <!-- share comment , report -->
<ul>
    <li>
        <a class="share-social" href="javascript:void(0)" data-toggle="popover" data-placement="top"  data-container="body" data-content='<ul class="socials-share"><li><a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(Request::url())}}%2F&t={{$data['question']['title']}}?" target="_blank" class="btn-fb"><i class="fa fa-facebook"></i></a></li><li><a target="_blank" href="http://twitter.com/share?text={{$data['question']['title']}}?&url={{urlencode(Request::url())}}%2F" class="btn-tw"><i class="fa fa-twitter"></i></a></li><li class="ggplus"><a target="_blank"  href="https://plus.google.com/share?url={{urlencode(Request::url())}}%2F" class="btn-gg"><i class="fa fa-google-plus"></i></a></li></ul>' data-html="true">
            Dela <i class="fa fa-share"></i>
        </a>
   </li>

    <!-- <li>
        <a href="javascript:void(0)">
            Report <i class="fa fa-flag"></i>
        </a>
    </li> -->
    <!-- comment count -->
    <li>
        <a class="show-comments active" href="#container_{{$answer['postid']}}">
            kommentar({{count($data['comments'][$answer['postid']])}})  <i class="fa fa-comment"></i>
        </a>
    </li>
</ul>
</div>
<!--// share comment , report -->
</div>
<div class="clearfix"></div>
<div class="comments-container @if(count($data['comments'][$answer['postid']]) == 0) collapse @endif" id="container_{{$answer['postid']}}">

                <div class="comments-wrapper">
                    @if(count($data['comments'][$answer['postid']]) > 0)
                    @foreach($data['comments'][$answer['postid']] as $comment)
                    <div data-id="{{$comment['postid']}}" class="row comment-item">
                        <div class="col-md-2 comment-avatar">
                            <img alt="" class="avatar" src="{{$comment['user_picture']}}">
                            <p class="cmt-author">
                                <a title="{{{$comment['handle']}}}" href="/user/{{{$comment['user_url_key']}}}">{{{$comment['handle']}}}</a>
                            </p>
                        </div>
                        <div class="col-md-10 comment-content">
                            <div class="cm-content-wrap">
                                <div class="cm-wrap"><p>{{ parse_content($comment['content']) }}</p>
                                </div>
                                <span class="comment-time">{{ Question::formattedCreatedDate($comment['created']) }} </span>
                                @if(Auth::check())
                                @if($comment['userid'] == Auth::user()->userid or Auth::user()->userid == 1)
                                <span class="comment-edit">
                                    <a class="edit-comment" href="javascript:void(0)">Redigera <i class="fa fa-pencil"></i></a>
                                   <!-- <a class="delete-comment" href="javascript:void(0)">Radera <i class="fa fa-times"></i></a>-->

                                    @if(Auth::user()->userid == 1)
                                        <a class="delete-comment" href="javascript:void(0)">Radera <i class="fa fa-times"></i></a>
                                    @endif
                                </span>
                                @endif
                                @endif
                            </div><!-- END COMMENT CONTENT -->
                            @if(Auth::check())
                            @if($comment['userid'] == Auth::user()->userid or Auth::user()->userid == 1)
                            <div class="cm-content-edit collapse">
                                <form class="edit-comment">
                                    <input type="hidden" name="qa_nonce" value="323e6c41a0" />
                                    <div class="wp-editor-container">
                                        <textarea name="comment_content" id="edit_comment_{{$comment['postid']}}">{{$comment['content']}}</textarea>
                                    </div>
                                    <div class="row submit-wrapper">
                                        <div class="col-md-3">
                                            <button id="submit_reply" class="btn-submit">Uppdatera</button>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javascript:void(0)" class="cancel-comment">Avbryt</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                           @endif
                           @endif
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
                <a data-container="body" data-toggle="popover" data-content="" class="add-comment" data-id="{{$answer['postid']}}" href="javascript:void(0)">Skriv kommentar</a>
                <div class="clearfix"></div>
                <form class="child-reply" method="POST">
                    <input type="hidden" name="qa_nonce"        value="a5acca4f54" />
                    <input type="hidden" name="comment_post_ID" value="{{$answer['postid']}}" />
                    <input type="hidden" name="comment_type"    value="answer" />
                    @if(Auth::check())
                    <input type="hidden" name="user_id" value="{{Auth::user()->userid}}" />
                    @else
                    <input type="hidden" name="user_id" value="" />
                    @endif
                    <div id="editor_wrap_{{$answer['postid']}}" class="child-answer-wrap collapse">
                        <div class="wp-editor-container">
                            <textarea name="post_content" id="insert_answer_{{$answer['postid']}}"></textarea>
                        </div>
                        <div class="row submit-wrapper">
                            <div class="col-md-3 col-xs-3">
                                <button id="submit_reply" class="btn-submit">
                                    Lägg till kommentar                    </button>
                            </div>
                            <div class="col-md-9 col-xs-9">
                                <a href="javascript:void(0)" class="hide-comment">Avbryt</a>
                            </div>
                        </div>
                    </div>
                </form><!-- END SUBMIT FORM COMMENT -->

            </div><!-- END COMMENTS CONTAINER -->
    </div>
</div>
      <?php $ans_counter++; ?>
      @endforeach
      @endif
      <!-- END REPLY-ITEM -->
    </div>
    <!-- ANSWERS LOOP -->
    <div class="row paginations collapse">
        <div class="col-md-12"></div>
    </div>
    <!-- END PAGINATIONS -->

    <script type="text/javascript">
        @if($data['answers'])
	//var answersData  = {{json_encode($answer_array)}};
        @else
        var answersData  = [];
        @endif
	var commentsData = [];
    </script>
<style>
#post_content{
	display:block !important;
	resize:none;
	cursor:pointer;
}
iframe#post_content_ifr{
	display:none !important;
}
</style>
    <div class="row form-reply">
        <div class="col-md-12">
            <h3>Ditt svar</h3>
            <form id="form_reply" method="POST">
                <input type="hidden" name="qa_nonce" value="f2ed5eb767" />
                <input type="hidden" name="post_parent" value="{{$data['question']['postid']}}" />
                <div id="wp-post_content-wrap" class="wp-core-ui wp-editor-wrap tmce-active">
                    <div id="wp-post_content-editor-container" class="wp-editor-container" >
                        <textarea data-target="#login_register2" data-toggle="modal" class="wp-editor-area" rows="8" tabindex="5" autocomplete="off" cols="40" name="post_content"  id="post_content" style="display:block !important;"></textarea>
                    </div>

                </div>
                <div class="row submit-wrapper">
                    <div class="col-md-2">
                        <button type="button" class="ask-upload-image" onclick="upload_mobile_image();">Ladda upp bild</button>
                    </div>

                    <div class="col-md-2 post_answer_clear_btn">
                        <button id="submit_reply" class="btn-submit">
                            Skicka svar                            </button>
                    </div>
                    <div class="col-md-10 term-texts">
                        Genom att posta din fråga <a target="_blank" href="/privacy-policy">godkänner</a> du <a target="_blank" href="/terms-of-service">våra villkor.</a>                        </div>
                </div>
            </form>
            <form id="uploadimage" name="uploadimage" action="" method="post" enctype="multipart/form-data" style="display: none;">
                <input type="file" name="images_upload" id="images_upload"  />
            </form>
        </div>
    </div><!-- END FORM REPLY -->

    <div class="clearfix"></div>

    <script type="text/javascript">

        currentQuestion = {"ID":{{$data['question']['postid']}},"qemail_me":"{{$data['question']['qemail_me']}}","post_author":"{{$data['question']['userid']}}","post_date":"{{$data['question']['created']}}","post_date_gmt":"{{$data['question']['created']}}","post_content":"{{$data['question']['content']}}","post_title":"{{$data['question']['title']}}","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"{{$data['question']['title']}}","to_ping":"","pinged":"","post_modified":"{{$data['question']['created']}}","post_modified_gmt":"{{$data['question']['created']}}","post_content_filtered":"","post_parent":0,"guid":"{{urlencode(Request::url())}}","menu_order":0,"post_type":"question","post_mime_type":"","comment_count":"0","filter":"raw","question_category":[{"term_id":{{$data['question']['categoryid']}},"name":"{{$data['question']['categoryname']}}","slug":"advice","term_group":0,"term_taxonomy_id":0,"taxonomy":"question_category","description":"","parent":0,"count":3,"filter":"raw"}],"qa_tag":<?php echo $current_tags_list; ?>,"et_vote_count":0,"et_view_count":"8","et_answers_count":0,"et_users_follow":"11","et_answer_authors":[],"et_last_author":"","et_vote_up_authors":[],"et_vote_down_authors":[],"et_best_answer":"","id":{{$data['question']['postid']}},"voted_down":true,"voted_up":true,"user_badge":"<span title=\u0022130\u0022 class=\u0022user-badge\u0022 style=\u0022background-color:#a35429;\u0022>Supporter<\/span>","answered":false,"has_category":true,"content_filter":"{{$data['question']['content']}}","content_edit":"{{$data['question']['content']}}","author_name":"{{$data['question']['handle']}}","followed":false,"reported":false};

       function upload_mobile_image(){
           jQuery('#images_upload').click();
       }

       jQuery(document).ready(function (e) {

        jQuery("#images_upload").change(function() {
            jQuery('#uploadimage').submit();
        });

        jQuery("#uploadimage").on('submit',(function(e) {
            e.preventDefault();
            jQuery.ajax({
                url: "/admin-ajax",
                type: "POST",
                dataType: "json",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(response){
                    if(response.success){
                        tinyMCE.get('post_content').execCommand('mceInsertContent', false, "<img style='width:70%;' src='" + response.data + "' >");
                        //tinyMCE.activeEditor.setContent("<img style='width:70%;' src='" + response.data + "' >");
                        //tinymce.activeEditor.execCommand('mceInsertContent', false, "<img style='width:70%;' src='" + response.data + "' >");
                    }
                }
            });
        }));


    });

    </script>



</div>



@stop
