@extends('layouts.default')
@section('content')
<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>{{$page_data['page_name']}}</span>
        </div>
        <div class="col-md-6 col-xs-6 select-categories input-find-tags" style="display: none">
            <form class="form-input-search" autocomplete="off" method="GET" action="/tags">
                <input autocomplete="off" type="text" name="ckey" id="ckey" value="" class="search-users" placeholder="Hitta tagg" />
                <i class="fa fa-chevron-circle-right"></i>
            </form>
        </div>                       
    </div><!-- END SELECT-CATEGORY -->
   
    <div class="col-md-12">
        <div class="blog-wrapper">
            <div class="row">
                <div id="page_content" class="col-md-12 ">
                    <div class="blog-content">
                        <p style="color: #7b7b7b;"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the hotelengine 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p style="color: #7b7b7b;"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@stop
