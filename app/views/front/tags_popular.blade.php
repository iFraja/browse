@extends('layouts.default')
@section('content')

<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>Taggar</span>
        </div>
        <div class="col-md-6 col-xs-6 select-categories input-find-tags">
            <form class="form-input-search" autocomplete="off" method="GET" action="/tags">
                <input autocomplete="off" type="text" name="ckey" id="ckey" value="" class="search-users" placeholder="Hitta tagg" />
                <i class="fa fa-chevron-circle-right"></i>
            </form>
        </div>                       
    </div><!-- END SELECT-CATEGORY -->
   
    <div class="row question-filter">
        <div class="col-md-6 col-xs-6 sort-questions">
            <ul>
                <li>
                    <a class="active" href="/tags/popular">Populära</a>
                </li>                    
                <li>
                    <a href="/tags" class="">Namn</a>
                </li>                
            </ul>
        </div>
    </div>
    
    @if(isset($data['tags']))
    <div class="main-tag-list">
        <div class="tags-list row ">            
            @foreach($data['tags'] as $tag)
            <div class="tag-item">
                    <a class="q-tag" href="/tag/{{ $tag->url_key }}">{{{ $tag->name }}}</a>
                    <p>&nbsp;</p>
            </div>
            @endforeach
            <div class="col-md-12">
                <div class="clearfix"></div>
                <div class="grey-line"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div><!-- END MAIN-TAGS-LIST -->    
    @endif
</div>

@stop
