@extends('layouts.default')
@section('content')
<div class="col-md-8 main-content">
    <div class="row select-category">
        <div class="col-md-6 col-xs-6 current-category">
            <span>Kategorier</span>
        </div>
        <div class="col-md-6 col-xs-6 select-categories input-find-tags">
            <form class="form-input-search" autocomplete="off" method="GET" action="/categories">
                <input autocomplete="off" type="text" name="ckey" id="ckey" value="" class="search-users" placeholder="Hitta kategorier" />
                <i class="fa fa-chevron-circle-right"></i>
            </form>
        </div>                       
    </div><!-- END SELECT-CATEGORY -->

    <div class="row question-filter">
        <div class="col-md-6 col-xs-6 sort-questions">
            <ul>
                <li>
                    <a href="/categories" >Namn</a>
                </li>
                <li>
                    <a href="/categories/popular" class="active">Populära</a>
                </li>                    
            </ul>
        </div>
    </div>
    
    @if(isset($data['categories']))
    <div class="main-tag-list">
        <div class="tags-list row cats-wrapper">
                @foreach($data['categories'] as $category)
                <div class="tag-item cat-item col-md-2 col-xs-2">
                    <a href="/category/{{ $category->url_key }}" class="q-tag">{{{ $category->title }}}</a>
                 </div>   
                @endforeach
            <div class="col-md-12">
                <div class="clearfix grey-line pull-left"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div><!-- END MAIN-TAGS-LIST -->    
    @endif
</div>
@stop