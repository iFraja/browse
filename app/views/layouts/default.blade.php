<!DOCTYPE html>
<html lang="en-US">
    <!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
    <!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->    
    <head>
        @include('includes.head')
    </head>
    <body class="archive post-type-archive post-type-archive-question logged-in cbp-spmenu-push single-question">
        @include('includes.navigations')
        <div class="container-fluid">
            <div class="row">
                @include('includes.header')
                @include('includes.left')
                @yield('content')
                @include('includes.right')
             </div>
        </div>
        @include('includes.footer')
    </body>
</html>
