<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Skriv ditt nya lösenord här</h2>

		<div>
			To reset your password, complete this form: {{ URL::to('password/reset', array($token)) }}.
		</div>
	</body>
</html>