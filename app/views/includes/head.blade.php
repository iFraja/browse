<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">
<title>Frågor & Svar</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href='//fonts.googleapis.com/css?family=Lato:400,700&subset=latin,cyrillic,cyrillic-ext,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="/qaengine/js/libs/selectivizr-min.js"></script>
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script type="text/javascript">
	ae_globals = {"ajaxURL":"/admin-ajax","imgURL":"","posts_per_page":"12","homeURL":"","user_confirm":0,"pending_questions":0,"pending_answers":0,"introURL":"/","gplus_client_id":"207245237692-ptiqsf0m5tmav257148d9bf5qfoah444.apps.googleusercontent.com","plupload_config":{"max_file_size":"3mb","url":"/admin-ajax","flash_swf_url":"/qaengine\/qa-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"/qaengine\/qa-includes\/js\/plupload\/plupload.silverlight.xap"}}		
</script>

<link rel='stylesheet' id='bootstrap-css'  href='/qaengine/css/libs/bootstrap.min.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='/qaengine/css/libs/font-awesome.min.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='main-style-css'  href='/qaengine/css/main.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='editor-style-css'  href='/qaengine/css/editor.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='push-menu-css'  href='/qaengine/css/libs/push-menu.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='chosen-css'  href='/qaengine/css/libs/chosen.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='custom-style-css'  href='/qaengine/css/custom.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='/qaengine/style.css?ver=4.0.1' type='text/css' media='all' />

<!--  File from QA-Include -->
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/plupload/plupload.full.min.js?ver=2.1.1'></script>
<!--  File from QA-Include -->

<!-- This site is using AdRotate v3.10.16 to display their advertisements - https://www.adrotateplugin.com/ -->

<style type="text/css">
    .tp-caption.demo-1 {
        font-size:35px;
        line-height:45px;
        font-weight:300;
        font-family:"Open Sans";
        color:rgb(0, 0, 0);
        text-decoration:none;
        background-color:transparent;
        text-shadow:none;
        width:260px;
        border-width:0px;
        border-color:rgb(0, 0, 0);
        border-style:none;
    }
</style>