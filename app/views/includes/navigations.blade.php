<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 left-sidebar">

                <div class="widget widget-btn">
                    <a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#login_register2" class="action ask-question abt">
                        <i class="fa fa-plus"></i> Ställ en fråga                    </a>
                </div><!-- END BUTTON MODAL QUESTION -->

                <div class="widget widget-menus">
                    <div class="menu-left-menu-container">
                        <ul id="menu-left-menu" class="menu">
                            <li class="fa-question-circle @if(Request::is('questions') or Request::is('questions/*') or Request::is('questions-*') or Request::is('question/*')) current-menu-item @endif"><a href="/questions"><i class="fa fa-question-circle"></i>Questions</a></li>
                            <li class="fa-tags @if(Request::is('tags') or Request::is('tags/*') or Request::is('tag/*')) current-menu-item @endif"><a href="/tags/popular"><i class="fa fa-tags"></i>Taggar</a></li>
                            <li class="fa-trophy @if(Request::is('badges')) current-menu-item @endif "><a href="/badges"><i class="fa fa-trophy"></i>Titlar</a></li>
                            <li class="fa-th-list @if(Request::is('categories') or Request::is('categories/*') or Request::is('category/*')) current-menu-item @endif"><a href="/categories"><i class="fa fa-th-list"></i>Kategorier</a></li>
                            <li class="fa-users @if(Request::is('users') or Request::is('users/*') or Request::is('user/*')) current-menu-item @endif"><a href="/users"><i class="fa fa-users"></i>Användare</a></li>
                        </ul>
                        
                    </div>                
                </div><!-- END LEFT MENU -->

               <?php 
    if(Auth::check()) { 
        $user_status_array = array();
        $counter = 0;
        $questions = DB::table('posts')->select(array('title', 'url_key','created'))->where('type', '=', 'Q')->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($questions) > 0){
            foreach($questions as $q){
                
                $user_status_array[$counter]['type'] = 'fa-question';
                $user_status_array[$counter]['text'] = 'Du har ställt en fråga';
                $user_status_array[$counter]['title'] = $q->title;
                $user_status_array[$counter]['url_key'] = '/question/'.$q->url_key;
                $user_status_array[$counter]['created'] = $q->created;
                $counter++;
            }
        }
        
        $answers = DB::table('posts')->select(array('title', 'parentid','created'))->where('type', '=', 'A')->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($answers) > 0){
            foreach($answers as $a){
                $q = DB::table('posts')->select(array('title', 'url_key','title', 'created'))->where('postid', '=', $a->parentid)->first();
                if(count($q) > 0){
                    $user_status_array[$counter]['type'] = 'fa-comments';
                    $user_status_array[$counter]['text'] = 'Du har svarat på en fråga';
                    $user_status_array[$counter]['title'] = $q->title;
                    $user_status_array[$counter]['url_key'] = '/question/'.$q->url_key;
                    $user_status_array[$counter]['created'] = $a->created;
                    $counter++;
                }
            }
        } 
        $comments = DB::table('posts')->select(array('title', 'parentid','created'))->where('type', '=', 'C')->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($comments) > 0){
            foreach($comments as $c){
                $res = DB::table('posts')->select(array('url_key','title', 'parentid', 'type'))->where('postid', '=', $c->parentid)->first();
                if(count($res) > 0){
                    $user_status_array[$counter]['type'] = 'fa-comments';
                    if($res->type == 'A'){
                        $user_status_array[$counter]['text'] = 'Du har kommenterat ett svar på en fråga';
                        $q = DB::table('posts')->select(array('title', 'url_key','title'))->where('postid', '=', $res->parentid)->first();
                        if(count($q) > 0){
                            $user_status_array[$counter]['title'] = $q->title;
                            $user_status_array[$counter]['url_key'] = '/question/'.$q->url_key;
                        }
                    }else{
                        $user_status_array[$counter]['text'] = 'Du besvarade en fråga.';
                        $user_status_array[$counter]['title'] = $res->title;
                        $user_status_array[$counter]['url_key'] = '/question/'.$res->url_key;
                    }
                    $user_status_array[$counter]['created'] = $c->created;
                    $counter++;
                }
            }
        }
        $uservotes = DB::table('uservotes')->select(array('postid','flag','created'))->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($uservotes) > 0){
            foreach($uservotes as $uv){
                $res = DB::table('posts')->select(array('url_key','title', 'parentid', 'type'))->where('postid', '=', $uv->postid)->first();
                if(count($res) > 0){
                $user_status_array[$counter]['type'] = 'fa-thumbs-up';
                if ($uv->flag == 0) {
                    $user_status_array[$counter]['type'] = 'fa-thumbs-down';
                }

                if ($res->type == 'A') {
                    if ($uv->flag == 0) {
                        $user_status_array[$counter]['text'] = 'Du har röstat ned ett svar på en fråga';
                    } else {
                        $user_status_array[$counter]['text'] = 'Du har röstat upp ett svar på en fråga';
                    }
                    $q = DB::table('posts')->select(array('url_key', 'title'))->where('postid', '=', $res->parentid)->first();
                    if (count($q) > 0) {
                        $user_status_array[$counter]['title'] = $q->title;
                        $user_status_array[$counter]['url_key'] = '/question/' . $q->url_key;
                    }
                } else {
                    if ($uv->flag == 0) {
                        $user_status_array[$counter]['text'] = 'Du har röstat ned en fråga';
                    } else {
                        $user_status_array[$counter]['text'] = 'Du har röstat upp en fråga';
                    }
                    $user_status_array[$counter]['title'] = $res->title;
                    $user_status_array[$counter]['url_key'] = '/question/' . $res->url_key;
                }


                $user_status_array[$counter]['created'] = $uv->created;
                $counter++;
            }
            }
        }
        if(count($user_status_array) > 0){
       
            function date_compare2($a, $b) {
                $t1 = strtotime($a['created']);
                $t2 = strtotime($b['created']);
                return $t2 - $t1;
            }
            usort($user_status_array, 'date_compare2');
        ?>
     <div class="widget widget-recent-activity">

        <ul>
            <?php 
                foreach($user_status_array as $usa){  
                if(isset($usa['url_key']) and isset($usa['title'])){    
            ?>
                    <li>
                        <i class="fa <?php echo $usa['type'] ?>"></i>
                        <span>
                            <?php echo $usa['text']; ?> 
                            <a href="<?php echo $usa['url_key']; ?>"><?php echo $usa['title']; ?></a>.
                            <br>
                            <span class="time-activity">
                                <?php echo Question::formattedCreatedDate($usa['created']); ?>
                            </span>
                        </span>
                    </li>
            <?php } } ?>
            
        </ul>

    </div>
    <?php }} ?>


                <div class="copyright">
                    &copy;<?php echo date('Y'); ?> SVARPAALLT <br>
                    <a href="http://blog.merinfo.se/kop-och-avtalsvillkor" target="_blank">Villkor</a>
                </div>
            </div><!-- END LEFT-SIDEBAR -->
        </div>
    </div>		
</nav>
<?php
$total_question = DB::table('posts')->where('type', '=', 'Q')->count();
$total_user = DB::table('users')->count();
?>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 right-sidebar">
                <div class="widget widget-statistic">
                    <p class="questions-count">
                        Questions<br>
                        <span><?php echo $total_question; ?></span>
                    </p>
                    <p class="members-count">
                        Medlemmar<br>
                        <span><?php echo $total_user; ?></span>
                    </p>
                </div><!-- END widget-statistic -->
                <?php
        $top_users = DB::table('users')->orderBy('points', 'desc')->take(5)->get();
        if (count($top_users) > 0) {
    ?>
            <div class="widget user-widget">
                <h3 class="widgettitle">BÄSTA ANVÄNDARE (Poäng)</h3>
                <div class="hot-user-question">
                    <ul>
                       <?php $counter = 1; ?>  
                       <?php foreach($top_users as $u){ ?> 
                        <li>
                            <span class="number"><?php echo $counter; ?></span>
                            <span class="username ">
                                <a title="<?php echo $u->username; ?>" href="/user/<?php echo $u->url_key; ?>" ><?php echo $u->username; ?></a>
                            </span>
                            <span title="<?php echo $u->points; ?> Poäng" class="points-count">
                                <i class="fa fa-star"></i>
                                <span><?php echo $u->points; ?></span>
                            </span>
                        </li>
                       <?php $counter++; } ?>
                    </ul>
                </div>
            </div>
    <?php } ?>
    <?php
        $top_questions = DB::select( DB::raw("SELECT qa_users.userid, qa_users.url_key as user_url_key, qa_users.username, count(*) as total FROM qa_users, qa_posts where qa_users.userid = qa_posts.userid and qa_posts.type = 'Q' group by qa_posts.userid order by total DESC ") );
        if (count($top_questions) > 0) {
    ?>
    <div class="widget user-widget">
        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Frågor)</h3>
        <div class="hot-user-question">
            <ul>
                <?php $counter = 1; ?>
                <?php foreach($top_questions as $q){ ?> 
                <li>
                    <span class="number"><?php echo $counter; ?></span>
                    <span class="username ">
                        <a title="<?php echo $q->username; ?>" href="/user/<?php echo $q->user_url_key; ?>"><?php echo $q->username; ?></a>
                    </span>
                    <span title="<?php echo $q->total; ?> Frågor" class="questions-count">
                        <i class="fa fa-question-circle"></i>
                        <span><?php echo $q->total; ?></span>
                    </span>
                </li>
                <?php $counter++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <?php
        $top_answers = DB::select( DB::raw("SELECT qa_users.userid, qa_users.url_key as user_url_key, qa_users.username, count(*) as total FROM qa_users, qa_posts where qa_users.userid = qa_posts.userid and qa_posts.type = 'A' group by qa_posts.userid order by total DESC ") );
        if (count($top_answers) > 0) {
    ?>
    <div class="widget user-widget">
        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Svar)</h3>
        <div class="hot-user-question">
            <ul>
                <?php $counter = 1; ?>
                <?php foreach($top_answers as $a){ ?> 
                <li>
                    <span class="number"><?php echo $counter; ?></span>
                    <span class="username ">
                        <a title="<?php echo $a->username; ?>" href="/user/<?php echo $a->user_url_key; ?>"><?php echo $a->username; ?></a>
                    </span>
                    <span title="<?php echo $a->total; ?> Svar" class="answers-count">
                        <i class="fa fa-comments"></i>
                        <span><?php echo $a->total; ?></span>
                    </span>
                </li>
                <?php $counter++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <?php
        $latest_users = DB::table('users')->orderBy('userid', 'desc')->take(5)->get();
        if (count($latest_users) > 0) {
    ?>
    <div class="widget user-widget">
        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Senaste)</h3>
        <div class="hot-user-question">
            <ul>
                <?php $counter = 1; ?>
                <?php foreach($latest_users as $u){ ?> 
                <li>
                    <span class="number"><?php echo $counter; ?></span>
                    <span class="username latest">
                        <a title="<?php echo $u->username; ?>" href="/user/<?php echo $u->url_key; ?>"><?php echo $u->username; ?></a>
                    </span>
                </li>
                <?php $counter++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
                

            </div><!-- END RIGHT-SIDEBAR -->

        </div>
    </div>
</nav>
<style>
a.abt{
	text-align:center !important;
	color:#FFF !important;
	padding-top:13px !important;
}
</style>