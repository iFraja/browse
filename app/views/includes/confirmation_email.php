<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>{{$user['full_name']}},</p>
        <p>
            Thank you for registering for Svarpåallt Q&A. <br>
            Please click below to confirm your email address.
        </p>
        <p><a href="{{URL::to('user/confirm')}}/{{$user['token']}}" target="_blank" >Bekräfta e-postadress</a></p>
        <p>Your login details are as follows:</p>
        <p>
            <b>User name:</b> {{$user['full_name']}}
            <b>Email Address:</b> {{$user['email_address']}}
        </p>
        <p>Please keep this information safe for future reference.</p>
        <p>&nbsp;</p>
        <p>Thank you, <br> Svarpåallt Q&A <br><a href="{{URL::to('/')}}" target="_blank" >Svarpåallt Q&A</a></p>
    </body>
</html>
