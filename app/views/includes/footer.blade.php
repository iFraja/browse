<script>



    if( navigator.userAgent.match(/iPhone|iPad|iPod/i) ) {

        $('#login_register').on('show.bs.modal', function() {

            // Position modal absolute and bump it down to the scrollPosition
            $(this)
                    .css({
                        position: 'fixed',
                        marginTop: $(window).scrollTop() + 'px',
                        bottom: 'auto'
                    });

            // Position backdrop absolute and make it span the entire page
            //
            // Also dirty, but we need to tap into the backdrop after Boostrap
            // positions it but before transitions finish.
            //
            setTimeout( function() {
                $('.modal-backdrop').css({
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                    ) + 'px'
                });
            }, 0);
        });
    }




</script>

@if(isset($data['question_detail_page']) and $data['question_detail_page'] === TRUE)
@else
<script type="text/javascript">
   currentQuestion = {}


 /*  jQuery(".modal-dialog").css({
       'margin-top': '50px',
       'margin-left': '50px'})
*/
</script>
@endif
<div style="display:none;">
		<div id="wp-temp_id-wrap" class="wp-core-ui wp-editor-wrap tmce-active"><link rel='stylesheet' id='dashicons-css'  href='/qaengine/qa-includes/css/dashicons.min.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='editor-buttons-css'  href='/qaengine/qa-includes/css/editor.min.css?ver=4.1' type='text/css' media='all' />
<div id="wp-temp_id-editor-container" class="wp-editor-container"><textarea class="wp-editor-area" rows="20" tabindex="5" autocomplete="off" cols="40" name="post_content" id="temp_id"></textarea></div>
</div>
</div>
@if(Auth::check() )
@if(Auth::user()->verified_account == 0)
<a href="javascript:void(0);" class="confirm_email_link" style="display: none;" >confirm_email_link</a>
<!-- Bekräfta e-postadress -->
<div class="modal fade reportFormModal" id="confirmUserEmail" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="close_confirm_window();"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">Bekräfta e-postadress</h4>
            </div>
            <div class="modal-body text-center">
                <label>Vänligen bekräfta ditt medlemskap genom den länk som mailats till er.</label>
            </div>
        </div>
    </div>
</div>	
<!-- Bekräfta e-postadress -->
@endif

<!-- MODAL UPLOAD IMAGE -->
<div class="modal fade modal-submit-questions" id="upload_images" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title modal-title-sign-in" id="myModalLabel">Lägg till bild</h4>
            </div>
            <div class="modal-body">
                <div class="upload-location" id="images_upload_container">
                    <span class="title">Ladda upp bild</span>
                    <div class="input-file">

                        <input type="button" value="Browse" class="bg-button-file button" id="images_upload_browse_button">                        
                        <span class="filename">Ingen fil vald</span>
                        <span class="et_ajaxnonce" id="994bc30981"></span> 
                    </div>
                </div> 
                <div class="upload-url">
                    <span class="title" style="display: none">Lägg till bild via en URL</span>
                    <div class="input-url" >
                        <input type="text" placeholder="https://www.domain.com/images.jpg" id="external_link" class="form-control" style="display: none">
                        <div class="button-event">
                            <button type="button" id="insert" class="btn">Infoga</button>
                            <a href="javascript:void(0)" class="btn-cancel collapse" data-dismiss="modal">
                                Avbryt	                    	</a>
                        </div>
                    </div>                  
                </div>					 
            </div>
        </div>
    </div>
</div>	
<!-- MODAL UPLOAD IMAGE -->

<!-- MODAL REPORT -->
<div class="modal fade reportFormModal" id="reportFormModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </button>
                <h4 class="modal-title">Rapportera </h4>
            </div>
            <div class="modal-body">
                <form id="report_form" class="form_modal_style">
                    <label>Ditt meddelande</label>
                    <textarea id="txt_report" rows="5" placeholder="Har du något att säga? Skriv ditt meddelande här." name= "message"></textarea>
                    <input type="submit" data-loading-text="Laddar…" class="btn" value ="Skicka" />
                </form>
            </div>
        </div>
    </div>
</div>	
<!-- END MODAL REPORT -->
<!-- CONTACT REPORT -->
<div class="modal fade reportFormModal" id="contactFormModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </button>
                <h4 class="modal-title">
                    Kontakt 				</h4>
            </div>
            <div class="modal-body">                
                <form id="contact_form" class="form_modal_style">
                    <input type="hidden" id="user_id" value="{{Request::segment(2)}}" />
                    <label>Ditt meddelande:</label>
                    <textarea id="txt_msg" class="submit-textarea" name="txt_msg" rows="5" placeholder="Har du något att säga? Skriv ditt meddelande här." name= "message"></textarea>
                    <input type="submit" data-loading-text="Laddar…" class="btn" value ="Skicka" />
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END CONTACT REPORT -->
<!-- MODAL INSERT NEW QUESTION -->
<!-- MODAL SUBMIT QUESTIONS -->
<div class="modal fade modal-submit-questions" id="modal_submit_questions" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Ställ en fråga</h4>
            </div>
            <div class="modal-body">
                <form id="submit_question">
                    <input type="hidden" id="qa_nonce" name="qa_nonce" value="28987ef17d">
                    <input type="text" class="submit-input" id="question_title" name="post_title" placeholder="Din fråga" />
                    <div class="select-categories">
                        <?php
                            $categories = DB::table('categories')->where('display_ask_question', 'y')->orderBy('title', 'ASC')->get();
                        ?>
                        <select id="question_category" name="question_category" class="categories-select">
                            <option value="">Välj kategori</option>
                            <?php foreach($categories as $cate){ ?>
                                <option value="<?php echo $cate->categoryid; ?>"><?php echo $cate->title; ?></option>
                            <?php } ?>
                        </select>    
                    </div>
                    <div class="wp-editor-container" style="display: none;">
                        <textarea name="post_content" id="insert_question"></textarea>
                    </div>
                        
                    <div id="question-tags-container">
                        <input data-provide="typeahead" type="text" class="submit-input tags-input" id="question_tags" name="question_tags" placeholder="Tagg (max 5 taggar)" />
                        <span class="tip-add-tag">Tryck retur för att lägga till ny tagg</span>
                        <ul class="tags-list" id="tag_list"></ul>
                    </div>
                    
                    <div style="display: none;">
                        <input type="checkbox" name="qemail_me" value="y" id="qemail_me"  /> <label for="qemail_me" class="checkbox-email">  Meddela mig via e-post om min fråga besvaras.
</label>
                        <div class="clearfix"></div>
                    </div>
                    
                    
                    <br >
                    <input type="text" class="submit-input" id="name" name="name" placeholder="Your name to display (optional)" style="display: none" />
                    <input type="text" class="submit-input" id="email" name="email" placeholder=" Email me at this address if my question is answered or commented (optional)" style="display: none" />

                    <input id="add_tag_text" type="hidden" value="You must have 1000 points to add tag. Current, you have to select existed tags." />

                    <button id="btn_submit_question" class="btn-submit-question">Skicka fråga</button>
                    <p class="term-texts">
                        Genom att posta din <a target="_blank" href="http://blog.merinfo.se/kop-och-avtalsvillkor">fråga godkänner</a> du <a target="_blank" href="http://blog.merinfo.se/kop-och-avtalsvillkor">våra villkor.</a>					</p>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL SUBMIT QUESTIONS -->		
<!-- MODAL INSERT NEW QUESTION -->
<!-- MODAL EDIT PROFILE / CHANGE PASS -->
<div class="modal fade modal-submit-questions" id="edit_profile" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title" id="myModalLabel">Redigera profil</h4>
                    </div>
                    <div class="modal-body">
                        <div class="author-edit" id="user_avatar_container">
                            <span class="author-avatar image" id="user_avatar_thumbnail">
                                <img src="{{Auth::user()->user_picture}}" class="avatar" alt="" />            </span>
                            <div class="edit-info-avatar">
                                <a href="javascript:void(0);" class="upload-avatar-btn" id="user_avatar_browse_button" >
                                    Ladda upp bild                </a>
                                <a href="javascript:void(0);" class="link_change_password">Ändra lösenord</a>
                                <a href="javascript:void(0);" class="link_change_profile">Redigera profil</a>
                            </div>
                            <span class="et_ajaxnonce" id="ad9571c0b7"></span>
                        </div>

                        <form id="submit_edit_profile" class="form_modal_style edit_profile_form">
                            <label>Fullständigt namn</label>
                            <input type="text" class="submit-input" maxlength="40" id="full_name" name="full_name" value="{{Auth::user()->handle}}">

                            <label>Geografisk plats</label>
                            <input type="text" class="submit-input" maxlength="40" id="location" name="location" value="{{Auth::user()->location}}">
                             
                          <!--  <label>Webbsida</label>
                            <input type="text" class="submit-input" maxlength="40" id="website" name="website" value="{{Auth::user()->website}}">-->
                            
                            <input type="checkbox" name="email_me" value="y" id="email_me" @if(Auth::user()->email_me == 'y') checked=checked @endif  /> <label for="email_me" class="checkbox-email"> Tillåt användare att e-posta dig
</label>
                            <div class="clearfix"></div>
                            <input type="checkbox" name="post_me" value="y" id="post_me" @if(Auth::user()->post_me == 'y') checked=checked @endif /> <label for="post_me" class="checkbox-email"> Tillåt användare att se din e-postadress
</label>
                            
                            <div class="clearfix"></div>

                            <label>Om</label>
                            <textarea  maxlength="350" class="submit-textarea" id="description" name="description">{{Auth::user()->description}}</textarea>

                            <div class="clearfix"></div>

                            <input type="submit" name="submit" value="Uppdatera Profil" class="btn-submit update_profile">
                        </form>

                        <form id="submit_edit_password" class="form_modal_style edit_password_form">
                            <label>Gammalt Lösenord</label>
                            <input type="password" class="submit-input" id="current_password" name="current_password">
                            <label>Nytt Lösenord</label>
                            <input type="password" class="submit-input" id="password" name="password">
                            <label>Repetera nytt Lösenord
</label>
                            <input type="password" class="submit-input" id="password_confirmation" name="password_confirmation">
                            <input type="submit" name="submit" value="Ändra lösenord" class="btn-submit update_profile">
                        </form>
                    </div>
                </div>
            </div>
        </div>
<!-- MODAL EDIT PROFILE / CHANGE PASS -->
@else

<!-- MODAL RESET PASSWORD -->
        <div class="modal fade modal-submit-questions" id="reset_password" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title modal-title-sign-in" id="myModalLabel">Återställ lösenord</h4>
                    </div>
                    <div class="modal-body">

                        <form id="resetpass_form" class="form_modal_style">
                            <label>Skriv ditt nya lösenord här</label>
                            <input type="password" class="name_user" name="new_password" id="new_password" />
                            <input type="password" class="name_user" name="re_new_password" id="re_new_password" />
                            <input type="submit" name="submit" value="Återställ" class="btn-submit">
                            <?php if(isset($data['user_information_reset']['user_key']) and $data['user_information_reset']['user_key']!="" ){ ?>
                                <input type="hidden" id="user_login" name="user_login" value="<?php echo $data['user_information_reset']['user_login']; ?>" />
                            <input type="hidden" id="user_key" name="user_key" value="<?php echo $data['user_information_reset']['user_key']; ?>">            	
                            <?php } ?>
                        </form>	 

                    </div>
                </div>
            </div>
        </div>	
<!-- MODAL RESET PASSWORD -->

<!-- MODAL LOGIN / REGISTER -->
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" id="login_register2" class="modal fade modal-submit-questions in" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title modal-title-sign-in" id="myModalLabel">Logga in</h4>
            </div>
            <div class="modal-body">
                <p id="signin_error_message" class="text-center" style="font-size: 16px;color: red;font-weight: bold;display: none"></p>
                <form id="signin_form" class="form_modal_style">
                    <label for="username">E-postadress</label>
                    <input type="text" class="email_user"  name="email" id="email"   />
                    <label for="password">Lösenord</label>
                    <input type="password" class="password_user" id="password" name="password">
                    <a href="javascript:void(0)" class="link_forgot_pass">Glömt lösenord?</a>
                    <div class="clearfix"></div>
                    <input type="submit" name="submit" value="Logga in" class="btn-submit">
                    <a href="javascript:void(0)" class="link_sign_up">Bli medlem</a>
                </form>

                <form id="signup_form" class="form_modal_style">
                    <label for="username">Ditt namn</label>
                    <input type="text" class="name_user" name="username" id="username" />
                    <label for="email">E-postadress</label>
                    <input type="text" class="email_user" name="email" id="email" />
                    <label for="password1">Lösenord</label>
                    <input type="password" class="password_user_signup" id="password1" name="password" />
                    <label for="re_password">Repetera lösenord</label>
                    <input type="password" class="repeat_password_user_signup" id="re_password" name="re_password" />
                    <div class="clearfix"></div>
                    <input type="submit" name="submit" value="Bli medlem" class="btn-submit">
                    <a href="javascript:void(0)" class="link_sign_in">Logga in</a>

                    <div class="clearfix"></div>

                    <p class="policy-sign-up">
                        Genom att klicka ”Bli medlem” godkänner du att du läst och <a target="_blank" href="/privacy-policy">accepterat</a> våra <a target="_blank" href="/terms-of-service">villkor.</a>	            </p>
                </form>

                <form id="forgotpass_form" class="form_modal_style collapse">
                    <label>Ange din e-postadress här</label>
                    <input type="text" class="name_user" name="email" id="email" />
                    <input type="submit" name="submit" value="Skicka" class="btn-submit">
                </form>

            </div>
        </div>
    </div>
</div>	<!-- MODAL LOGIN / REGISTER -->
@endif

<!-- TAG TEMPLATE -->
<script type="text/template" id="tag_item">
    <input type="hidden" name="tags[]" value="{<?php echo '{= stripHTML(name) }'; ?>}" />
    {<?php echo '{= stripHTML(name) }'; ?>} <a href="javascript:void(0)" class="delete"><i class="fa fa-times"></i></a>
</script>

<script type="text/javascript">
        function stripHTML(html)
        {
           var tmp = document.createElement("DIV");
           tmp.innerHTML = html;
           return tmp.textContent||tmp.innerText;
        }
</script>
<!-- TAG TEMPLATE -->

<script type="text/template" id="answer_item">
	
<div class="col-md-2 col-xs-2 vote-block">
    <ul>
        <li>
            <a href="javascript:void(0)" data-name="vote_up" class="action vote vote-up img-circle">
                    <i class="fa fa-chevron-up"></i>
            </a>
        </li>
        <li>
            <span class="vote-count">{<?php echo '{= et_vote_count }'; ?>}</span>
        </li>
        <li>
            <a href="javascript:void(0)" data-name="vote_down" class="action vote vote-down img-circle">
                    <i class="fa fa-chevron-down"></i>
            </a>
        </li>
        <# if(currentUser.ID == parent_author) { #>
        <li>
            <a href="javascript:void(0)" data-name="accept-answer" class="action accept-answer img-circle">
                    <i class="fa fa-check"></i>
            </a>
        </li>
        <# } #>
    </ul>
</div>
<div class="col-md-9 col-xs-9 q-right-content">
<ul class="post-controls" >
    <li>
        <a href="javascript:void(0)" data-name="edit" class="post-edit action">
            <i class="fa fa-pencil"></i>
        </a>
    </li>
    <li>
        <a href="javascript:void(0)" data-name="delete" class="post-delete action" >
            <i class="fa fa-trash-o"></i>
        </a>
    </li>
    <li style="display:none;">
        <a href="javascript:void(0)" data-name="report" class="report action" >
            <i class="fa fa-exclamation-triangle"></i>
        </a>
    </li>
</ul>		    
    <div class="clearfix"></div>
    <div class="question-content">
        {<?php echo '{= content_filter }'; ?>}
    </div>
    <div class="post-content-edit collapse">
        <form class="edit-post">
            <input type="hidden" name="qa_nonce" value="99417a6040" />
            <div class="wp-editor-container">
                <!--<textarea name="post_content" id="edit_post_{<?php echo '{= ID }'; ?>}"></textarea>-->
            </div>
            <div class="row submit-wrapper">
                <div class="col-md-2">
                    <button id="submit_reply" class="btn-submit">Uppdatera</button>
                </div>
                <div class="col-md-2">
                    <a href="javascript:void(0)" data-name="cancel-post-edit" class="action cancel-edit-post">Avbryt</a>
                </div>                                        
            </div>                                    
        </form>
    </div><!-- END EDIT POST FORM --> 		        
    <div class="row cat-infomation">
        <div class="col-md-8 col-xs-8 question-cat">
            <span class="author-avatar">
                {<?php echo '{= avatar }'; ?>}        
            </span>
            <span class="author-name">{<?php echo '{= author_name }'; ?>}</span>
            {<?php echo '{= user_badge }'; ?>}
            <span class="question-time">
                Answered {<?php echo '{= human_date }'; ?>}
            </span>
        </div>
        <div class="col-md-4 col-xs-4 question-control">
            <ul>
                <li>
                                            <a class="share-social" href="javascript:void(0)" data-toggle="popover" data-placement="top" data-container="body" data-content="<ul class=&quot;socials-share&quot;><li><a href=&quot;https://www.facebook.com/sharer/sharer.php?u={<?php echo '{= guid }'; ?>}&amp;t={<?php echo '{= post_title }'; ?>}&quot; target=&quot;_blank&quot; class=&quot;btn-fb&quot;><i class=&quot;fa fa-facebook&quot;></i></a></li><li><a target=&quot;_blank&quot; href=&quot;http://twitter.com/share?text={<?php echo '{= post_title }'; ?>}&amp;url={<?php echo '{= guid }'; ?>}&quot; class=&quot;btn-tw&quot;><i class=&quot;fa fa-twitter&quot;></i></a></li><li class=&quot;ggplus&quot;><a target=&quot;_blank&quot;  href=&quot;https://plus.google.com/share?url={<?php echo '{= guid }'; ?>}&quot; class=&quot;btn-gg&quot;><i class=&quot;fa fa-google-plus&quot;></i></a></li></ul>" data-html="true">
                            Share <i class="fa fa-share"></i>
                        </a>	                        
                </li>
                <!--<li>
                    <a href="javascript:void(0)">
                        Rapportera <i class="fa fa-flag"></i>
                    </a>
                </li>-->
                <li>
                    <a href="#container_{<?php echo '{= ID }'; ?>}" class="show-comments">
                        {<?php echo '{= comments }'; ?>} <i class="fa fa-comment"></i>
                    </a>
                </li>
            </ul>
        </div>                   
    </div>
    <div class="clearfix"></div>
    <div class="comments-container collapse" id="container_{<?php echo '{= ID }'; ?>}">
            <div class="comments-wrapper"></div>
        <a class="add-comment" data-id="{<?php echo '{= ID }'; ?>}" href="javascript:void(0)">Lägg till kommentar</a>
        <div class="clearfix"></div>
        <form class="child-reply" method="POST">
            <input type="hidden" name="qa_nonce" value="abe8eb7947" />
            <input type="hidden" name="comment_post_ID" value="{<?php echo '{= ID }'; ?>}" />  
            <input type="hidden" name="comment_type"    value="answer" />            
            <input type="hidden" name="user_id" value="{<?php echo '{= post_author }'; ?>}" />              
                <div id="editor_wrap_{<?php echo '{= ID }'; ?>}" class="child-answer-wrap collapse">
                    <div class="wp-editor-container">
                            <!--<textarea name="post_content" id="insert_answer_{<?php echo '{= ID }'; ?>}"></textarea>-->
                    </div>
                            <div class="row submit-wrapper">
                        <div class="col-md-3 col-xs-3">
                            <button id="submit_reply" class="btn-submit">
                                Lägg till kommentar		                        </button>
                        </div>
                            <div class="col-md-9 col-xs-9">
                                <a href="javascript:void(0)" class="hide-comment">Avbryt</a>
                            </div>
                    </div>
                </div>
        </form>            
    </div>
</div>
	
</script>

<script type="text/template" id="comment_item">

    <div class="col-md-2 comment-avatar">
        {<?php echo '{= avatar}'; ?>}
        <p class="cmt-author">
            <a href="{<?php echo '{= author_url}'; ?>}" title="{<?php echo '{= author}'; ?>}">
                {<?php echo '{= author}'; ?>}
            </a>
        </p>
    </div>
    <div class="col-md-10 comment-content">
        <div class="cm-content-wrap">
            <div class="cm-wrap">{<?php echo '{= content_filter}'; ?>}</div>
            <span class="comment-time">{<?php echo '{= human_date}'; ?>}</span> 
                    <span class="comment-edit" >
                       <a class="edit-comment" href="javascript:void(0)">Redigera <i class="fa fa-pencil"></i></a>
                        @if(Auth::check() && Auth::user()->userid == 1)
                            <a class="delete-comment" href="javascript:void(0)">Radera <i class="fa fa-times"></i></a>
                        @endif
                    </span>
        </div><!-- END COMMENT CONTENT -->
        <div class="cm-content-edit collapse">
            <form class="edit-comment">
                <input type="hidden" name="qa_nonce" value="323e6c41a0" />
                <div class="wp-editor-container">
                    <!--<textarea name="comment_content" id="edit_comment_{<?php echo '{= comment_ID}'; ?>}"></textarea>-->
                </div>
                <div class="row submit-wrapper">
                    <div class="col-md-3">
                        <button id="submit_reply" class="btn-submit">Uppdatera</button>
                    </div>
                    <div class="col-md-3">
                        <a href="javascript:void(0)" class="cancel-comment">Avbryt</a>
                    </div>
                </div>
            </form>
        </div><!-- END EDIT COMMENT FORM -->
    </div>

</script>

<?php //<script type='text/javascript' src='http://demo.enginethemes.com/qaengine/wp-content/plugins/adrotate/library/jquery.adrotate.clicktracker.js?ver=0.7'></script> ?>
<script type='text/javascript' src='/qaengine/qa-includes/js/underscore.min.js?ver=1.6.0'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/backbone.min.js?ver=1.1.2'></script>
<script type='text/javascript' src='/qaengine/includes/aecore/assets/js/appengine.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/functions.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/bootstrap.min.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/modernizr.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/jquery.simple-text-rotator.min.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/jquery.validate.min.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.10.4'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.10.4'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/jquery.ui.position.min.js?ver=1.10.4'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/jquery.ui.menu.min.js?ver=1.10.4'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/jquery/ui/jquery.ui.autocomplete.min.js?ver=1.10.4'></script>
<script type='text/javascript' src='/qaengine/js/libs/waypoints.min.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/waypoints-sticky.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/chosen.jquery.min.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/classie.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/scripts.js?ver=4.0.1'></script>

<script type='text/javascript'>
/* <![CDATA[ */
var qa_front = {"form_auth":{"error_msg":"Fyll i alla obligatoriska fält.","error_user":"Please enter your user name.","error_email":"Vänligen ange en giltig e-postadress.","error_username":"Please enter a valid username.","error_repass":"Ditt lösenord matchar inte, vänligen försök på nytt.","error_url":"Please enter a valid URL.","error_cb":"You must accept the term & privacy."},"texts":{"require_login":"Du måste vara inloggad för att utföra denna åtgärd.","enought_points":"You don't have enought points to perform this action.","create_topic":"Create Topic","upload_images":"Ladda upp bild","insert_codes":"Insert Code","no_file_choose":"Ingen fil vald.","require_tags":"Please insert at least one tag.","add_comment":"Lägg till kommentar","cancel":"Cancel","sign_up":"Bli medlem","sign_in":"Logga in","accept_txt":"Accept","best_ans_txt":"Best answer","forgotpass":"Glömt lösenord?","close_tab":"You have made some changes which you might want to save.","confirm_account":"You must activate your account first to create questions \/ answers!.","cancel_auth":"User cancelled login or did not fully authorize.","banned_account":"You account has been banned, you can't make this action!"}};
/* ]]> */
</script>

<script type='text/javascript' src='/qaengine/js/front.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/profile.js?ver=4.0.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/syntaxhighlighter/shCore.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/syntaxhighlighter/shBrushJScript.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/syntaxhighlighter/shBrushPhp.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/libs/syntaxhighlighter/shBrushCss.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/js/single-question.js?ver=4.1'></script>


<script type='text/javascript' src='/qaengine/qa-includes/js/utils.min.js?ver=4.1'></script>
<script type='text/javascript' src='/qaengine/qa-admin/js/editor.min.js?ver=4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpLinkL10n = {"title":"Insert\/edit link","update":"Update","save":"Add Link","noTitle":"(no title)","noMatchesFound":"Inga resultat hittades."};
/* ]]> */
</script>

<script type='text/javascript' src='/qaengine/qa-includes/js/wplink.min.js?ver=4.1'></script>
<script type="text/javascript">
                tinyMCEPreInit = {
                        baseURL: "/qaengine/qa-includes/js/tinymce",
                        suffix: ".min",
                                                mceInit: {'post_content':{theme:"modern",skin:"lightgray",language:"en",formats:{
                                                alignleft: [
                                                        {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: {textAlign:'left'}},
                                                        {selector: 'img,table,dl.wp-caption', classes: 'alignleft'}
                                                ],
                                                aligncenter: [
                                                        {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: {textAlign:'center'}},
                                                        {selector: 'img,table,dl.wp-caption', classes: 'aligncenter'}
                                                ],
                                                alignright: [
                                                        {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: {textAlign:'right'}},
                                                        {selector: 'img,table,dl.wp-caption', classes: 'alignright'}
                                                ],
                                                strikethrough: {inline: 'del'}
                                        },block_formats:"Paragraph=p;Pre=pre;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6",relative_urls:false,remove_script_host:false,convert_urls:false,browser_spellcheck:true,fix_list_elements:true,entities:"38,amp,60,lt,62,gt",entity_encoding:"raw",keep_styles:false,cache_suffix:"wp-mce-4107-20141130",preview_styles:"font-family font-size font-weight font-style text-decoration text-transform",wpeditimage_disable_captions:false,wpeditimage_html5_captions:false,plugins:"charmap,colorpicker,hr,lists,media,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpgallery,wplink,wpdialogs,wpview,image",external_plugins:{"qaimage":"/qaengine\/js\/plugins\/feimage\/editor_plugin_src.js","qacode":"/qaengine\/js\/plugins\/fecode\/editor_plugin.js","autoresize":"/qaengine\/js\/plugins\/autoresize\/editor_plugin.js","autolink":"/qaengine\/js\/plugins\/autolink\/plugin.min.js"},content_css:"/qaengine/css/editor_content.css",selector:"#post_content",resize:"vertical",menubar:false,wpautop:true,indent:false,toolbar1:"bold,|,italic,|,underline,|,link,unlink,|,bullist,numlist,qaimage,qacode",toolbar2:"",toolbar3:"",toolbar4:"",tabfocus_elements:":prev,:next",body_class:"post_content post-type-question post-status-publish locale-en-us",height:"350",autoresize_min_height:"350",force_p_newlines:false,statusbar:false,force_br_newlines:false,forced_root_block:"",setup:function(ed) {
                        ed.on("keyup", function(e) {
                                if ( typeof hasChange == "undefined" ) {
                                        hasChange = true;
                                }

                                var content = ed.getContent(),
                                textarea    = jQuery("#insert_question"),
                                container   = textarea.parent();
                                label       = container.find("label.error");

                                if(content){
                                        label.hide();
                                        textarea.val(content).removeClass("error").addClass("valid");
                                } else {
                                        label.show();
                                        textarea.removeClass("valid").addClass("error");
                                }
                        });
                        ed.on("focus", function(e) {
                                if(currentUser.ID == 0)
                                        QAEngine.App.openAuthModal(e);
                        });
                        ed.onPaste.add(function(ed, e) {
                                if ( typeof hasChange == "undefined" ) {
                                        hasChange = true;
                                }
                        });
           }},'temp_id':{selector:"#temp_id",resize:"vertical",menubar:false,wpautop:true,indent:false,toolbar1:"bold,|,italic,|,underline,|,link,unlink,|,bullist,numlist,qaimage,qacode",toolbar2:"",toolbar3:"",toolbar4:"",tabfocus_elements:":prev,:next",body_class:"temp_id post-type-question post-status-publish locale-en-us",content_css:"/qaengine/css/editor_content.css",height:"350",autoresize_min_height:"350",force_p_newlines:false,statusbar:false,force_br_newlines:false,forced_root_block:"",setup:function(ed) {
                        ed.on("keyup", function(e) {
                                if ( typeof hasChange == "undefined" ) {
                                        hasChange = true;
                                }

                                var content = ed.getContent(),
                                textarea    = jQuery("#insert_question"),
                                container   = textarea.parent();
                                label       = container.find("label.error");

                                if(content){
                                        label.hide();
                                        textarea.val(content).removeClass("error").addClass("valid");
                                } else {
                                        label.show();
                                        textarea.removeClass("valid").addClass("error");
                                }
                        });
                        ed.on("focus", function(e) {
                                if(currentUser.ID == 0)
                                        QAEngine.App.openAuthModal(e);
                        });
                        ed.onPaste.add(function(ed, e) {
                                if ( typeof hasChange == "undefined" ) {
                                        hasChange = true;
                                }
                        });
           }}},
                        qtInit: {},
                        ref: {plugins:"charmap,colorpicker,hr,lists,media,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpgallery,wplink,wpdialogs,wpview,image",theme:"modern",language:"en"},
                        load_ext: function(url,lang){var sl=tinymce.ScriptLoader;sl.markDone(url+'/langs/'+lang+'.js');sl.markDone(url+'/langs/'+lang+'_dlg.js');}
                };
</script>
                

<script type='text/javascript' src='/qaengine/qa-includes/js/tinymce/tinymce.min.js?ver=4107-20141130'></script>
<script type='text/javascript' src='/qaengine/qa-includes/js/tinymce/plugins/compat3x/plugin.min.js?ver=4107-20141130'></script>
<script type='text/javascript'>
tinymce.addI18n( 'en', {"Ok":"OK","Bullet list":"Bulleted list","Spellcheck":"Check Spelling","Row properties":"Table row properties","Cell properties":"Table cell properties","Paste row before":"Paste table row before","Paste row after":"Paste table row after","Cut row":"Cut table row","Copy row":"Copy table row","Merge cells":"Merge table cells","Split cell":"Split table cell","Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.":"Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.\n\nIf you\u2019re looking to paste rich content from Microsoft Word, try turning this option off. The editor will clean up text pasted from Word automatically.","Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help":"Rich Text Area. Press Alt-Shift-H for help","You have unsaved changes are you sure you want to navigate away?":"The changes you made will be lost if you navigate away from this page.","Your browser doesn't support direct access to the clipboard. Please use the Ctrl+X\/C\/V keyboard shortcuts instead.":"Your browser does not support direct access to the clipboard. Please use keyboard shortcuts or your browser\u2019s edit menu instead.","Edit ":"Edit"});
tinymce.ScriptLoader.markDone( '/qaengine/qa-includes/js/tinymce/langs/en.js' );
</script>
<script type='text/javascript' src='/qaengine/qa-includes/js/tinymce/langs/wp-langs-en.js?ver=4107-20141130'></script>
<script type="text/javascript">
		tinyMCEPreInit.load_ext("/qaengine/js/plugins/feimage", "en");
tinymce.PluginManager.load("qaimage", "/qaengine/js/plugins/feimage/editor_plugin_src.js");
tinyMCEPreInit.load_ext("/qaengine/js/plugins/fecode", "en");
tinymce.PluginManager.load("qacode", "/qaengine/js/plugins/fecode/editor_plugin.js");
tinyMCEPreInit.load_ext("/qaengine/js/plugins/autoresize", "en");
tinymce.PluginManager.load("autoresize", "/qaengine/js/plugins/autoresize/editor_plugin.js");
tinyMCEPreInit.load_ext("/qaengine/js/plugins/autolink", "en");
tinymce.PluginManager.load("autolink", "/qaengine/js/plugins/autolink/plugin.min.js");

var ajaxurl = "/admin-ajax";
		( function() {
			var init, edId, qtId, firstInit, wrapper;

			if ( typeof tinymce !== 'undefined' ) {
				for ( edId in tinyMCEPreInit.mceInit ) {
					if ( firstInit ) {
						init = tinyMCEPreInit.mceInit[edId] = tinymce.extend( {}, firstInit, tinyMCEPreInit.mceInit[edId] );
					} else {
						init = firstInit = tinyMCEPreInit.mceInit[edId];
					}

					wrapper = tinymce.DOM.select( '#wp-' + edId + '-wrap' )[0];

					if ( ( tinymce.DOM.hasClass( wrapper, 'tmce-active' ) || ! tinyMCEPreInit.qtInit.hasOwnProperty( edId ) ) &&
						! init.wp_skip_init ) {

						try {
							tinymce.init( init );

							if ( ! window.wpActiveEditor ) {
								window.wpActiveEditor = edId;
							}
						} catch(e){}
					}
				}
			}

			if ( typeof quicktags !== 'undefined' ) {
				for ( qtId in tinyMCEPreInit.qtInit ) {
					try {
						quicktags( tinyMCEPreInit.qtInit[qtId] );

						if ( ! window.wpActiveEditor ) {
							window.wpActiveEditor = qtId;
						}
					} catch(e){};
				}
			}

			if ( typeof jQuery !== 'undefined' ) {
				jQuery('.wp-editor-wrap').on( 'click.wp-editor', function() {
					if ( this.id ) {
						window.wpActiveEditor = this.id.slice( 3, -5 );
					}
				});
			} else {
				for ( qtId in tinyMCEPreInit.qtInit ) {
					document.getElementById( 'wp-' + qtId + '-wrap' ).onclick = function() {
						window.wpActiveEditor = this.id.slice( 3, -5 );
					}
				}
			}
		}());
		</script>
<script type="text/javascript">
            _.templateSettings = {
                evaluate: /\<\#(.+?)\#\>/g,
                interpolate: /\{\{=(.+?)\}\}/g,
                escape: /\{\{-(.+?)\}\}/g
            };
</script>

<script type="text/javascript" id="frontend_scripts">
    (function ($) {
            $(document).ready(function(){

                    if(typeof QAEngine.Views.Front != 'undefined') {
                            QAEngine.App = new QAEngine.Views.Front();
                    }

                    if(typeof QAEngine.Views.Intro != 'undefined') {
                            QAEngine.Intro = new QAEngine.Views.Intro();
                    }

                    if(typeof QAEngine.Views.UserProfile != 'undefined') {
                            QAEngine.UserProfile = new QAEngine.Views.UserProfile();
                    }

                    if(typeof QAEngine.Views.Single_Question != 'undefined') {
                            QAEngine.Single_Question = new QAEngine.Views.Single_Question();
                    }


                    /*======= Open Reset Password Form ======= */
                        <?php if(isset($data['user_information_reset']['user_key']) and $data['user_information_reset']['user_key']!="" ){ ?>
                            var resetPassModal = new QAEngine.Views.ResetPassModal({ el: $("#reset_password") });
                            resetPassModal.openModal();
                        <?php } ?>
                    /*======= Open Reset Password Form ======= */

                    /*======= Open Confirmation Message Modal ======= */

      });
    })(jQuery);
</script>
<script type="text/javascript" id="current_user">
@if(Auth::check())
    currentUser = {"ID":"{{Auth::user()->userid}}","user_login":"{{Auth::user()->username}}","user_nicename":"{{Auth::user()->username}}","user_email":"{{Auth::user()->email}}","user_url":"","user_registered":"{{Auth::user()->created}}","user_activation_key":"","user_status":"0","display_name":"{{Auth::user()->handle}}","et_avatar":"<img src=\"http:\/\/1.gravatar.com\/avatar\/b033185a7cebc82082f2cd177cc99c4f?s=64&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D64&amp;r=G\" class=\"avatar\" alt=\"tanveer\" \/>","qa_following_questions":"","user_facebook":"","user_twitter":"","user_gplus":"","user_location":"","register_status":"","key_confirm":"b033185a7cebc82082f2cd177cc99c4f","et_question_count":0,"et_answer_count":0,"description":"","qa_point":"1000","show_email":"","ban_expired":"January 1, 1970","is_ban":false,"id":"{{Auth::user()->userid}}","cap":{"vote_up":true,"add_comment":true,"vote_down":true,"create_tag":true,"edit_question":true,"edit_answer":true,"edit":true}}
@else
    currentUser = {"id":0,"ID":0}		
@endif
</script>

<script>
    function close_confirm_window(){
        jQuery('#confirmUserEmail').addClass('fade');
        jQuery('#confirmUserEmail').hide();
        jQuery('.modal-backdrop').hide();
    }
</script>    

@if(Auth::check() and Auth::user()->verified_account == 0)
<script type="text/javascript">
    (function ($) {
        $("a.confirm_email_link").click(function() {
            $('#confirmUserEmail').removeClass('fade');
            $('#confirmUserEmail').show();
            $('body').append('<div class="modal-backdrop fade in"></div>');
        });        
        $("a.confirm_email_link").click();
    })(jQuery);
/*
    jQuery(".modal-dialog").modal('show').css({
        'margin-top': function () { //vertical centering
            return (jQuery(this).height() / 2);
        },
        'margin-left': function () { //Horizontal centering
            return (jQuery(this).width() / 2);
        }
    });
 */
$('#post_content').css("cursor", "pointer");
$(document).ready(function() {
  $('#post_content').click(function() {
    $('#login_register2').modal();
  });
});
</script>
@endif

