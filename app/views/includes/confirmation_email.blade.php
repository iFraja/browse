<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Du behöver bekräfta din medlemsregistrering för svar på allt!</p>
        <p>Användarnamn: <a href="mailto:{{$user['email_address']}}" >{{$user['email_address']}}</a></p>
        <p>Tack för att du valt att bli medlem på svar på allt! Nu när du skapat ett konto behöver du även bekräfta att detta är en giltig e-postadress som ger dig tillgång till att:</p>
        <p>
        <ul>
            <li>Återskapa glömt lösenord</li>
            <li>Erhålla e-post när någon svarar på din fråga</li>
        </ul>
        </p>
        <p>Det är viktigt att du bekräftar ditt medlemskap för att undvika att bli utelåst från ditt konto, vilket leder till att du inte kan utnyttja till fullo de förmåner som kommer med ett medlemskap på svar på allt!</p>
        <p>
            Vänligen använd följande länk för att bekräfta ditt medlemskap:
        </p>
        <p><a href="{{URL::to('users/confirm')}}/{{$user['token']}}" target="_blank" >{{URL::to('users/confirm')}}/{{$user['token']}}</a></p>
        
        <p>Observera att det inte är möjligt att svara på detta mail.</p>
        <p>&nbsp;</p>
        <p>Med vänlig hälsning <br> Teamet på Svar på allt!</p>
    </body>
</html>
