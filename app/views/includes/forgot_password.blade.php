<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Hej!</h2>
        <p>En begäran om utskick för återställning av lösenord har genererats genom svar på allt!</p>
        <p>Dina inloggningsuppgifter är enligt följande:</p>
        <p>Användarnamn: <a href="mailto:{{$user['email']}}" >{{$user['email']}}</a></p>
        <p>Lösenord: Vänligen klicka <a href="{{URL::to('user/reset')}}/{{$user['token']}}" target="_blank" >här</a> för att erhålla nytt lösenord.</p>
        <p>Tänk på att inloggningsuppgifterna är en värdehandling och därför inte bör lämnas ut till obehöriga. Förvara dina inloggningsuppgifter på ett säkert sätt.</p>
        <p>Om du inte vet med dig att du begärt inloggningsuppgifterna kan det betyda att någon av misstag eller medvetet angett ditt kundnummer på vår webbplats. Du kan då bortse från detta meddelande.</p>
        <p>&nbsp;</p>
        <p>Mvh <br> Teamet på svar på allt!</p>
    </body>
</html>
