<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>{{$user['full_name']}},</p>
        <p> Rapportera request from Svarpåallt Q&A.</p>
        <p>&nbsp;</p>
        <p>{{$user['message']}},</p>
        <p>&nbsp;</p>
        <p><a href="{{$user['question_url']}}" target="_blank" >{{$user['question_title']}}</a></p>
        
        <p>&nbsp;</p>
        <p>Thank you, <br> Svarpåallt Q&A <br><a href="{{URL::to('/')}}" target="_blank" >Svarpåallt Q&A</a></p>
    </body>
</html>
