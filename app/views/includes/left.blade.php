<div class="col-md-2 disable-mobile left-sidebar">
    <div class="widget widget-btn">
        <a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#login_register2" class="action ask-question abt">
            <i class="fa fa-plus"></i> Ställ en fråga
        </a>
    </div><!-- END BUTTON MODAL QUESTION -->
    
    <div class="widget widget-menus">
        <div class="menu-left-menu-container">
            <ul id="menu-left-menu-1" class="menu">
                <li class="fa-question-circle @if(Request::is('questions') or Request::is('questions/*') or Request::is('questions-*') or Request::is('question/*')) current-menu-item @endif"><a href="/questions"><i class="fa fa-question-circle"></i>Frågor</a></li>
                <li class="fa-tags @if(Request::is('tags') or Request::is('tags/*') or Request::is('tag/*')) current-menu-item @endif"><a href="/tags/popular"><i class="fa fa-tags"></i>Taggar</a></li>
                <li class="fa-trophy @if(Request::is('badges')) current-menu-item @endif "><a href="/badges"><i class="fa fa-trophy"></i>Titlar</a></li>
                <li class="fa-th-list @if(Request::is('categories') or Request::is('categories/*') or Request::is('category/*')) current-menu-item @endif"><a href="/categories"><i class="fa fa-th-list"></i>Kategorier</a></li>
                <li class="fa-users @if(Request::is('users') or Request::is('users/*') or Request::is('user/*')) current-menu-item @endif"><a href="/users"><i class="fa fa-users"></i>Användare</a></li>
            </ul>
        </div>
    </div><!-- END LEFT MENU -->
    <?php 
    if(Auth::check()) { 
        $user_status_array = array();
        $counter = 0;
        $questions = DB::table('posts')->select(array('title', 'url_key','created'))->where('type', '=', 'Q')->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($questions) > 0){
            foreach($questions as $q){
                
                $user_status_array[$counter]['type'] = 'fa-question';
                $user_status_array[$counter]['text'] = 'Du har ställt en fråga';
                $user_status_array[$counter]['title'] = $q->title;
                $user_status_array[$counter]['url_key'] = '/question/'.$q->url_key;
                $user_status_array[$counter]['created'] = $q->created;
                $counter++;
            }
        }
        
        $answers = DB::table('posts')->select(array('title', 'parentid','created'))->where('type', '=', 'A')->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($answers) > 0){
            foreach($answers as $a){
                $q = DB::table('posts')->select(array('title', 'url_key','title', 'created'))->where('postid', '=', $a->parentid)->first();
                if(count($q) > 0){
                    $user_status_array[$counter]['type'] = 'fa-comments';
                    $user_status_array[$counter]['text'] = 'Du har svarat på en fråga';
                    $user_status_array[$counter]['title'] = $q->title;
                    $user_status_array[$counter]['url_key'] = '/question/'.$q->url_key;
                    $user_status_array[$counter]['created'] = $a->created;
                    $counter++;
                }
            }
        } 
        $comments = DB::table('posts')->select(array('title', 'parentid','created'))->where('type', '=', 'C')->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($comments) > 0){
            foreach($comments as $c){
                $res = DB::table('posts')->select(array('url_key','title', 'parentid', 'type'))->where('postid', '=', $c->parentid)->first();
                if(count($res) > 0){
                    $user_status_array[$counter]['type'] = 'fa-comments';
                    if($res->type == 'A'){
                        $user_status_array[$counter]['text'] = 'Du har kommenterat ett svar på en fråga';
                        $q = DB::table('posts')->select(array('title', 'url_key','title'))->where('postid', '=', $res->parentid)->first();
                        if(count($q) > 0){
                            $user_status_array[$counter]['title'] = $q->title;
                            $user_status_array[$counter]['url_key'] = '/question/'.$q->url_key;
                        }
                    }else{
                        $user_status_array[$counter]['text'] = 'Du kommenterade på fråga';
                        $user_status_array[$counter]['title'] = $res->title;
                        $user_status_array[$counter]['url_key'] = '/question/'.$res->url_key;
                    }
                    $user_status_array[$counter]['created'] = $c->created;
                    $counter++;
                }
            }
        }
        $uservotes = DB::table('uservotes')->select(array('postid','flag','created'))->where('userid', '=', Auth::user()->userid)->orderBy('created', 'desc')->take(2)->get();
        if(count($uservotes) > 0){
            foreach($uservotes as $uv){
                $res = DB::table('posts')->select(array('url_key','title', 'parentid', 'type'))->where('postid', '=', $uv->postid)->first();
                if(count($res) > 0){
                $user_status_array[$counter]['type'] = 'fa-thumbs-up';
                if ($uv->flag == 0) {
                    $user_status_array[$counter]['type'] = 'fa-thumbs-down';
                }

                if ($res->type == 'A') {
                    if ($uv->flag == 0) {
                        $user_status_array[$counter]['text'] = 'Du har röstat ned ett svar på en fråga';
                    } else {
                        $user_status_array[$counter]['text'] = 'Du har röstat upp ett svar på en fråga';
                    }
                    $q = DB::table('posts')->select(array('url_key', 'title'))->where('postid', '=', $res->parentid)->first();
                    if (count($q) > 0) {
                        $user_status_array[$counter]['title'] = $q->title;
                        $user_status_array[$counter]['url_key'] = '/question/' . $q->url_key;
                    }
                } else {
                    if ($uv->flag == 0) {
                        $user_status_array[$counter]['text'] = 'Du har röstat ned en fråga';
                    } else {
                        $user_status_array[$counter]['text'] = 'Du har röstat upp en fråga';
                    }
                    $user_status_array[$counter]['title'] = $res->title;
                    $user_status_array[$counter]['url_key'] = '/question/' . $res->url_key;
                }


                $user_status_array[$counter]['created'] = $uv->created;
                $counter++;
            }
            }
        }
        if(count($user_status_array) > 0){
       
            function date_compare($a, $b) {
                $t1 = strtotime($a['created']);
                $t2 = strtotime($b['created']);
                return $t2 - $t1;
            }
            usort($user_status_array, 'date_compare');
        ?>
     <div class="widget widget-recent-activity">

        <ul>
            <?php 
                foreach($user_status_array as $usa){  
                if(isset($usa['url_key']) and isset($usa['title'])){    
            ?>
                    <li>
                        <i class="fa <?php echo $usa['type'] ?>"></i>
                        <span>
                            <?php echo $usa['text']; ?> 
                            <a href="<?php echo $usa['url_key']; ?>"><?php echo $usa['title']; ?></a>.
                            <br>
                            <span class="time-activity">
                                <?php echo Question::formattedCreatedDate($usa['created']); ?>
                            </span>
                        </span>
                    </li>
            <?php } } ?>
            
        </ul>

    </div>
    <?php }} ?>

    <div class="copyright">
        &copy;<?php echo date('Y'); ?> SVARPAALLT <br>
        <a href="http://blog.merinfo.se/kop-och-avtalsvillkor" target="_blank">Villkor</a>
    </div>
</div><!-- END LEFT-SIDEBAR -->  


<style>
a.abt{
	text-align:center !important;
	color:#FFF !important;
	padding-top:13px !important;
}
</style>