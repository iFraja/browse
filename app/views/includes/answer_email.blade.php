<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Hej {{$user['email']}}</p>
        <p>Du har fått ett svar på din fråga du ställt på svar på allt!</p>
        <p>&nbsp;</p>
        <p><a href="{{URL::to('/')}}/question/{{$user['question_url']}}" target="_blank" >{{$user['question_title']}}</a></p>
        
        <p>Har du glömt lösenordet? <a href="{{URL::to('user/reset')}}/{{$user['token']}}" target="_blank" >Klicka här</a>.</p>
        
        <p>&nbsp;</p>
        <p>Hoppas svaret är till din belåtenhet! <br> Teamet på svar på allt!</p>
    </body>
</html>
