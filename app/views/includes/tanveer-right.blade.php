<?php
$total_question = DB::table('posts')->where('type', '=', 'Q')->count();
$total_user = DB::table('users')->count();
?>
<div class="col-md-2 disable-mobile right-sidebar">
    <div class="widget widget-statistic">
        <p class="questions-count">
            Questions<br>
            <span><?php echo $total_question; ?></span>
        </p>
        <p class="members-count">
            Medlemmar<br>
            <span><?php echo $total_user; ?></span>
        </p>
    </div><!-- END widget-statistic -->
    
    <?php
        $top_users = DB::table('users')->orderBy('points', 'desc')->take(5)->get();
        if (count($top_users) > 0) {
    ?>
            <div class="widget user-widget">
                <h3 class="widgettitle">BÄSTA ANVÄNDARE (Poäng)</h3>
                <div class="hot-user-question">
                    <ul>
                       <?php $counter = 1; ?>  
                       <?php foreach($top_users as $u){ ?> 
                        <li>
                            <span class="number"><?php echo $counter; ?></span>
                            <span class="username ">
                                <a title="<?php echo $u->username; ?>" href="/user/<?php echo $u->url_key; ?>" ><?php echo $u->username; ?></a>
                            </span>
                            <span title="<?php echo $u->points; ?> Poäng" class="points-count">
                                <i class="fa fa-star"></i>
                                <span><?php echo $u->points; ?></span>
                            </span>
                        </li>
                       <?php $counter++; } ?>
                    </ul>
                </div>
            </div>
    <?php } ?>
    <?php
        $top_questions = DB::select( DB::raw("SELECT qa_users.userid, qa_users.url_key as user_url_key, qa_users.username, count(*) as total FROM qa_users, qa_posts where qa_users.userid = qa_posts.userid and qa_posts.type = 'Q' group by qa_posts.userid order by total DESC ") );
        if (count($top_questions) > 0) {
    ?>
    <div class="widget user-widget">
        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Frågor)</h3>
        <div class="hot-user-question">
            <ul>
                <?php $counter = 1; ?>
                <?php foreach($top_questions as $q){ ?> 
                <li>
                    <span class="number"><?php echo $counter; ?></span>
                    <span class="username ">
                        <a title="<?php echo $q->username; ?>" href="/user/<?php echo $q->user_url_key; ?>"><?php echo $q->username; ?></a>
                    </span>
                    <span title="<?php echo $q->total; ?> Frågor" class="questions-count">
                        <i class="fa fa-question-circle"></i>
                        <span><?php echo $q->total; ?></span>
                    </span>
                </li>
                <?php $counter++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <?php
        $top_answers = DB::select( DB::raw("SELECT qa_users.userid, qa_users.url_key as user_url_key, qa_users.username, count(*) as total FROM qa_users, qa_posts where qa_users.userid = qa_posts.userid and qa_posts.type = 'A' group by qa_posts.userid order by total DESC ") );
        if (count($top_answers) > 0) {
    ?>
    <div class="widget user-widget">
        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Svar)</h3>
        <div class="hot-user-question">
            <ul>
                <?php $counter = 1; ?>
                <?php foreach($top_answers as $a){ ?> 
                <li>
                    <span class="number"><?php echo $counter; ?></span>
                    <span class="username ">
                        <a title="<?php echo $a->username; ?>" href="/user/<?php echo $a->user_url_key; ?>"><?php echo $a->username; ?></a>
                    </span>
                    <span title="<?php echo $a->total; ?> Svar" class="answers-count">
                        <i class="fa fa-comments"></i>
                        <span><?php echo $a->total; ?></span>
                    </span>
                </li>
                <?php $counter++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <?php
        $latest_users = DB::table('users')->orderBy('userid', 'desc')->take(5)->get();
        if (count($latest_users) > 0) {
    ?>
    <div class="widget user-widget">
        <h3 class="widgettitle">BÄSTA ANVÄNDARE (Senaste)</h3>
        <div class="hot-user-question">
            <ul>
                <?php $counter = 1; ?>
                <?php foreach($latest_users as $u){ ?> 
                <li>
                    <span class="number"><?php echo $counter; ?></span>
                    <span class="username latest">
                        <a title="<?php echo $u->username; ?>" href="/user/<?php echo $u->url_key; ?>"><?php echo $u->username; ?></a>
                    </span>
                </li>
                <?php $counter++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
</div><!-- END RIGHT-SIDEBAR -->