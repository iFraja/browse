<?php
$keywords = '';
if(isset($data['keywords'])){
    $keywords = $data['keywords'];
} 
?>
<header id="header">
    <div class="col-md-2 col-xs-2" id="logo">
        <a href="/">
            <img src="/qaengine/img/logo1.png">
        </a>
    </div><!-- logo -->
    <div class="col-md-8 col-xs-8">
        <div class="header-menu">
            <ul>
                <li><a href="/">Hem</a></li>
                <li><a target="_blank" href="http://blog.merinfo.se/om-oss-merinfo-se">Om oss</a></li>
                <li><a target="_blank" href="http://blog.merinfo.se/kop-och-avtalsvillkor">Villkor</a></li>
                <li><a target="_blank" href="http://blog.merinfo.se/om-oss-merinfo-se">Kontakt</a></li>
            </ul>
        </div><!-- menu -->
        <div class="header-search-wrapper header-center-wrapper" >
            <section class="buttonset">
                <button id="showLeftPush">&nbsp;</button>
                <button id="showRightPush">&nbsp;</button>
            </section>
            <form id="header_search" method="GET" action="/questions/search" class="disable-mobile">
                <input type="text" name="keyword" value="<?php echo $keywords; ?>" placeholder="Ange nyckelord" autocomplete="off" />
                <i class="fa fa-search"></i>
                <div id="search_preview" class="search-preview empty"></div>
            </form>
        </div><!-- search -->
    </div>
    
    
   @if(Auth::check())    
    <div class="col-md-2 col-xs-2 btn-group header-avatar " >
        <span class="expand dropdown-toggle" type="span" data-toggle="dropdown">
            <a href="javascript:void(0)" class="dropdown-account " >
                <span class="avatar"><img src="{{Auth::user()->user_picture}}" class="avatar" alt="" /></span>
                <span class="display_name">{{Auth::user()->handle}}</span>
                <span class="icon-down"><i class="fa fa-chevron-circle-down"></i></span>
            </a>
        </span>
        <ul class="dropdown-menu dropdown-profile">
            <li>
                <a href="/user/{{Auth::user()->url_key}}">
                    <i class="fa fa-user"></i> Min profil								</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="open-edit-profile edit_profile">
                    <i class="fa fa-cog"></i> Mina inställningar								</a>
            </li>

            <li>
                <a href="/logout">
                    <i class="fa fa-power-off"></i> Logga ut								</a>
            </li>
        </ul>

    </div><!-- avatar -->
    @else
    <div class="col-md-2 col-xs-2 btn-group ">
	<a data-toggle="modal" data-target="#login_register2" href="javascript:void(0)" class="login-url">Logga in eller bli medlem</a>
    </div>
    @endif
</header><!-- END HEADER -->
<div class="col-md-12 second_header">
   <form id="header_search" method="GET" action="/questions/search" >
        <input type="text" name="keyword" value="<?php echo $keywords; ?>" placeholder="Ange nyckelord" autocomplete="off" />
        <i class="fa fa-search"></i>
        <div id="search_preview" class="search-preview empty"></div>
</form> 
</div>

<div class="col-md-12 col-xs-12" id="header_sidebar"></div>        
