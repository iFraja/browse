@extends('layouts.default')
@section('content')

<div class="qa-main">
	<h1>Administration center - Users</h1>
	<div class="qa-part-form">
					<form action="./index.php?qa=admin&amp;qa_1=userfields" method="post">
						<table class="qa-form-tall-table">
							<tbody><tr>
								<td class="qa-form-tall-label">
									Field name:
								</td>
							</tr>
							<tr>
								<td class="qa-form-tall-data">
									<input type="text" class="qa-form-tall-text" value="" id="name" name="name">
								</td>
							</tr>
							</tbody><tbody id="type_display">
								<tr>
									<td class="qa-form-tall-label">
										Content type:
										&nbsp;
										<select class="qa-form-tall-select" name="type">
											<option selected="" value="0">Single line of text</option>
											<option value="1">Multiple lines of text</option>
											<option value="2">Linked URL</option>
										</select>
									</td>
								</tr>
							</tbody>
							<tbody id="permit_display">
								<tr>
									<td class="qa-form-tall-label">
										Visible for:
										&nbsp;
										<select class="qa-form-tall-select" name="permit">
											<option value="150">Anybody</option>
											<option value="120">Registered users</option>
											<option value="100">Experts, Editors, Moderators, Admins</option>
											<option value="70">Editors, Moderators, Admins</option>
											<option value="40">Moderators and Admins</option>
											<option value="20">Administrators</option>
										</select>
									</td>
								</tr>
							</tbody>
							<tbody id="position_display">
								<tr>
									<td class="qa-form-tall-label">
										Position:
										&nbsp;
										<select class="qa-form-tall-select" name="position">
											<option value="1">First</option>
											<option value="2">After "Fullständigt namn"</option>
											<option value="3">After "Geografisk plats"</option>
											<option value="4">After "Website"</option>
											<option selected="" value="5">After "About"</option>
										</select>
									</td>
								</tr>
							</tbody>
							<tbody id="register_display">
								<tr>
									<td class="qa-form-tall-label">
										<label>
											<input type="checkbox" class="qa-form-tall-checkbox" value="1" name="onregister">
											Show field on user registration form
										</label>
									</td>
								</tr>
							</tbody>
							<tbody><tr>
								<td class="qa-form-tall-buttons" colspan="1">
									<input type="submit" class="qa-form-tall-button qa-form-tall-button-save" title="" value="Add Field">
									<input type="submit" class="qa-form-tall-button qa-form-tall-button-cancel" title="" value="Avbryt" name="docancel">
								</td>
							</tr>
						</tbody></table>
						<input type="hidden" value="1" name="dosavefield">
						<input type="hidden" value="" name="edit">
						<input type="hidden" value="1-1418623772-f0554f5c3a7a647da9c474ca9bd5c39ecebea0b0" name="code">
					</form>
				</div>
</div>
@stop
